`include "defines.v"

module fir #(
  parameter FIR_TAPS = `FIR_TAPS,
  parameter FIR_NB_I = `FIR_NB_I,
  parameter FIR_NB_F = `FIR_NB_F
) (
  clk, rst_n, enable,
  i_sample, i_coef,
  o_sample
);

  // Total number of bits for representing a coefficient:
  localparam FIR_NB = FIR_NB_I + FIR_NB_F;
  
  // Number of bits for dot operation:
  localparam NBI_DOT_COEFF = FIR_NB_I + $clog2(FIR_TAPS);   // Integer
  localparam NBF_DOT_COEFF = FIR_NB_F;                      // Fractional
  localparam NB_DOT_COEFF = NBI_DOT_COEFF + NBF_DOT_COEFF;  // Total

  input clk;
  input rst_n;
  input enable;
  input signed [FIR_TAPS-1 : 0] i_sample;
  input signed [FIR_TAPS*(FIR_NB)-1 : 0] i_coef;
  output reg [NB_DOT_COEFF-1 : 0] o_sample;

  wire signed [FIR_NB-1 : 0] coeff_channel[FIR_TAPS-1 : 0];
  wire signed [FIR_NB-1 : 0] dot_filter[FIR_TAPS-1 : 0];
  reg signed  [NB_DOT_COEFF-1 : 0] add_dot_filter;
  integer ptr_add;


  always@(posedge clk or negedge rst_n) begin 
    if (!rst_n) begin
      o_sample <= 0;
    end
    else if(enable) begin
      o_sample <= add_dot_filter;
     end
  end

  // dot product
  generate
    genvar tap;
    for(tap=0; tap < FIR_TAPS; tap=tap+1) begin:dotprod
      assign coeff_channel[FIR_TAPS-1 - tap]  = i_coef[tap*FIR_NB +: FIR_NB];  
      assign dot_filter[FIR_TAPS-1 - tap] = (i_sample[tap] == 1'b1) ? 
                                              coeff_channel[tap] : 
                                              (coeff_channel[tap] == {{1'b1},{FIR_NB-1{1'b0}}}) ? 
                                                {{1'b0},{FIR_NB-1{1'b1}}} :
                                                -coeff_channel[tap];
    end
  endgenerate

  // convolution
  always@(*) begin:convfilter
    add_dot_filter = 0;
    for(ptr_add=0; ptr_add<FIR_TAPS; ptr_add=ptr_add+1) begin:convpartial
      // sum all taps (extending bit sign)
      add_dot_filter = add_dot_filter + { {$clog2(FIR_TAPS){dot_filter[ptr_add][FIR_NB-1]}}, dot_filter[ptr_add] };
    end
  end

endmodule
