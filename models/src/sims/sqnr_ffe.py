"""SQNR test for FFE component.

This test will measure SQNR for a fixed point FFE component against a reference
(floating) FFE.

How to run:
    python -m src.sims sqnr ffe

The results will be sent to the Plot module (./src/Plot) for storing and plotting with
plotly.
"""

from functools import partial

from src import System

from .core import SQNRTestDirector

EXCLUSIONS = ("",)


def sqnr_ffe():
    """SQNR simulation for FFE quantization."""
    modifier = partial(SQNRTestDirector.clean_quantizations, exclusions=EXCLUSIONS)
    system = System.from_yaml("quantization.yaml", modifier)

    tests = SQNRTestDirector(
        adapt=500_000,
        measure=10_000,
        system=system,
        reference=system.forward,
    )

    tests.add_test(
        name="Coefficients",
        quantize={"coef": "s0/0-z/s"},
        sweep="coef",
        integer=range(0, 2),
        fractional=range(1, 13),
    )

    tests.add_test(
        name="Input",
        quantize={"coef": "s10/8-z/s", "input": "s0/0-z/s"},
        sweep="input",
        integer=range(0, 3),
        fractional=range(1, 13),
    )

    tests.add_test(
        name="Output",
        quantize={
            "coef": "s10/8-z/s",
            "input": "s10/7-z/s",
            "response": "s0/0-z/s",
        },
        sweep="response",
        integer=range(0, 3),
        fractional=range(1, 13),
    )

    return tests.run()
