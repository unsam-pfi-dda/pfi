`include "defines.v"

module branch_metrics #(
  parameter SAMP_W   = `SAMP_W,
  parameter ADQ_W    = `ADQ_W,
  parameter BM_WIDTH = `BM_WIDTH
) (
  // Expected adquisitions
  input  signed [ADQ_W-1 :0] i_adq_000,
  input  signed [ADQ_W-1 :0] i_adq_001,
  input  signed [ADQ_W-1 :0] i_adq_010,
  input  signed [ADQ_W-1 :0] i_adq_011,
  input  signed [ADQ_W-1 :0] i_adq_100,
  input  signed [ADQ_W-1 :0] i_adq_101,
  input  signed [ADQ_W-1 :0] i_adq_110,
  input  signed [ADQ_W-1 :0] i_adq_111,

  // Sampled adquisition
  input  signed [SAMP_W-1:0] i_sample,

  // Difference (absolute) between expected and sampled adquisitions
  output        [BM_WIDTH-1 :0] o_delta_000,
  output        [BM_WIDTH-1 :0] o_delta_001,
  output        [BM_WIDTH-1 :0] o_delta_010,
  output        [BM_WIDTH-1 :0] o_delta_011,
  output        [BM_WIDTH-1 :0] o_delta_100,
  output        [BM_WIDTH-1 :0] o_delta_101,
  output        [BM_WIDTH-1 :0] o_delta_110,
  output        [BM_WIDTH-1 :0] o_delta_111
);

assign o_delta_000 = (((i_adq_000 - i_sample) < 0) ? (i_sample - i_adq_000) : (i_adq_000 - i_sample));
assign o_delta_001 = (((i_adq_001 - i_sample) < 0) ? (i_sample - i_adq_001) : (i_adq_001 - i_sample));
assign o_delta_010 = (((i_adq_010 - i_sample) < 0) ? (i_sample - i_adq_010) : (i_adq_010 - i_sample));
assign o_delta_011 = (((i_adq_011 - i_sample) < 0) ? (i_sample - i_adq_011) : (i_adq_011 - i_sample));
assign o_delta_100 = (((i_adq_100 - i_sample) < 0) ? (i_sample - i_adq_100) : (i_adq_100 - i_sample));
assign o_delta_101 = (((i_adq_101 - i_sample) < 0) ? (i_sample - i_adq_101) : (i_adq_101 - i_sample));
assign o_delta_110 = (((i_adq_110 - i_sample) < 0) ? (i_sample - i_adq_110) : (i_adq_110 - i_sample));
assign o_delta_111 = (((i_adq_111 - i_sample) < 0) ? (i_sample - i_adq_111) : (i_adq_111 - i_sample));

endmodule
