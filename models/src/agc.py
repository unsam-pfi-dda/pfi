import numpy as np

from .component import ComponentBase
from .wave import Wave


class AGC(ComponentBase):
    """Automatic gain controller for keeping the signal power normalized to 1."""

    def __init__(self, gain: float = 1.0, buffer_length: int = 32) -> None:
        self.locals = locals()
        self.gain = gain  # This is the target gain

        self.input = Wave(buffer_length)
        self.response = Wave(buffer_length)
        self._adapt_offset = 0

    def run(self, sample: np.float64) -> np.float64:
        """Run."""
        self.input.add(sample)

        # Do not adapt until buffer is full
        if 0 in self.input.values:
            response = sample
        else:
            response = self.input.last * self.gain / self.input.power
        self.response.add(response)
        return self.response.last

    @property
    def output(self) -> Wave:
        return self.response

    @property
    def adapt_offset(self) -> int:
        return self._adapt_offset

    @adapt_offset.setter
    def adapt_offset(self, new: int):
        self._adapt_offset = new

    @property
    def lag(self) -> int:
        return 0

    def adapt(self, error: np.float64) -> None:
        pass
