"""Maximum likelihood sequence estimator.

This module contains three classes:
 - Viterbi (Main class)
 - Cell
 - Connection

The Viterbi setup will take three arguments: `depth`, `taps` and `symbol`.


"""
import itertools
from collections import Counter
from concurrent.futures import ThreadPoolExecutor
from copy import deepcopy
from dataclasses import dataclass, field
from typing import Any

import numpy as np
from numfi import numfi
from numpy.typing import NDArray
from tqdm import tqdm

from .fixed_arithmetic.utils import to_fixed
from .slicer import BPSK
from .wave import Wave


class Cell:
    """Viterbi cell class. This object will compare the two possible transitions
    and select the one with the least cost.

    Attributes:
        symbols: a tuple of possible symbols
        sequence:
        metrics: A cost matrix for each origin sequence toward each symbol. For a two-symbol and
            two-taps system that would be a 2x2 matrix with all four possible transitions and costs.
            The calculate method will add the accumulated cost to this metric in order to select
            the path with the least cost.

    Example:
                [in1->out1, in2->out1]
                [in1->out2, in2->out2]

    """

    symbols: tuple[float, ...]
    sequence: tuple[tuple[list[int], ...], ...]
    metrics: list[NDArray[np.floating[Any]]]
    quantize: str

    def __init__(
        self,
        sequence: tuple[tuple[list[int], ...], ...],
        symbols: tuple[float, ...],
        quantize: str = "",
    ) -> None:
        if len(sequence) is not len(symbols):
            raise ValueError("There must be as many sequences as symbols")

        self.symbols = symbols
        self.sequence = sequence
        self.quantize = quantize

        self.update_metrics(np.float64([0]))

    def calculate(
        self,
        sample: np.float64,
        cost: tuple[np.float64, ...],
        stream: tuple[Wave, ...],
    ) -> list[tuple[np.float64, Wave]]:
        """Calculate the cell response for a new sample
        Arguments:
            cost
                The accumulated costs carried from previous iterations. Is a tuple for each valid
                path.

            stream
                The accumulated sequence carried from previous iterations. Is a tuple for each valid
                path.

        Returns
            A list of `symbols` length each element containing a tuple with `cost` and `stream`.
            e.g. for symbols=[-1, 1] will return [(c1, s1), (c2, s2)] where `cx` is cost and `sx`
            is stream.
        """
        # There will be as many output values as symbol quantities
        output = []
        # Need to transpose metrics matrix in order to align the costs per symbol.
        for metric, symbol in zip(self.metrics, self.symbols):
            # Calculate the cost for each possible sequence and transition
            # Not using matrix operations to keep numfi parameters (otherwise will get erased by np.array)
            costs = [
                _cost + np.abs(sample - _metric) for _cost, _metric in zip(cost, metric)
            ]

            new_cost = min(costs)
            sequence = deepcopy(stream)[costs.index(new_cost)]
            # TODO: Might use deque instead of Wave for better performance.
            #       because no numpy/numfi operations are needed for this buffer.
            sequence.add(symbol)

            result = (new_cost, sequence)
            output.append(result)

        return output

    def update_metrics(self, coef: NDArray[np.float64]) -> None:
        """Calculate Cell metric values for the provided coefficients."""
        # Repeat the first symbol as the initializer sequence.
        initialize_sequence = [self.symbols[0]] * (len(coef) - 1)

        # Metrics list
        self.metrics = [
            [
                np.convolve(
                    coef,
                    np.concatenate((initialize_sequence, sequence), axis=None),
                    mode="full",
                )[-len(coef)]
                for sequence in _sequences
            ]
            for _sequences in zip(*self.sequence)
        ]

        # Quantize results
        if self.quantize:
            self.metrics = [
                numfi(metric, **to_fixed(self.quantize), fixed=True)
                for metric in self.metrics
            ]


@dataclass
class State:
    """MLSE Cell state object class.

    Attributes
        id: unique identifier
        combination: unique symbol combination that identify this state
        stream: bitstream sequence to this state
        cost: accumulated cost for the stream
        quantize: parameters for 'cost' quantization, leave empty for floating, defaults to ""

        transitions: state unique identifier that this state can transition to
        sequence: initialization sequence used for calculating the cost toward each transition
        cell: Cell object that references this state.
    """

    id: int
    combination: tuple[float, ...]
    stream: Wave = field(repr=False)
    cost: np.float64 = field(repr=False, default=0.0)
    quantize: str = ""

    transitions: tuple[int, ...] = field(default_factory=lambda: (-1,))
    sequence: tuple[list[int], ...] = field(repr=False, default_factory=lambda: ([-1],))
    cell: Cell = field(repr=True, default_factory=lambda: Cell((([-1],),), (-1,)))

    def __post_init__(self):
        if self.quantize:
            self.cost = numfi(self.cost, **to_fixed(self.quantize), fixed=True)


class Viterbi:
    """MLSE Viterbi algorithm class."""

    taps: int
    depth: int
    symbols: tuple[float]
    states: list[State]
    input: Wave
    output: Wave
    i: int

    cost: np.float64

    def __init__(
        self,
        depth: int,
        taps: int,
        quantize: None | dict[str, str] = None,
        symbols: tuple[float, ...] = (-1, 1),
    ) -> None:
        """Viterbi class initializer.

        Arguments:
            taps: Number of post-cursors coefficients: d1, d2 and d3 (not including main cursor).
            depth: How many samples uses to estimate the most likely sequence,
                also determines the Viterbi lag.
            symbols: Possible symbols that are being used.
            quantize: possible keys are 'metrics' and 'costs' for quantizing Cell object buffers.

        """
        self.taps = taps
        self.depth = depth
        self.quantize = quantize if quantize else {}
        self.symbols = symbols

        self.states = self.def_states(
            taps=self.taps,
            symbols=self.symbols,
            depth=self.depth,
            quantize_cost=self.quantize.get("cost", ""),
            quantize_metrics=self.quantize.get("metrics", ""),
        )
        self.input = Wave(self.depth)
        self.output = Wave(self.depth)

        # Iteration cycle
        self.i = -1
        self.cost = 0

    @staticmethod
    def def_states(
        taps: int,
        symbols: tuple[float, ...],
        depth: int,
        quantize_cost: str = "",
        quantize_metrics: str = "",
    ) -> list[State]:
        """Generate all the possible sequence, calculate metrics and state transitions for the trellis
        Returns a list of State objects with a size of len(symbols)^taps, each list object contains information
        about each unique state in a State-object format as follows:
            - 'id': an int for the state id
            - 'combination': a tuple of length `taps` with the combination value for this state
            - 'transitions': a tuple of length `symbols` with the target state to transition when a new symbol is received
            - 'cell': a Cell object reference that this state connects to.
            - 'sequence': a tuple of length `symbols` containing a numpy array with the symbol sequence required to initialize this state.

        Arguments:
            taps: Number of Viterbi taps
            symbols: List of possible symbols
            depth: Number of Viterbi trellis iterations.
            quantize_cost: Quantization parameters for 'cost' buffer in State object.
            quantize_metrics: Quantization parameters for 'metrics' list in Cell object.
        """
        # Get a list of all combinations of length `taps` for any possible `symbol` sequence
        #   e.g. for symbols [-1, 1] and 2 taps -> [(-1, -1), (-1, 1), (1, -1), (1, 1)]
        combinations = list(itertools.product(symbols, repeat=taps))

        # Each possible combination will have a unique (ordered) id as a reference
        states = [
            State(
                id=id,
                combination=combination,
                stream=Wave(depth),
                quantize=quantize_cost,
            )
            for id, combination in enumerate(combinations)
        ]

        for state in states:
            # Calculate sequence
            # TODO: Still need to work this out
            sequence: list[int] = []
            for i, _ in enumerate(symbols):
                seq = np.concatenate(
                    (np.zeros(taps), np.array(list(state.combination)), np.array([i])),
                ).astype(int)
                seq = np.where(seq == 0, -1, seq)  # TODO: WTF?
                sequence.append(seq)
            state.sequence = tuple(sequence)

            # Calculate all allowed transitions for each state by inspecting the possible combination states
            #  e.g. For state=0  comb=(-1, -1) the only allowed transitions are trans=(-1, 1)
            #       Because the only possible states are (-1, -1) for [-1] as input or (-1, 1) for [1].
            # Transitions is a tuple with length `symbol`, which values corresponds to the transitioned state
            # aligned with `symbol` list. The first tuple value will be the state id to transition to, if the first
            # symbol value is received, and so on.
            state.transitions = tuple(
                _state.id
                for symbol in symbols
                # Get the state id for each possible transition by looking for the state id
                # for the combination with each symbol.
                #   e.g. for symbols [-1, 1] then state=0 [-1,-1] can transition to
                #       state=0 [-1,-1,-1] or to state=1 [-1,-1, 1].
                for _state in states
                if _state.combination == (state.combination[1:] + (symbol,))
            )

        # There will be a Cell for each unique transition, or number of possible symbols. Assign
        # a cell id for each state/transition.
        # TODO: Not the case, but both 0,1 and 1,0 should be the same cell but with switched wire.
        #       Need to check if that will ever happen.
        # Convert to dict then back to list to keep the cell id ordered with the states/transitions,
        # otherwise set() will not ensure a correct order (not needed but will keep things cleaner).

        for state_ids in dict.fromkeys(state.transitions for state in states):
            # Generate a tuple for all states sequences that belong to this cell
            cell = Cell(
                sequence=tuple(
                    state.sequence for state in states if state.transitions == state_ids
                ),
                symbols=symbols,
                quantize=quantize_metrics,
            )

            # Assign cell reference to the state.cell attribute
            for state in states:
                if state.transitions == state_ids:
                    state.cell = cell

        return states

    def run(self, sample: np.float64, coef: NDArray[np.float64]) -> np.float64:
        """Run the VA algorithm for a new sample.

        Arguments:
            sample: new sample value to compute.
            coef: list of postcursors values, should not include main cursor.

        Returns:
            The estimated result.
        """
        # Skip if depth is zero (behave just like a slicer)
        if self.depth == 0:
            return BPSK(sample)

        self.i = (self.i + 1) % self.depth

        # Latch the most likelihood sequence stream once a full cycle has been done
        if self.i == 0:
            # The coefficient list received from the DFFE does not include the cursor,
            # so it is manually inserted to the coefficient list.
            # NOTE: This assumes that the equalizer main cursor value is 1 and the
            #   system's response power is normalized, or results might not be accurate.
            coef = [1] + coef.tolist()[: self.taps]

            cost, stream = self.calculate(self.input.values.copy(), coef)
            self.output.set(stream.values.copy())
            self.cost = np.float64(cost.copy())

        self.input.add(sample)

        # Serialize the output
        return self.output.values[self.i]

    def calculate(
        self,
        samples: NDArray[np.float64],
        coef: NDArray[np.float64],
    ) -> tuple[np.float64, Wave]:
        """Calculate Viterbi, forward and traceback to most likely sequence."""
        # Update branch metrics and clear costs:
        for cell in self.cells:
            cell.update_metrics(coef)
        for state in self.states:
            state.cost -= state.cost

        # Trellis forward
        for sample in samples:
            # Feed the cells with the 'from' data and do a calculation.
            calculations = [
                cell.calculate(
                    sample=sample,
                    cost=tuple(s.cost for s in self.cells_from(cell)),
                    stream=tuple(s.stream for s in self.cells_from(cell)),
                )
                for cell in self.cells
            ]

            # Update states to the next cells with the previously calculated values
            for cell, results in zip(self.cells, calculations):
                for state, (cost, stream) in zip(self.cells_to(cell), results):
                    state.cost = cost
                    state.stream = stream

        # Trellis traceback: Select the most likely sequence (least cost)
        choices = [(state.cost, state.stream) for state in self.states]
        chosen = choices[0]
        for choice in choices:
            # This is the most likelihood sequence so far (could change)
            chosen = choice if choice[0] < chosen[0] else chosen

        return chosen

    @property
    def lag(self) -> int:
        """Calculates the time-delay for Viterbi."""
        return -self.depth

    @property
    def cells(self) -> tuple[Cell]:
        """Returns a list of all cells."""
        return tuple(dict.fromkeys(state.cell for state in self.states))

    def cells_from(self, ref: Cell) -> tuple[State, ...]:
        """Returns the states id that transitions TO the provided cell."""
        return tuple(state for state in self.states if state.cell is ref)

    def cells_to(self, ref: Cell) -> tuple[State, ...]:
        """Returns the states id that transitions FROM the provided cell."""
        state, _ = self.cells_from(ref)
        return tuple(self.states[id] for id in state.transitions)


def simulate(snr: float, size: int, depth: int, channel: list[float]):
    # Generate the bitstream to feed the Viterbi
    symbols = [-1.0, 1.0]
    np.random.seed(0)
    bitstream = np.random.choice(symbols, size=(size + depth + len(channel) - 1,))

    U = np.convolve(channel, bitstream, mode="valid")

    # Adjust Channel component noise_scale parameter for this current SNR.
    snr_scalar = 10 ** (snr / 10)
    input_power = np.dot(channel, channel)
    sn = input_power / snr_scalar
    noise_scale = np.sqrt(sn / 2)
    N = np.random.normal(loc=0, size=len(U)) * noise_scale  # En dB

    Z = U + N

    viterbi = Viterbi(depth=depth, taps=len(channel) - 1)

    stream_in = bitstream[:size]
    results = np.array(
        [
            viterbi.run(sample=sample, coef=channel[1:])
            for sample in tqdm(Z, desc=f"SNR = {snr}")
        ],
    )

    # Need to compensate both Viterbi and channel lag
    lag = viterbi.lag + (len(channel) - 1)

    stream_out = results[-lag : size - lag]

    errors = Counter(stream_in == stream_out)[False]
    ber = errors / size

    return (snr, errors, size, ber)


if __name__ == "__main__":
    from src.plot import Plot

    # Steps for running this as standalone:
    #  - Move to `./models` directory
    #  - Execute `python -m src.mlse ./src/mlse.py`

    DEPTH = [5, 25, 100]
    SIZE = 10_000
    CHANNEL = np.array([1, 0.5, 0.125])
    SNR = range(4, 12)

    data = []

    for depth in DEPTH:
        with ThreadPoolExecutor() as executor:
            futures = [
                executor.submit(simulate, snr, SIZE, depth, CHANNEL) for snr in SNR
            ]

        data.append(
            {
                "group": f"Viterbi - D={depth} - Channel = {CHANNEL}",
                "symbol": "circle",
                "results": [[future.result() for future in futures]],
            },
        )

    # Store results in a file, read the file and plot the data
    Plot.save_plot(plot_type="ber", plot_data=data)
