from collections.abc import Sequence

import numpy as np
from pylfsr import LFSR


class Generator:
    """Class for random symbol generation using different probability density functions."""

    @classmethod
    def normal(cls) -> np.float64:
        """Generates a random symbol using normal distribution."""
        return np.random.normal()

    @classmethod
    def uniform(cls, symbols: Sequence[float]) -> np.float64:
        """Generates a random symbol using uniform distribution."""
        return np.random.choice(symbols, None)

    @classmethod
    def prbs(cls, sequence: int = 9, seed: int = -1):
        """PRSB-k generator.

        Arguments:
            sequence:
            seed: The random seed. The value must be between 0 and 2^sequence-1.

        Examples:
            Sequence for a PRBS-4 with seed 0xFF (default)

            >>> import itertools
            >>> list(itertools.islice(Generator.prbs(sequence=4), 15))
            [1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 1, 1, 0, 1, 0]

            Sequence for a PRBS-4 with seed 0x01

            >>> list(itertools.islice(Generator.prbs(sequence=4, seed=0x01), 15))
            [0, 0, 0, 1, 0, 0, 1, 1, 0, 1, 0, 1, 1, 1, 1]

            The sequence will repeat after 2^sequence-1 iterations

            >>> p = list(itertools.islice(Generator.prbs(sequence=4, seed=0x01), 30))
            >>> p[0:15] == p[15:30]
            True

        """
        prbs_sequences = {
            4: [4, 3],  # x^4 + x^3 + x^1
            5: [5, 3],  # x^5 + x^3 + x^1
            6: [6, 5],  # x^6 + x^5 + x^1
            7: [7, 6, 5, 4],  # x^7 + x^6 + x^5 + x^4 + x^1
            8: [8, 6],  # x^8 + x^6 + x^1
            9: [9, 5],  # x^9 + x^5 + x^1
            10: [10, 7],  # x^10 + x^7 + x^1
            11: [11, 9],  # x^11 + x^9 + x^1
            12: [12, 11, 10, 4],  # x^12 + x^11 + x^10 + x^4 + x^1
            13: [13, 12, 11, 8],  # x^13 + x^12 + x^11 + x^8 + x^1
            14: [14, 13, 12, 2],  # x^14 + x^13 + x^12 + x^2 + x^1
            15: [15, 14],  # x^15 + x^14 + x^1
            16: [16, 14, 13, 11],  # x^16 + x^14 + x^13 + x^11 + x^1
            17: [17, 14],  # x^17 + x^14 + x^1
            18: [18, 11],  # x^18 + x^11 + x^1
            19: [19, 18, 17, 14],  # x^19 + x^18 + x^17 + x^14 + x^1
            20: [20, 17],  # x^20 + x^17 + x^1
            23: [23, 18],  # x^23 + x^18 + x^1
            31: [31, 28],  # x^31 + x^28 + x^1
        }

        if sequence not in prbs_sequences:
            raise ValueError("invalid prbs sequence")

        if seed < 0:
            seed = 2**sequence - 1

        initial_state = [
            int(bit) for bit in reversed(format(seed, "b").zfill(sequence))
        ]
        lfsr = LFSR(fpoly=prbs_sequences[sequence], initstate=np.array(initial_state))
        while True:
            yield lfsr.runKCycle(1)[0]


if __name__ == "__main__":
    # Define the possible symbols
    samples_symbols = [-1, 1]
    # Generate 1000 samples
    N = 10000
    samples_normal = [Generator.normal() for _ in range(N)]
    samples_uniform = [Generator.uniform(samples_symbols) for _ in range(N)]

    # Plot results
    import plotly.graph_objects as go

    fig = go.Figure()
    fig.add_trace(go.Histogram(x=samples_normal, histnorm="probability", name="Normal"))
    fig.add_trace(
        go.Histogram(x=samples_uniform, histnorm="probability", name="Uniform"),
    )

    fig.update_xaxes(title_text="Symbol")
    fig.update_yaxes(title_text="Probability")
    fig.update_layout(barmode="overlay")
    fig.update_traces(opacity=0.75)
    fig.show()
