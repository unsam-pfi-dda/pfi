`include "defines.v"

module fir_parallel #(
  parameter PARALLELISM           = `PARALLELISM,
  parameter FIR_TAPS              = `FIR_TAPS,
  parameter FIR_NB_I              = `FIR_NB_I,
  parameter FIR_NB_F              = `FIR_NB_F
) (
  input                                   clk,
  input                                   rst_n,
  input                                   enable,
  input signed [PARALLELISM         - 1 : 0] i_sample,
  input signed [FIR_TAPS*NB_COEF - 1 : 0] i_coef,
  output       [PARALLELISM*NB_DOT_COEFF     - 1 : 0] o_sample
);

  // Total number of bits for representing a coefficient:
  localparam NB_COEF                    = FIR_NB_I + FIR_NB_F;
  // Number of bits for dot operation:
  localparam NBI_DOT_COEFF              = FIR_NB_I + $clog2(FIR_TAPS);    // Integer
  localparam NBF_DOT_COEFF              = FIR_NB_F;                       // Fractional
  localparam NB_DOT_COEFF               = NBI_DOT_COEFF + NBF_DOT_COEFF;  // Total

  reg [(PARALLELISM+FIR_TAPS-1) - 1 : 0]         fir_ch_retimer;
   
  generate
    genvar i;
    for(i=0; i<PARALLELISM; i=i+1) begin:fir_parallel
      fir #(
        .FIR_TAPS(FIR_TAPS),
        .FIR_NB_I(FIR_NB_I)
      ) u_fir (
        .clk (clk),
        .rst_n (rst_n),
        .enable (enable),
        .i_sample (fir_ch_retimer[(FIR_TAPS+i)-1 -: FIR_TAPS]),
        .i_coef (i_coef),
        .o_sample (o_sample[(NB_DOT_COEFF*(i+1)) - 1 -: NB_DOT_COEFF])
      );
    end
  endgenerate

  always@(posedge clk) begin
    fir_ch_retimer <= {{fir_ch_retimer[(FIR_TAPS-1)-1 -: FIR_TAPS-1]},i_sample};
  end
   
endmodule
