from typing import TypeAlias

import plotly.graph_objects as go
import plotly.subplots

PlotCoefficientsType: TypeAlias = list[dict[str, str | int | list[float]]]


def minmax(figure: go.Figure):
    """Get minimum and maximum values for all figure traces data."""
    min_x = float("inf")  # Initialize with positive infinity
    max_x = float("-inf")  # Initialize with negative infinity
    min_y = float("inf")  # Initialize with positive infinity
    max_y = float("-inf")  # Initialize with negative infinity

    for trace in figure.data:
        if hasattr(trace, "x") and trace.x:
            min_x = min(min_x, min(trace.x))
            max_x = max(max_x, max(trace.x))
        if hasattr(trace, "y") and trace.y:
            min_y = min(min_y, min(trace.y))
            max_y = max(max_y, max(trace.y))

    return (min_x, max_x, min_y, max_y)


def plot_coefficients(data: PlotCoefficientsType) -> go.Figure:
    """Component bitstream samples plot.

    Example of input data:
        data = [
            {
                "name": "Channel",
                "offset": 1,
                "values": [0.25, 0.5, 1, 0.5, 0.125],
            },
            {
                "name": "Forward",
                "offset": 0,
                "values": [0, 0.25, 0.5, 0.90, 0.1, 0.0, 0.0],
            },
        ]
    """
    fig = plotly.subplots.make_subplots(
        rows=len(data),
        cols=1,
        shared_xaxes=True,
        shared_yaxes=True,
        vertical_spacing=0.01,
    )

    # Plot the output buffers for each component
    for row, trace in enumerate(data, start=1):
        fig.add_trace(
            go.Scatter(
                # x0=trace["offset"],
                x=list(range(trace["offset"], trace["offset"] + len(trace["values"]))),
                y=trace["values"],
                mode="markers",
                name=trace["name"],
                yaxis="y1",
                marker_color="black",
            ),
            row=row,
            col=1,
        )
        fig.update_yaxes(title={"text": rf"$\text{{{trace['name']}}}$"}, col=1, row=row)

    # Add vertical stems for each point in all traces
    for trace in fig.data:
        for x, y in zip(trace.x, trace.y):
            fig.add_shape(
                type="line",
                x0=x,  # x-coordinate of the starting point of the stem (x value of scatter plot)
                x1=x,  # x-coordinate of the ending point of the stem (x value of scatter plot)
                y0=0,  # y-coordinate of the starting point of the stem (fixed at y=0)
                y1=y,  # y-coordinate of the ending point of the stem (y value of scatter plot)
                xref=trace.xaxis,  # x-axis reference ('x' or 'x2' etc.)
                yref=trace.yaxis,  # y-axis reference ('y' or 'y2' etc.)
                line={"color": "black"},  # specify line properties
            )

    _, _, min_y, max_y = minmax(fig)
    fig.update_xaxes(
        title={
            "text": r"$\text{Sample}$",
        },
        col=1,
        row=len(data),
    )
    fig.update_xaxes(
        tickmode="linear",
        gridcolor="#CCC",
        griddash="dot",
        linecolor="#000",
        tickcolor="#000",
        ticklen=6,
        ticks="inside",
        mirror="ticks",
        showline=True,
    )
    fig.update_yaxes(
        title={
            "standoff": 0.05,
        },
        matches="y",
        range=[min_y * 1.1, max_y * 1.1],
        zerolinecolor="#CCC",
        gridcolor="#CCC",
        griddash="dot",
        linecolor="#000",
        tickcolor="#000",
        ticklen=6,
        ticks="inside",
        mirror="ticks",
        showline=True,
    )

    fig.update_layout(
        height=700,
        width=700,
        showlegend=False,
        plot_bgcolor="#ffffff",
        paper_bgcolor="#ffffff",
    )

    fig.show()
    return fig


if __name__ == "__main__":
    pass
