from collections.abc import Callable

import numpy as np
from numfi import numfi

from .component import Component
from .fixed_arithmetic.utils import to_fixed
from .slicer import BPSK
from .wave import Wave


class Quantizer(Component):
    """Quantizer component."""

    def __init__(
        self,
        slicer: Callable[[np.float64], np.float64] = BPSK,
        buffer_length: int = 1,
        **kwargs,
    ) -> None:
        self._set_locals(locals())
        super().__init__(buffer_length=buffer_length, **kwargs)
        self.slicer = slicer

    def run(self, sample: np.float64) -> np.float64:
        self.input.add(sample)
        response = self.slicer(self.input.last)
        self.response.add(response)
        return self.response.last

    @property
    def output(self) -> Wave:
        return self.response

    @property
    def error(self) -> np.float64:
        """Returns the error signal for the last sample."""
        error = self.output.last - self.input.last

        if "error" in self.quantize:
            error = np.float64(numfi(error, **to_fixed(self.quantize["error"])))

        return error

    @property
    def lag(self) -> int:
        return 0
