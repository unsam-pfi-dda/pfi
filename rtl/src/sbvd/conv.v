`include "defines.v"

module conv #(
  parameter COEF_W = `COEF_W,
  parameter ADQ_W  = `ADQ_W
) (

  input  signed [COEF_W-1:0] i_cursor ,  // Cursor
  input  signed [COEF_W-1:0] i_postc1 ,  // Postcursor 1
  input  signed [COEF_W-1:0] i_postc2 ,  // Postcursor 2

  output signed [ADQ_W-1 :0] o_adq_000,  // Ideal adquisition of state 00 -> 00.
  output signed [ADQ_W-1 :0] o_adq_001,  // Ideal adquisition of state 00 -> 01.
  output signed [ADQ_W-1 :0] o_adq_010,  // Ideal adquisition of state 01 -> 10.
  output signed [ADQ_W-1 :0] o_adq_011,  // Ideal adquisition of state 01 -> 11.
  output signed [ADQ_W-1 :0] o_adq_100,  // Ideal adquisition of state 10 -> 00.
  output signed [ADQ_W-1 :0] o_adq_101,  // Ideal adquisition of state 10 -> 01.
  output signed [ADQ_W-1 :0] o_adq_110,  // Ideal adquisition of state 11 -> 10.
  output signed [ADQ_W-1 :0] o_adq_111   // Ideal adquisition of state 11 -> 11.
);

wire signed [ADQ_W-1:0] m_p1_m_p2;
wire signed [ADQ_W-1:0] m_p1_p_p2;
wire signed [ADQ_W-1:0] p_p1_m_p2;
wire signed [ADQ_W-1:0] p_p1_p_p2;

assign m_p1_m_p2 = - i_postc1 - i_postc2;
assign m_p1_p_p2 = i_postc2 - i_postc1;
assign p_p1_m_p2 = i_postc1 - i_postc2;
assign p_p1_p_p2 = i_postc1 + i_postc2;

assign o_adq_000 = m_p1_m_p2 - i_cursor;  // 00 --> 00  =  (-C) + (-P1) + (-P2)
assign o_adq_001 = i_cursor + m_p1_m_p2;  // 00 --> 01  =  ( C) + (-P1) + (-P2)
assign o_adq_010 = p_p1_m_p2 - i_cursor;  // 01 --> 10  =  (-C) + ( P1) + (-P2)
assign o_adq_011 = i_cursor + p_p1_m_p2;  // 01 --> 11  =  ( C) + ( P1) + (-P2)
assign o_adq_100 = m_p1_p_p2 - i_cursor;  // 10 --> 00  =  (-C) + (-P1) + ( P2)
assign o_adq_101 = i_cursor + m_p1_p_p2;  // 10 --> 01  =  ( C) + (-P1) + ( P2)
assign o_adq_110 = p_p1_p_p2 - i_cursor;  // 11 --> 10  =  (-C) + ( P1) + ( P2)
assign o_adq_111 = i_cursor + p_p1_p_p2;  // 11 --> 11  =  ( C) + ( P1) + ( P2)

// *****************************************************************************
endmodule
