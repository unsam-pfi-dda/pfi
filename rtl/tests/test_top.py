"""Toplevel testbench."""

import sys
from pathlib import Path

import cocotb
import numpy as np
import pytest
from cocotb.clock import Clock
from cocotb.handle import Force, HierarchyObject, Release
from cocotb.triggers import ClockCycles, Edge, FallingEdge, RisingEdge, with_timeout
from cocotb_test.simulator import Simulator

sys.path.append(str(Path(__file__).parent.parent.parent.resolve()))
from models.src import DFFE, Channel, Plot, System
from rtl.tests.utils import binstr2float, binstr2list


@pytest.mark.parametrize("depth", ["2", "10"])
def test_top(simulator: Simulator, depth: str):
    """Run parametrized CoCoTB test with pytest."""
    simulator.parameters["D"] = depth
    simulator.run()


@cocotb.test()
async def system_lag(dut: HierarchyObject):
    """Validate system lag with BER position register."""
    await startup(dut)
    dut.rst_n.value = 0
    await RisingEdge(dut.clk)
    dut.rst_n.value = 1
    await RisingEdge(dut.clk)

    while dut.u_ber.state.value != 2:
        await with_timeout(Edge(dut.u_ber.state), 280, "us")

    assert dut.u_ber.position_reg.value == dut.FIR_TAPS.value


@cocotb.test()
async def noise_manipulated_test(dut: HierarchyObject):
    """Test GNG, noise scale with sigma and decimal point shift."""
    await startup(dut)
    dut.rst_n.value = 0
    await RisingEdge(dut.clk)
    dut.rst_n.value = 1
    await RisingEdge(dut.clk)
    results_awgn = []
    results_sigma = []
    results_man = []
    for _ in range(50_000):
        await FallingEdge(dut.clk)
        if "x" in dut.awgn.value.binstr:
            continue
        results_awgn.append(binstr2float(binstr=dut.awgn.value.binstr, fractional=11))
        results_sigma.append(binstr2float(binstr=dut.noise.value.binstr, fractional=18))
        results_man.append(
            binstr2float(binstr=dut.noise_manipulated.value.binstr, fractional=7)
        )

    data = [
        {"name": "GNG output", "values": [results_awgn]},
        {"name": f"AWGN for SNR={dut.snr_sel.value}", "values": [results_sigma]},
        {"name": "Shifted decimal position", "values": [results_man]},
    ]
    Plot.to_file("histogram", data)


@cocotb.test()
async def signal_path(dut: HierarchyObject):
    """Test sequence from PRBS (forced) output up to BER input."""
    await startup(dut)

    dut.u_prbs.o_z.value = Force(0)
    await ClockCycles(dut.clk, 1_000)
    for value in (1, 0, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 1):
        dut.u_prbs.o_z.value = Force(value)
        await RisingEdge(dut.clk)
    dut.u_prbs.o_z.value = Force(0)
    await ClockCycles(dut.clk, 1_000)
    dut.u_prbs.o_z.value = Release()


@cocotb.test()
async def model_cosimulation(dut: HierarchyObject):
    """Cosimulate RTL and model and compare results."""
    iterations = 100_000

    await startup(dut)

    # Pre-configure BER module, skip calibration
    dut.u_ber.state.value = 2
    dut.u_ber.position_reg.value = dut.FIR_TAPS.value

    # Generate the golden model using RTL parameters.
    coefficients = [
        binstr2float(
            binstr,
            fractional=dut.u_channel.FIR_NB_F.value,
            binaryRepresentation=cocotb.binary.BinaryRepresentation.TWOS_COMPLEMENT,
        )
        for binstr in binstr2list(dut.coefficients.value.binstr, bits=dut.FIR_NB.value)
    ]

    system = System(
        symbols=(-1, 1),
        components=[
            Channel(taps=coefficients, snr=int(dut.snr_sel.value)),
            DFFE(
                taps=dut.u_sbvd.COEFFICIENTS.value - 1,
                iterations=1,
                mlse_depth=dut.D.value,
                mlse_taps=dut.u_sbvd.COEFFICIENTS.value - 1,
                adaptive=False,
            ),
        ],
    )
    system.feedback.mlse_main_tap = system.channel.coef[0]
    system.feedback.coef = system.channel.coef[1:]

    # Purge buffers
    for _ in range(-system.lag):
        system.run()
    errors = 0
    for _ in range(iterations):
        system.run()
        errors += system.is_error
    golden = errors / iterations

    await ClockCycles(dut.clk, iterations)
    await FallingEdge(dut.clk)
    dut.ber_read.value = 1
    await FallingEdge(dut.clk)

    result = int(dut.o_errors.value) / int(dut.o_counter.value)
    np.testing.assert_allclose(result, golden, rtol=5, atol=0.005)


async def startup(dut: HierarchyObject):
    """Initialize clock and modules."""
    cocotb.start_soon(Clock(dut.clk, 1, units="ns").start())
    dut.enable.value = 1
    dut.ch_sel.value = 1
    dut.snr_sel.value = 10
    dut.ber_read.value = 0
    await RisingEdge(dut.clk)
    dut.rst_n.value = 0
    await ClockCycles(dut.clk, 1)
    dut.rst_n.value = 1
    await RisingEdge(dut.clk)
