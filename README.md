[[_TOC_]]
# unsam-pfi-dda

### Proyecto final integrador de UNSAM & DDA  

__Alumnos__:  
  - Prudente, Tomás Vicente  
  - Wörn, Ignacio  

__Tutor__:  
  - Dr. Ariel Pola 


## Set up environment (python3.11)

```shell
sudo add-apt-repository ppa:deadsnakes/ppa
sudo apt install python3.11 python3.11-distutils python3.11-dev iverilog
python3.11 -m pip install -r requirements.txt -r models/requirements.txt
```

### Block diagram

```mermaid
graph LR
  BIT(BPSK Antipodal Bitstream) --> A
  A[Tx - Freq compression] .-> B(LPF Channel)
  B .-> C[Rx - Adaptive Filter]
  C --> Slicer
```

### Tx
  - Sends the bitstream to the channel
  - By now, a frequency compressor is not implemented
  - 1 baud == 1 bit/s

### Channel
  - Channel where the signal will be transmited through
  - Is a LPF with AWGN, but by now is an ideal channel.

### Rx
  - Has an adaptive filter
  - A slicer will be implemented to decide if the received value is 1 or 0
  
### Tests

```
python3.11 -m pytest ./rtl/tests/test_sbvd.py -k 2
```


### Synthesis toolchanin

Install docker in WSL2, follow this guide: https://dev.to/bowmanjd/install-docker-on-windows-wsl-without-docker-desktop-34m9


Install dependencies 


```shell
sudo apt install --no-install-recommends apt-transport-https ca-certificates curl gnupg2
```

Reconfigure iptables, select `iptables legacy` : 

```shell
sudo update-alternatives --config iptables
```

Install docker

```shell
. /etc/os-release
curl -fsSL https://download.docker.com/linux/${ID}/gpg | sudo tee /etc/apt/trusted.gpg.d/docker.asc
echo "deb [arch=amd64] https://download.docker.com/linux/${ID} ${VERSION_CODENAME} stable" | sudo tee /etc/apt/sources.list.d/docker.list
sudo apt update
sudo apt install -y docker-ce docker-ce-cli containerd.io
sudo usermod -aG docker $USER
```

And finally restart WSL, from a powershell terminal run `wsl --shutdown`.


Pull and run the F4PGA docker image for Xilinx XC7-A50T series (yes, includes A35T architecture definitions) (source: https://hdl.github.io/containers/ToolsAndImages.html).
Make sure to run from the  project root directory:

```shell
docker run --rm -it -v /$(pwd)://wrk -w //wrk gcr.io/hdl-containers/conda/f4pga/xc7/a50t
```

From the docker shell, navigate to `./syn/` folder and run the Makefile:

```shell
cd syn
make
```

This process can take a couple of minutes, once done the final bitstream will be located at `./syn/build/` folder