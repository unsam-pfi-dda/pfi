"""Unit test for SQNR simulation core module."""

import itertools

import pytest
from numfi import numfi

from src import FFE, Channel, Quantizer, System
from src.sims.core import SQNRTestDirector


@pytest.fixture(name="system")
def fixture_system():
    """Create a System object."""
    return System(
        components=[
            Channel(taps=[0, 0.5, 1, 0, 0]),
            FFE(taps=5),
            Quantizer(),
        ],
    )


@pytest.mark.parametrize("sample", [-1, 1])
def test_instantiation(system: System, sample: int):
    """Test that SQNR Director creates tests correctly."""
    director = SQNRTestDirector(
        adapt=1000,
        measure=100,
        system=system,
        reference=system.forward,
    )

    test = director.add_test(
        name="test",
        quantize={"input": "s0/0-z/s"},
        sweep="input",
        integer=[0, 1],
        fractional=[1, 2],
    )
    assert director.tests

    system.run(sample)
    test.run(measure=True)

    assert not isinstance(system.forward.input.values, numfi)

    for integer, fractional in itertools.product(test.integer, test.fractional):
        w = 1 + integer + fractional
        f = fractional
        assert isinstance(test._components[integer][fractional].input.values, numfi)
        assert test._components[integer][fractional].locals["quantize"] == {
            "input": f"s{w}/{f}-z/s"
        }

        quantized = numfi(
            [system.forward.input.last],
            s=1,
            w=w,
            f=f,
            rounding="zero",
            overflow="saturate",
            fixed=True,
        )
        dut: numfi = test._components[integer][fractional].input.values

        assert quantized[-1] == test._components[integer][fractional].input.last

        assert quantized.s == dut.s
        assert quantized.w == dut.w
        assert quantized.f == dut.f
        assert quantized.rounding == dut.rounding
        assert quantized.overflow == dut.overflow
        assert quantized.fixed == dut.fixed
