`include "defines.v"

module sliding_block
  #(
    parameter COEF_W   = `COEF_W,
    parameter ADQ_W    = `ADQ_W,
    parameter SM_WIDTH = `SM_WIDTH,
    parameter SAMP_W   = `SAMP_W,
    parameter BM_WIDTH = `BM_WIDTH,

    parameter LAG_SYMBOL = 0,
    parameter LAG_DECISION = 0,
    parameter LAG_DATA = 0,
    parameter DIRECTION = 0, // 0=Forward; 1=Backward
    parameter DECODE = 1  // 0=Sync; 1=Decode (sync does not output decision signal)
  ) (

    input clk,
    input rst_n,
    input enable,

    input  signed [SAMP_W-1: 0]     i_symbol,
    input         [4*SM_WIDTH-1: 0] i_state_metric,  // {w4, w3, w2, w1}
    input  signed [2*4*ADQ_W-1: 0]  i_branch_distance,
    input         [1:0]             i_state_estimate,

    output signed [4*SM_WIDTH-1: 0] o_state_metric,
    output        [1:0]             o_state_estimate,
    output                          o_data
  );

wire signed   [SAMP_W-1:0] symbol_reg              ;
wire [       3:0] decisions               ;
wire [       3:0] decisions_reg           ;
wire [       1:0] traceback_state_estimate;
wire              data                    ;

fifo #(
  .WIDTH(SAMP_W    ),
  .DELAY (LAG_SYMBOL)
) u_symbol_fifo (
  .clk     (clk       ),
  .enable  (enable    ),
  .rst_n   (rst_n     ),
  .data_in (i_symbol  ),
  .data_out(symbol_reg)
);

vtb_branch #(
  .COEF_W   (COEF_W   ),
  .ADQ_W    (ADQ_W    ),
  .SM_WIDTH (SM_WIDTH ),
  .SAMP_W   (SAMP_W   ),
  .BM_WIDTH (BM_WIDTH ),
  .DIRECTION(DIRECTION)
) u_vtb_branch (
  .clk      (clk                                     ),
  .rst_n    (rst_n                                   ),
  .enable   (enable                                  ),
  .i_sm_00  (i_state_metric[SM_WIDTH*(0) +: SM_WIDTH]),
  .i_sm_01  (i_state_metric[SM_WIDTH*(1) +: SM_WIDTH]),
  .i_sm_10  (i_state_metric[SM_WIDTH*(2) +: SM_WIDTH]),
  .i_sm_11  (i_state_metric[SM_WIDTH*(3) +: SM_WIDTH]),
  .i_adq_000(i_branch_distance[ADQ_W*(0) +: ADQ_W]   ),
  .i_adq_001(i_branch_distance[ADQ_W*(1) +: ADQ_W]   ),
  .i_adq_010(i_branch_distance[ADQ_W*(2) +: ADQ_W]   ),
  .i_adq_011(i_branch_distance[ADQ_W*(3) +: ADQ_W]   ),
  .i_adq_100(i_branch_distance[ADQ_W*(4) +: ADQ_W]   ),
  .i_adq_101(i_branch_distance[ADQ_W*(5) +: ADQ_W]   ),
  .i_adq_110(i_branch_distance[ADQ_W*(6) +: ADQ_W]   ),
  .i_adq_111(i_branch_distance[ADQ_W*(7) +: ADQ_W]   ),
  .i_sample (symbol_reg                              ),
  .o_sm_00  (o_state_metric[SM_WIDTH*(0) +: SM_WIDTH]),
  .o_sm_01  (o_state_metric[SM_WIDTH*(1) +: SM_WIDTH]),
  .o_sm_10  (o_state_metric[SM_WIDTH*(2) +: SM_WIDTH]),
  .o_sm_11  (o_state_metric[SM_WIDTH*(3) +: SM_WIDTH]),
  .o_dec_00 (decisions[0]                            ),
  .o_dec_01 (decisions[1]                            ),
  .o_dec_10 (decisions[2]                            ),
  .o_dec_11 (decisions[3]                            )
);

if (DECODE) begin
  fifo #(
    .WIDTH(4),
    .DELAY(LAG_DECISION)
  ) u_decision_fifo (
    .clk (clk),
    .enable (enable),
    .rst_n (rst_n),
    .data_in (decisions),
    .data_out (decisions_reg)
  );

  traceback #(.DIRECTION(DIRECTION)) u_traceback (
    .i_state    (i_state_estimate        ),
    .i_decisions(decisions_reg           ),
    .o_state    (traceback_state_estimate),
    .o_data     (data                    )
  );

  fifo #(
    .WIDTH(2),
    .DELAY (1)
  ) u_state_estimate_fifo (
    .clk     (clk                     ),
    .enable  (enable                  ),
    .rst_n   (rst_n                   ),
    .data_in (traceback_state_estimate),
    .data_out(o_state_estimate        )
  );

  fifo #(
    .WIDTH(1       ),
    .DELAY (LAG_DATA)
  ) u_data_fifo (
    .clk     (clk   ),
    .enable  (enable),
    .rst_n   (rst_n ),
    .data_in (data  ),
    .data_out(o_data)
  );
end
endmodule
