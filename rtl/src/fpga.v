`include "defines.v"

module fpga
  #(
    parameter PARALLELISM = `PARALLELISM,
    parameter COEF_W      = `COEF_W,  // SBVD coefficients width
    parameter FIR_TAPS    = `FIR_TAPS,
    parameter SAMP_W      = `SAMP_W,
    parameter D = 10,   // Viterbi processor iterations.
    parameter M = 2*D,  // Viterbi processor depth (forward + backward)
    parameter SIGMA_W = 7,
    parameter NOISE_W = 23,
    parameter NB_BER_ERROR = `NB_BER_ERROR
  ) (
    input clk
  );

    wire enable;
    wire rst_n;
    wire [1:0] ch_sel;
    wire [3:0] snr_sel;
    wire ber_read;
    wire [NB_BER_ERROR-1:0] o_errors;
    wire [NB_BER_ERROR-1:0] o_counter;
    wire o_ready;

    // clock divider 125MHz -> 100MHz
    wire clk_100;
    clk_wiz u_clk_div(
      .clk_in1(clk),
      .clk_out1(clk_100)
    );

  top #(
    .PARALLELISM(PARALLELISM),
    .COEF_W(COEF_W),
    .FIR_TAPS(FIR_TAPS),
    .SAMP_W(SAMP_W),
    .D(D),
    .M(M),
    .SIGMA_W(SIGMA_W),
    .NOISE_W(NOISE_W),
    .NB_BER_ERROR(NB_BER_ERROR)
  ) u_top (
    .clk(clk_100),
    .rst_n(rst_n),
    .enable(enable),
    .ch_sel(ch_sel),
    .snr_sel(snr_sel),
    .ber_read(ber_read),
    .o_ready(o_ready),
    .o_errors(o_errors),
    .o_counter(o_counter)
  );

  vio #(
  ) u_vio (
    .clk(clk_100),
    .probe_out0(rst_n),
    .probe_out1(enable),
    .probe_out2(ch_sel),
    .probe_out3(snr_sel),
    .probe_out4(ber_read),
    .probe_in0(o_ready),
    .probe_in1(o_errors),
    .probe_in2(o_counter)
  );

endmodule