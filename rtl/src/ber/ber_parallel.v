// `include "defines.v"

// module ber_parallel #(
//     parameter PARALLELISM      = `PARALLELISM,
//     parameter NB_BER_ERROR     = `NB_BER_ERROR
// ) (
//     input                          i_clock          ,  // Clock
//     input                          i_rst_n          ,  // Reset - active low
//     input                          i_enb            ,  // Module enable
//     input  [PARALLELISM-1:0]       i_prbs           ,  // PBRS (golden)
//     input  [PARALLELISM-1:0]       i_system         ,  // System output (real)
//     input                          i_ber_read       ,  // Rise this input when want to read the BER Counter
//     output                         o_check_enable   ,  // BER count is enabled - Will be low when can't count more
//     output [ NB_BER_ERROR   -1:0]  o_checked_counter,  // Total samples taken into account (4 channels)    // LocalParam
//     output [ NB_BER_ERROR   -1:0]  o_total_errors      // Total errors
// )


// endmodule