"""FIFO Synchronic RAM - module testbench"""

import sys
from pathlib import Path
import cocotb
from cocotb.triggers import FallingEdge, RisingEdge, ClockCycles
from cocotb.clock import Clock
from cocotb.handle import HierarchyObject
from cocotb_test.simulator import Simulator
import pytest
import numpy as np

sys.path.append(str(Path(__file__).parent.parent.parent.resolve()))
from rtl.tests.utils import list2binstr


@pytest.mark.parametrize("width", ("1", "2", "8", "24"))
@pytest.mark.parametrize("delay", ("0", "1", "5", "500"))
def test_fifo(simulator: Simulator, width: int, delay: int):
    """Run parametrized CoCoTB test with pytest"""
    simulator.parameters["WIDTH"] = width
    simulator.parameters["delay"] = delay
    simulator.run()


async def read_write(dut: HierarchyObject, sequence: list[str]):
    """Coroutine for reading and writing data into the FIFO."""
    for data_in in sequence:
        await FallingEdge(dut.clk)
        dut.data_in.value = data_in
        await RisingEdge(dut.clk)
        data_out: str = dut.data_out.value.binstr
        yield data_out


@cocotb.test()
async def model_cosimulation(dut: HierarchyObject):
    """Cosimulate RTL and model and compare results"""
    await startup(dut)
    width: int = dut.WIDTH.value
    delay: int = dut.DELAY.value

    sequence = [
        list2binstr(np.random.choice([0, 1], size=width))
        for _ in range(10 * (delay if delay else 10))
    ]

    results = [data async for data in read_write(dut, sequence)]

    # Align results buffer (drops starting X states)
    results, sequence = zip(*zip(results[delay:], sequence))
    np.testing.assert_array_equal(results, list(map(str, sequence)))


async def startup(dut: HierarchyObject):
    cocotb.start_soon(Clock(dut.clk, 1, units="ns").start())
    dut.enable.value = 1
    dut.rst_n.value = 0
    await ClockCycles(dut.clk, 1)
    dut.rst_n.value = 1
    await RisingEdge(dut.clk)
