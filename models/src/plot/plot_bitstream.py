from typing import TypeAlias

import plotly.graph_objects as go
import plotly.subplots

PlotBitstreamType: TypeAlias = list[dict[str, str | int | list[float]]]


def minmax(figure: go.Figure):
    """Get minimum and maximum values for all figure traces data."""
    min_x = float("inf")  # Initialize with positive infinity
    max_x = float("-inf")  # Initialize with negative infinity
    min_y = float("inf")  # Initialize with positive infinity
    max_y = float("-inf")  # Initialize with negative infinity

    for trace in figure.data:
        if hasattr(trace, "x") and trace.x:
            min_x = min(min_x, min(trace.x))
            max_x = max(max_x, max(trace.x))
        if hasattr(trace, "y") and trace.y:
            min_y = min(min_y, min(trace.y))
            max_y = max(max_y, max(trace.y))

    return (min_x, max_x, min_y, max_y)


def plot_bitstream(data: PlotBitstreamType) -> go.Figure:
    """Component bitstream samples plot.

    Example of input data:
        data = [
            {
                "name": "Channel",
                "lag": 1,
                "input": [-1, 1, 1, -1],
                "output": [0, -0.95, 0.85, 1.05, 0.22],
            },
            {
                "name": "FFE",
                "lag": 1,
                "input": [0, -0.95, 0.85, 1.05, 0.22],
                "output": [0, 0, -0.95, 0.90, 1, 0.22],
            },
        ]
    """
    fig = plotly.subplots.make_subplots(
        rows=len(data) + 1,
        cols=1,
        shared_xaxes=True,
        shared_yaxes=True,
        vertical_spacing=0.01,
    )

    # Plot the first input buffer
    input_buffer: list[int] = data[0]["input"]
    x_values = list(range(len(input_buffer)))
    fig.add_trace(
        go.Scatter(
            x=x_values,
            y=input_buffer,
            name="PRBS",
            mode="markers",
            marker_color="black",
        ),
        row=1,
        col=1,
    )
    fig.update_yaxes(title={"text": r"$\text{PRBS}$"})

    # Plot the output buffers for each component
    lag = 0
    for row, trace in enumerate(data, start=2):
        lag += trace["lag"]
        fig.add_trace(
            go.Scatter(
                x=list(range(-lag, len(trace["output"]) - lag)),
                y=trace["output"],
                mode="markers",
                name=trace["name"],
                yaxis="y1",
                marker_color="black",
            ),
            row=row,
            col=1,
        )
        fig.update_yaxes(title={"text": rf"$\text{{{trace['name']}}}$"}, col=1, row=row)

    # Add vertical stems for each point in all traces
    for trace in fig.data:
        for x, y in zip(trace.x, trace.y):
            fig.add_shape(
                type="line",
                x0=x,  # x-coordinate of the starting point of the stem (x value of scatter plot)
                x1=x,  # x-coordinate of the ending point of the stem (x value of scatter plot)
                y0=0,  # y-coordinate of the starting point of the stem (fixed at y=0)
                y1=y,  # y-coordinate of the ending point of the stem (y value of scatter plot)
                xref=trace.xaxis,  # x-axis reference ('x' or 'x2' etc.)
                yref=trace.yaxis,  # y-axis reference ('y' or 'y2' etc.)
                line={"color": "black"},  # specify line properties
            )

    _, _, min_y, max_y = minmax(fig)
    total_lag = sum(x["lag"] for x in data)
    fig.update_xaxes(
        title={
            "text": r"$\text{Sample}$",
        },
        tickmode="linear",
        gridcolor="#CCC",
        griddash="dot",
        linecolor="#000",
        tickcolor="#000",
        ticklen=6,
        ticks="inside",
        mirror="ticks",
        showline=True,
        range=[0, len(x_values) - total_lag - 1],
    )
    limits = max(abs(min_y), abs(max_y)) * 1.1
    fig.update_yaxes(
        title={
            "standoff": 0.05,
        },
        matches="y",
        range=[-limits, limits],
        zerolinecolor="#CCC",
        gridcolor="#CCC",
        griddash="dot",
        linecolor="#000",
        tickcolor="#000",
        ticklen=6,
        ticks="inside",
        mirror="ticks",
        showline=True,
    )

    fig.update_layout(
        height=1000,
        width=1000,
        showlegend=False,
        plot_bgcolor="#ffffff",
        paper_bgcolor="#ffffff",
    )

    fig.show()
    return fig


if __name__ == "__main__":
    pass
