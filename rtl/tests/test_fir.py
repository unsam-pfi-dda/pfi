"""Finite impulse response module testbench"""

from typing import Sequence
import sys
from pathlib import Path
from itertools import product, chain
import numpy as np
import cocotb
from cocotb.triggers import FallingEdge, RisingEdge, ClockCycles
from cocotb.clock import Clock
from cocotb.handle import HierarchyObject
from cocotb_test.simulator import Simulator


sys.path.append(str(Path(__file__).parent.parent.parent.resolve()))

from models.src import System, Channel, Quantizer
from rtl.tests.utils import binstr2list, list2binstr, float2binstr

SYMBOLS = (-1, 1)

SYMBOLS = (-1, 1)


def test_fir(simulator: Simulator):
    """Run parametrized CoCoTB test with pytest"""
    simulator.run()


async def startup(dut: HierarchyObject):
    """Startup sequence"""
    cocotb.start_soon(Clock(dut.clk, 1, units="ns").start())
    dut.rst_n.value = 0
    dut.enable.value = 1
    dut.i_coef.value = 0
    dut.i_sample.value = 0
    await FallingEdge(dut.clk)
    dut.rst_n.value = 1
    await RisingEdge(dut.clk)


async def model_cosimulation(dut: HierarchyObject, channel: Sequence[int]):
    """Cosimulate RTL and model and compare results"""
    await startup(dut)

    taps: int = dut.FIR_TAPS.value
    assert len(channel) == taps

    bits_coef: int = dut.FIR_NB.value
    dut.i_coef.value = list2binstr(channel, bits=bits_coef)
    dut.i_sample.value = 0

    system = System((-1, 1), [Channel(taps=taps)])
    system.channel.coef = np.array(channel)

    await RisingEdge(dut.clk)

    np.testing.assert_array_equal(
        system.channel.coef, binstr2list(dut.i_coef.value.binstr, bits=bits_coef)
    )

    for sample in chain.from_iterable(product(SYMBOLS, repeat=taps)):
        golden = system.run(sample)
        buffer = [0 if v == SYMBOLS[0] else 1 for v in system.channel.input.values]
        dut.i_sample.value = list2binstr(buffer, bits=1)
        await FallingEdge(dut.clk)
        assert golden == dut.o_sample.value.signed_integer

    for _ in range(taps):
        golden = system.run(SYMBOLS[0])
        buffer = [0 if v == SYMBOLS[0] else 1 for v in system.channel.input.values]
        dut.i_sample.value = list2binstr(buffer, bits=1)
        await FallingEdge(dut.clk)
        assert golden == dut.o_sample.value.signed_integer


tf = cocotb.regression.TestFactory(model_cosimulation)
tf.add_option(
    "channel",
    [
        [0, 1, 0],
        [1, 0, 0],
        [0, 0, 1],
        [4, 2, 1],
        [0b01110000, 0b00111000, 0b00011100],  # [4, 2, 1]
        [2, 2, 0],
        [0b00111000, 0b00111000, 0b00011100],  # [2, 2, 1]
    ],
)
tf.generate_tests()
