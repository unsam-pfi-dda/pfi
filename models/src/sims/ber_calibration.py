"""BET Calibration atest.

This test will measure the Bit Error Rate for different system configurations

How to run:
    python -m src.sims ber calibration

The results will be sent to the Plot module (./src/Plot) for storing and plotting with
plotly.
"""
from typing import TypeVar

from src import Channel, System

from .core import BerTestDirector

T = TypeVar("T")


def ber_calibration():
    config = "calibration.yaml"
    tests = BerTestDirector()
    sweep = "snr"
    sweep_range_no_channel = range(4, 13)
    sweep_range_channel = range(4, 16)
    reference = Channel
    adapt_samples = 100_000  # how many samples to adapt

    # # Test: No channel, no components
    # def modifier_no_components(cfg: T) -> T:
    #     """Modifier for System configuration file for testing: No channel, no components."""
    #     cfg["Components"]["Channel"]["taps"] = [1]
    #     cfg["Components"]["DFFE"]["mlse_depth"] = 0
    #     del cfg["Components"]["AGC"]
    #     del cfg["Components"]["DFFE"]
    #     del cfg["Components"]["FFE"]
    #     return cfg

    # no_comps = System.from_yaml(config, modifier_no_components)
    # tests.add_test(
    #     "Slicer (No channel)",
    #     no_comps,
    #     sweep,
    #     sweep_range_no_channel,
    #     reference,
    #     100,
    #     "circle",
    # )

    # # Test: No channel, agc + ffe + dfe + lms
    # def modifier_agc_ffe_dfe_lms(cfg: T) -> T:
    #     """Modifier for System configuration file for testing: No channel, agc + ffe + dfe + lms."""
    #     cfg["Components"]["Channel"]["taps"] = [1]
    #     cfg["Components"] = {
    #         "DFE" if k == "DFFE" else k: v for k, v in cfg["Components"].items()
    #     }
    #     del cfg["Components"]["DFE"]["iterations"]
    #     del cfg["Components"]["DFE"]["mlse_iterations"]
    #     del cfg["Components"]["DFE"]["mlse_depth"]
    #     del cfg["Components"]["DFE"]["mlse_taps"]
    #     return cfg

    # agc_ffe_dfe_lms = System.from_yaml(config, modifier_agc_ffe_dfe_lms)
    # tests.add_test(
    #     "DFE (No channel)",
    #     agc_ffe_dfe_lms,
    #     sweep,
    #     sweep_range_no_channel,
    #     reference,
    #     adapt_samples,
    #     "circle",
    # )

    # # Test: No channel, agc + ffe + dffe + lms
    # def modifier_agc_ffe_dffe_lms(cfg: T) -> T:
    #     """Modifier for System configuration file for testing: No channel, agc + ffe + dffe + lms."""
    #     cfg["Components"]["Channel"]["taps"] = [1]
    #     cfg["Components"]["DFFE"]["mlse_iterations"] = 1
    #     cfg["Components"]["DFFE"]["mlse_depth"] = 0
    #     return cfg

    # agc_ffe_dffe_lms = System.from_yaml(config, modifier_agc_ffe_dffe_lms)
    # tests.add_test(
    #     "DFFE (No channel)",
    #     agc_ffe_dffe_lms,
    #     sweep,
    #     sweep_range_no_channel,
    #     reference,
    #     adapt_samples,
    #     "circle",
    # )

    # # Test: No channel, agc + ffe + dffe-va + lms
    # def modifier_agc_ffe_dffeva_lms(cfg: T) -> T:
    #     """Modifier for System configuration file for testing: No channel, agc + ffe + dffe-va + lms."""
    #     cfg["Components"]["Channel"]["taps"] = [1]
    #     return cfg

    # agc_ffe_dffeva_lms = System.from_yaml(config, modifier_agc_ffe_dffeva_lms)
    # tests.add_test(
    #     "DFFE-VA (No channel)",
    #     agc_ffe_dffeva_lms,
    #     sweep,
    #     sweep_range_no_channel,
    #     reference,
    #     adapt_samples,
    #     "circle",
    # )

    # Test: With channel, no components
    def modifier_no_components(cfg: T) -> T:
        """Modifier for System configuration file for testing: No channel, no components."""
        del cfg["Components"]["AGC"]
        del cfg["Components"]["DFFE"]
        del cfg["Components"]["FFE"]
        return cfg
    no_comps = System.from_yaml(config, modifier_no_components)
    tests.add_test(
        "Slicer",
        no_comps,
        sweep,
        sweep_range_channel,
        reference,
        100,
        "circle",
    )

    # # Test: With channel, agc + ffe + dfe + lms
    # def modifier_reference(cfg: T) -> T:
    #     """Modifier for System configuration file for testing: With channel, agc + ffe + dfe + lms."""
    #     cfg["Components"] = {
    #         "DFE" if k == "DFFE" else k: v for k, v in cfg["Components"].items()
    #     }
    #     del cfg["Components"]["DFE"]["iterations"]
    #     del cfg["Components"]["DFE"]["mlse_iterations"]
    #     del cfg["Components"]["DFE"]["mlse_depth"]
    #     del cfg["Components"]["DFE"]["mlse_taps"]
    #     return cfg

    # dfe_reference = System.from_yaml(config, modifier_reference)
    # tests.add_test(
    #     "DFE",
    #     dfe_reference,
    #     sweep,
    #     sweep_range_channel,
    #     reference,
    #     adapt_samples,
    #     "star",
    # )

    # # Test: With channel, agc + ffe + dffe + lms
    # def modifier_no_viterbi(cfg: T) -> T:
    #     """Modifier for System configuration file for testing: With channel, agc + ffe + dffe + lms."""
    #     cfg["Components"]["DFFE"]["mlse_iterations"] = 1
    #     cfg["Components"]["DFFE"]["mlse_depth"] = 0
    #     return cfg

    # no_viterbi = System.from_yaml(config, modifier_no_viterbi)
    # tests.add_test(
    #     "DFFE",
    #     no_viterbi,
    #     sweep,
    #     sweep_range_channel,
    #     reference,
    #     adapt_samples,
    #     "circle",
    # )

    # # Test: With channel, agc + ffe + dffe-va + lms
    # viterbi = System.from_yaml(config)
    # tests.add_test(
    #     "DFFE-VA",
    #     viterbi,
    #     sweep,
    #     sweep_range_channel,
    #     reference,
    #     adapt_samples,
    #     "circle",
    # )

    return tests.run()
