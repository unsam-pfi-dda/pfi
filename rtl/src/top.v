`include "defines.v"

module top
  #(
    parameter PARALLELISM = `PARALLELISM,
    parameter COEF_W      = `COEF_W,  // SBVD coefficients width
    parameter FIR_TAPS    = `FIR_TAPS,
    parameter SAMP_W      = `SAMP_W,

    parameter D = 10,   // Viterbi processor iterations.
    parameter M = 2*D,  // Viterbi processor depth (forward + backward)
    parameter SIGMA_W = 7,
    parameter NOISE_W = 23,

    parameter NB_BER_ERROR = `NB_BER_ERROR
  ) (

    input                            clk   ,
    input                            rst_n ,
    input                            enable,

    input wire [1:0]                 ch_sel,
    input wire [3:0]                 snr_sel,
    input wire                       ber_read,
    output [NB_BER_ERROR-1:0]        o_errors,
    output [NB_BER_ERROR-1:0]        o_counter,
    output o_ready

  );

// muestra es s(10,7)
localparam FIR_NB = `FIR_NB;  // FIR Coeff width
localparam NB_DOT_COEFF = `NB_DOT_COEFF;
localparam DELAY = (((2 * D - 1) + 1 + 3 + D ) * 2 + 1) * D;

wire                            bitstream    ;
wire [NB_DOT_COEFF-1       : 0] sample       ;
wire [SAMP_W-1             : 0] noisy_sample ;
wire [M-1:0]                    out          ;
reg  [FIR_TAPS*FIR_NB-1: 0]     coefficients ;    // Coeficientes s(8,7)
wire signed [    15:0]          awgn  ;
reg  [     4:0]                 counter      ;
reg  [M*SAMP_W-1:0]             sbvd_input   ;
wire                            awgn_valid   ;
wire                            ena_gated    ;
wire [5     :0]                 count_rg     ;

wire [SIGMA_W - 1 : 0]          sigma_scale [0:15];
wire [SIGMA_W - 1 : 0]          sigma;
wire signed [NOISE_W - 1 : 0]   noise;
wire [9 : 0]                    noise_manipulated;

// (Fake) Reduce the delay
reg [$clog2(DELAY) : 0] ber_delay_counter;
wire       ber_enable;
wire       i_transmitted;
// ----------

always @(*) begin
  case(ch_sel)
    2'b00: coefficients = {8'b01010101, 8'b01010101, 8'b00101011};  // 2 2 1
    2'b01: coefficients = {8'b01110000, 8'b00111000, 8'b00011100};  // 4 2 1
    2'b10: coefficients = {8'b01111111, 8'd0, 8'd0};  // 0.9999 0 0
    default: coefficients = {8'b10000000, 8'd0, 8'd0};  // -1 0 0
  endcase
end

assign sigma_scale[0] = 7'd0;
assign sigma_scale[1] = 7'd0;
assign sigma_scale[2] = 7'd0;
assign sigma_scale[3] = 7'd0;
assign sigma_scale[4] = 7'b0111001;
assign sigma_scale[5] = 7'b0110011;
assign sigma_scale[6] = 7'b0101101;
assign sigma_scale[7] = 7'b0101000;
assign sigma_scale[8] = 7'b0100100;
assign sigma_scale[9] = 7'b0100000;
assign sigma_scale[10] = 7'b0011101;
assign sigma_scale[11] = 7'b0011010;
assign sigma_scale[12] = 7'b0010111;
assign sigma_scale[13] = 7'b0010100;
assign sigma_scale[14] = 7'b0010010;
assign sigma_scale[15] = 7'b0010000;

assign sigma = sigma_scale[snr_sel];
assign noise = awgn * $signed(sigma);    // awgn s(16,11) * sigma_scale u(7,7) = noise s(23,18)

// queremos llevar s(23, 18) a s(10,7)
// 22 21 20 19 18   17 16 15 14 13 12 11 10 09 08 07 06 05 04 03 02 01 00
// __ __ __ __ __ , __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __
//       __ __ __ , __ __ __ __ __ __ __
assign noise_manipulated = {noise[20], noise[19-:9]};
assign noisy_sample = sample + noise_manipulated;

prbs9 u_prbs (
  .i_clock(clk      ),
  .i_rst_n(rst_n    ),
  .i_enb  (enable   ),
  .o_z    (bitstream)
);

fir_parallel #(
  .PARALLELISM(1)
) u_channel (
    .clk(clk),
    .rst_n(rst_n),
    .enable(enable),
    .i_sample(bitstream),
    .i_coef(coefficients),
    .o_sample(sample)
);

gng u_gng (
  .clk       (clk       ),               
  .rstn      (rst_n     ),              
  .ce        (1'b1      ),                
  .valid_out (awgn_valid),        
  .data_out  (awgn      )  // s(16,11) bits noisy output
);

// Con los coeff normalizados, calcular el sigma para el SNR que queremos
// Con ese sigma u(7,7), multiplicarlo por el ruido s(16,11)  --> s(23,18)
// Recordar que en la multiplicación binaria, para evitar overflow:
// a x b = c
// len(a) + len(b) = len(c) 
// Y la coma se maneja como en una multiplicación en base 10,
// sumando la cantidad de decimales de ambos factores
// Por ejemplo:
// s(16, 11) * u(7, 7) = s(23, 18)
// Como sigma siempre va a ser menor a 1, ésta es una sobre estimación


// Ir poniendo cada sample en una posición de memoria, y que después salga todo paralelo
always @(posedge clk or negedge rst_n) begin
  if (!rst_n) begin
    sbvd_input   <= {M*SAMP_W{1'b0}};
  end
  else begin
    sbvd_input   <= {sbvd_input[(M-1)*SAMP_W-1:0], noisy_sample};
  end
end

clock_enable #(
  .DIVIDER(M)
) u_clock_enable (
  .clk      (clk      ),
  .rst_n    (rst_n    ),
  .count_rg (count_rg ),
  .clk_en_rg(ena_gated)
);

sbvd #(
  .D(D),
  .SAMP_W(SAMP_W)
) u_sbvd (
  .clk     (clk         ),
  .rst_n   (rst_n       ),
  .enable  (ena_gated   ),
  .i_sample(sbvd_input  ),  // samples + awgn
  .i_coef  (coefficients),
  .o_data  (out         )
);

// El SBVD lag es:
// lag = -((2 * D - 1) + 1 + 3 + D  * 2 + 1) * D
// Retrasar lo que llega a i_transmitted
// Y hay que retrasar el enable del ber la misma cantidad de clocks (contador hasta sbvd_lag)
always @(posedge clk or negedge rst_n) begin
  if (!rst_n) begin
    ber_delay_counter <= 'd0;
  end
  else begin
    if (ber_delay_counter <= DELAY * 2 ) ber_delay_counter <= ber_delay_counter + 1;
  end
end
assign ber_enable = (ber_delay_counter >= DELAY * 2) ? 1'b1 : 1'b0;

fifo #(
  .WIDTH(1),
  .DELAY (DELAY)
) u_bitstream_delay (
  .clk     (clk          ),
  .enable  (enable       ),
  .rst_n   (rst_n        ),
  .data_in (bitstream    ),
  .data_out(i_transmitted)
);

// Bit error rate counter
ber u_ber (
  .clk           (clk          ),
  .rst_n         (rst_n        ),
  .enable        (ber_enable   ),
  .i_transmitted (i_transmitted),
  .i_received    (out[(M-1) - count_rg]),
  .i_read        (ber_read     ),
  .o_ready       (o_ready      ),
  .o_errors      (o_errors     ),
  .o_counter     (o_counter    )
);

endmodule
