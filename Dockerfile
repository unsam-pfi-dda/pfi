FROM python:3.11-slim
RUN apt-get update --fix-missing --allow-releaseinfo-change
RUN apt-get install -y make gcc g++ iverilog gtkwave graphviz
COPY requirements.txt ./
RUN python -m pip install -r requirements.txt
COPY models/requirements.txt ./
RUN python -m pip install -r requirements.txt