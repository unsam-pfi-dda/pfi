`include "defines.v"

module fifo #(
  parameter WIDTH = 8,
  parameter DELAY  = 2
) (

  input              clk    ,   // I Clock.
  input              enable ,   // I Enable.
  input              rst_n  ,   // I Reset.
  input  [WIDTH-1:0] data_in,   // I Data in.
  output [WIDTH-1:0] data_out   // O Data out.
);

reg [WIDTH-1:0] ram[DELAY-1:0];
integer ptr = 0;


if(DELAY > 0) begin
  always @(posedge clk) begin
    if(enable) begin
      ptr <= ptr < DELAY-1 ? ptr + 1 : 0;
      ram[ptr] <= data_in;
    end
  end
  assign data_out = ram[ptr];

end else begin
  assign data_out = data_in;
end

endmodule
