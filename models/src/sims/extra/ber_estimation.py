"""Module to estimate analytically the bit error rate of a system."""

from collections import Counter

import numpy as np


def snr_to_noise_scalar(snr_db: float, input_scalar: float = 1) -> float:
    """Calculate the noise power value in scalar for a noise power given a SNR in dB.

    It will assume (by default) that the input signal power is normalized to 1,
    but it can be manually set with `input_scalar` argument.
    """
    if input_scalar < 0:
        raise ValueError("input power can't be a negative value")

    snr_scalar = 10 ** (snr_db / 10)
    return input_scalar / snr_scalar


def noise_scale(snr_db: float, input_scalar: float = 1) -> float:
    """Calculates the noise amplitude value in scalar for a given SNr in dB.
    It will assume (by default) that the input signal power is normalized to 1,
    but it can be manually set with `input_scalar` argument.
    """
    return np.sqrt(snr_to_noise_scalar(snr_db, input_scalar) / 2)


def estimate(symbols: list[float], snr: float, coefficients: list[float]) -> None:
    """Estimates the bit error rate for a transfer function with a SNR (in dB)."""
    noise_scale(snr_db=snr, input_scalar=1)
    # Assume uniform probability density function for each symbol
    probability = [1 / len(symbols) for _ in symbols]

    convolve = [
        symbol * (np.convolve([1] * len(coefficients), coefficients, mode="same") - 1)
        for symbol in symbols
    ]
    signal = [Counter(conv) for conv in convolve]
    signal_probability = [
        [prob * _sig / sum(sig.values()) for _sig in sig.values()]
        for sig, prob in zip(signal, probability)
    ]

    print(convolve)
    print(signal, signal_probability)


if __name__ == "__main__":
    estimate(symbols=[-1, 1], snr=1, coefficients=[1, 0.5, 0.125])
