"""Common utility functions for running the system."""

from collections.abc import Sequence
from typing import Any


def align(a: Sequence[Any], b: Sequence[Any], offset: int) -> tuple[slice, slice, int]:
    """Get the intersection between two sets that have a offset
    Returns a pair of slice coordinates for both lists.
    """
    if not a or not b:
        raise ValueError("Input list is empty")

    offset += len(a) - len(b)
    _int = set(range(len(a))).intersection(range(offset, len(b) + offset))

    if not _int:
        return (slice(0, 0), slice(0, 0), offset)

    a_slice = slice(min(_int), max(_int) + 1)
    b_slice = slice(min(_int) - offset, max(_int) - offset + 1)

    return (a_slice, b_slice, offset)
