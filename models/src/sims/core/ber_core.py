"""Class module for running Bit-Error-Rate analysis for an entire System object."""

import itertools
import multiprocessing
from collections.abc import Sequence
from concurrent.futures import ProcessPoolExecutor, as_completed
from copy import deepcopy
from dataclasses import dataclass, field
from threading import RLock

from tqdm.auto import tqdm

from src import Component, System


@dataclass
class BerTestSingle:
    """BER Test worker class."""

    name: str
    system: System
    adapt: int

    samples: int = 0
    errors: int = 0
    errors_min: int = 10
    errors_max: int = 10_000
    ber_limit: float = 1e-8

    _ber: list[float] = field(init=False, default_factory=list)
    _lock: RLock = multiprocessing.Manager().RLock()

    def __post_init__(self):
        self.system = deepcopy(self.system)

    def run(self):
        # TODO: This has an issue that has an offest from the adaptation
        #       processes (offset = num of BerTest objects)
        position = multiprocessing.current_process()._identity[0] - 3

        # Adapt the system coefficients to this SNR.
        self._adapt(position)

        with self._lock:
            pbar = tqdm(position=position, leave=False, unit="err")
        pbar.set_lock(self._lock)  # type: ignore

        # Measure Bit-error-rate
        while True:
            self.samples += 1
            self.system.run()

            if self.system.is_error:
                self.errors += 1
                self._ber.append(self.ber)

                error_target = int(max(self.ber // self.ber_limit, 1) * self.errors_min)
                error_target = min(error_target, self.errors_max)
                pbar.set_description(f" {self.name} ber: {self.ber:.6f}")  # type: ignore
                pbar.update()
                pbar.total = error_target
                if self.errors > self.errors_min and self.errors > error_target:
                    break

        return self.results()

    def _adapt(self, position: int = 0):
        """Initial adaptation for the system with noise."""
        with self._lock:
            pbar = tqdm(
                range(self.adapt),
                position=position,
                leave=False,
                desc=f" {self.name} adapt",
                unit="sa",
            )
        pbar.set_lock(self._lock)  # type: ignore

        for _ in pbar:  # type: ignore
            self.system.run()

    def results(self):
        """Compiles the results for the worker executor."""
        return (self.system, self.samples, self.errors, self._ber)

    @property
    def ber(self) -> float:
        """Calculates the bit error rate for the current test."""
        return self.errors / self.samples


@dataclass
class BerTest:
    name: str
    system: System
    sweep: str
    values: Sequence[int]
    reference: type[Component]
    adapt_samples: int
    symbol: str

    tests: list[BerTestSingle] = field(init=False, default_factory=list)
    _lock: RLock = multiprocessing.Manager().RLock()

    def adapt(self):
        """Initial adaptation for the system without noise."""
        position = multiprocessing.current_process()._identity[0] - 3

        # Ensure that adaptation is enabled for all components
        for component in self.system.components:
            if hasattr(component, "adaptive"):
                component.adaptive = True

        with tqdm(
            total=self.adapt_samples,
            position=position,
            leave=False,
            desc=f" {self.name} adapt",
            unit="sa",
        ) as pbar:
            pbar.set_lock(self._lock)  # type: ignore

            for _ in range(self.adapt_samples):
                self.system.run()
                pbar.update()

        return self.system

    def create_tests(self) -> list[BerTestSingle]:
        """Creates BerTestSingle objects from the internal system, call adapt() method first."""
        self.tests = []
        for value in self.values:
            system = deepcopy(self.system)
            # Create a new component with the new argument
            reference = system.get_component(self.reference)
            attributes = deepcopy(reference.locals)
            attributes[self.sweep] = value
            new = reference.__class__(**attributes)
            # Assign the new component to the system
            system.replace_component(self.reference, new)
            self.tests.append(
                BerTestSingle(
                    name=f"{self.name}:{value:2}",
                    system=system,
                    adapt=self.adapt_samples,
                    _lock=self._lock,
                ),
            )

        return self.tests

    def results(self):
        """Compiles the results for Plot module."""
        return {
            "group": self.name,
            "channel": self.system.channel.coef,
            "symbol": self.symbol,
            "results": [
                [
                    (value, test.errors, test.samples, test.ber)
                    for value, test in zip(self.values, self.tests)
                ],
            ],
            "sweep": self.sweep,
            # Extra data for debugging
        }


@dataclass
class BerTestDirector:
    tests: list[BerTest] = field(init=False, default_factory=list)
    _workers: list[BerTestSingle] = field(init=False, default_factory=list)

    # Need to use a recursive lock for tqdm bars inside workers.
    _lock: RLock = multiprocessing.Manager().RLock()

    def add_test(
        self,
        name: str,
        system: System,
        sweep: str,
        values: Sequence[int],
        reference: type[Component],
        adapt: int,
        symbol: str,
    ):
        """Adds a new BER test to the list.

        Args:
            name: Name of the test
            system: A System object to use for running the tests.
            sweep: Attribute name that will be sweeped for the reference Component object.
            values: List of values to assign to `sweep` attribute.
            reference: The Component object in which the `sweep` attribute will be changed.
            adapt: Number of adaptation samples to run.
            symbol: Plotly symbol that will be used for this test.
        """
        system = deepcopy(system)
        self.tests.append(
            BerTest(
                name,
                system,
                sweep,
                values,
                reference,
                adapt,
                symbol,
                _lock=self._lock,
            ),
        )

    def run(self, max_workers: int = multiprocessing.cpu_count() - 1):
        """Runs all tests and return the resutlts.

        Args:
            max_workers: Maximum number of simultaneous processing workers for running
            the test. Defaults to (#cores - 1).
        """
        # Reference system adaptation stage
        with ProcessPoolExecutor(max_workers=max_workers) as executor:
            future = [executor.submit(test.adapt) for test in self.tests]

            with tqdm(
                total=len(future),
                position=0,
                leave=True,
                desc="Initial adaptation",
                unit="sa",
            ) as pbar:
                pbar.set_lock(self._lock)  # type: ignore

                for result in as_completed(future):
                    test = self.tests[future.index(result)]
                    test.system = result.result()
                    pbar.update()

        # Create single tests
        workers = [reversed(test.create_tests()) for test in self.tests]
        # Interleave workers between different tests, this way longer simulations are
        # processed first. Example: [t1_snr10, t2_snr10, t1_snr9, t2_snr9 ...]
        self._workers = [
            worker
            for worker in itertools.chain.from_iterable(itertools.zip_longest(*workers))
            if worker is not None
        ]

        # Run all tests
        with ProcessPoolExecutor(max_workers=max_workers) as executor:
            future = [executor.submit(worker.run) for worker in self._workers]

            with tqdm(
                total=len(future),
                leave=True,
                position=0,
                desc="Tests",
                unit="sa",
            ) as pbar:
                pbar.set_lock(self._lock)  # type: ignore
                for result in as_completed(future):
                    worker = self._workers[future.index(result)]
                    (
                        worker.system,
                        worker.samples,
                        worker.errors,
                        worker._ber,
                    ) = result.result()
                    pbar.update()
        return self.results()

    def results(self):
        """Compiles the result from all workers for Plot module."""
        return [test.results() for test in self.tests]
