"""SQNR test for DFFE component.

This test will measure SQNR for a fixed-point DFFE component against a reference
(floating) DFFE.

How to run:
    python -m src.sims sqnr dffe

The results will be sent to the Plot module (./src/Plot) for storing and plotting with
plotly.
"""
from functools import partial

from src import System

from .core import SQNRTestDirector

EXCLUSIONS = ("FFE",)


def sqnr_dffe():
    """SQNR simulation for FFE quantization."""
    modifier = partial(SQNRTestDirector.clean_quantizations, exclusions=EXCLUSIONS)
    system = System.from_yaml("quantization.yaml", modifier)

    tests = SQNRTestDirector(
        adapt=500_000,
        measure=10_000,
        system=system,
        reference=system.feedback,
    )

    tests.add_test(
        name="Coefficients",
        quantize={"coef": "s0/0-z/s"},
        sweep="coef",
        integer=range(0, 1),
        fractional=range(1, 13),
    )

    tests.add_test(
        name="Input",
        quantize={"coef": "s9/8-z/s", "input": "s0/0-z/s"},
        sweep="input",
        integer=range(0, 3),
        fractional=range(1, 13),
    )

    tests.add_test(
        name="Output",
        quantize={"coef": "s9/8-z/s", "input": "s10/7-z/s", "response": "s0/0-z/s"},
        sweep="response",
        integer=range(0, 3),
        fractional=range(1, 13),
    )

    return tests.run()
