"""Unit testing for src.sim.core.sqnr_core module."""


import pytest
from numfi import numfi

from src import FFE, Channel, Quantizer, System, Wave
from src.sims.core import SQNRTestDirector

SYMBOLS = (-1, 1)


@pytest.fixture(name="director")
def fixture_director():
    """Fixture for creating a test SQNR director fixture."""
    system = System(
        symbols=SYMBOLS,
        components=[
            Channel(),
            FFE(),
            Quantizer(),
        ],
    )

    director = SQNRTestDirector(
        adapt=1000,
        measure=100,
        system=system,
        reference=system.forward,
    )

    return director


def test_component_quantization(director: SQNRTestDirector):
    """Test that test components are being quantized as expected."""
    director.add_test(
        name="Test",
        quantize={
            "coef": "s9/7-z/s",
            "input": "s9/7-z/s",
            "response": "s0/0-z/s",
        },
        sweep="response",
        integer=range(2),
        fractional=range(10),
    )
    for test in director.tests:
        for integer, data in test._components.items():
            for fractional, component in data.items():
                buffer: Wave = component.__getattribute__(test.sweep)
                values = buffer.values
                assert isinstance(values, numfi)
                assert values.f == fractional
                assert values.w == fractional + integer + 1
