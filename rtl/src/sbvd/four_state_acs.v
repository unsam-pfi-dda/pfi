`include "defines.v"

module four_state_acs #(
  parameter SM_WIDTH = `SM_WIDTH,
  parameter BM_WIDTH = `BM_WIDTH,
  parameter DIRECTION = 0 // 0=Forward; 1=Backward
) (

  input [SM_WIDTH-1:0] i_sm_00    ,  // I State metric 00.
  input [SM_WIDTH-1:0] i_sm_01    ,  // I State metric 01.
  input [SM_WIDTH-1:0] i_sm_10    ,  // I State metric 10.
  input [SM_WIDTH-1:0] i_sm_11    ,  // I State metric 11.
  input [BM_WIDTH-1:0] i_delta_000,  // I Absolute value of the metric 000.
  input [BM_WIDTH-1:0] i_delta_001,  // I Absolute value of the metric 001.
  input [BM_WIDTH-1:0] i_delta_010,  // I Absolute value of the metric 010.
  input [BM_WIDTH-1:0] i_delta_011,  // I Absolute value of the metric 011.
  input [BM_WIDTH-1:0] i_delta_100,  // I Absolute value of the metric 100.
  input [BM_WIDTH-1:0] i_delta_101,  // I Absolute value of the metric 101.
  input [BM_WIDTH-1:0] i_delta_110,  // I Absolute value of the metric 110.
  input [BM_WIDTH-1:0] i_delta_111,  // I Absolute value of the metric 111.

  output         [SM_WIDTH-1:0] o_sm_00    ,  // O State metric 00.
  output         [SM_WIDTH-1:0] o_sm_01    ,  // O State metric 01.
  output         [SM_WIDTH-1:0] o_sm_10    ,  // O State metric 10.
  output         [SM_WIDTH-1:0] o_sm_11    ,  // O State metric 11.
  output                        o_dec_00   ,  // O Decision bits.
  output                        o_dec_01   ,  // O Decision bits.
  output                        o_dec_10   ,  // O Decision bits.
  output                        o_dec_11      // O Decision bits.
);

two_way_acs #(
  .SM_WIDTH(SM_WIDTH),
  .BM_WIDTH(BM_WIDTH)
) u_two_way_acs_00 (
  .i_sm_0        (!DIRECTION ? i_sm_00 : i_sm_00        ),
  .i_sm_1        (!DIRECTION ? i_sm_10 : i_sm_01        ),
  .i_bm_0        (!DIRECTION ? i_delta_000 : i_delta_000),
  .i_bm_1        (!DIRECTION ? i_delta_100 : i_delta_001),
  .o_state_metric(o_sm_00                               ),
  .o_decision    (o_dec_00                              )
);

two_way_acs #(
  .SM_WIDTH(SM_WIDTH),
  .BM_WIDTH(BM_WIDTH)
) u_two_way_acs_01 (
  .i_sm_0        (!DIRECTION ? i_sm_00 : i_sm_10        ),
  .i_sm_1        (!DIRECTION ? i_sm_10 : i_sm_11        ),
  .i_bm_0        (!DIRECTION ? i_delta_001 : i_delta_010),
  .i_bm_1        (!DIRECTION ? i_delta_101 : i_delta_011),
  .o_state_metric(o_sm_01                               ),
  .o_decision    (o_dec_01                              )
);

two_way_acs #(
  .SM_WIDTH(SM_WIDTH),
  .BM_WIDTH(BM_WIDTH)
) u_two_way_acs_10 (
  .i_sm_0        (!DIRECTION ? i_sm_01 : i_sm_00        ),
  .i_sm_1        (!DIRECTION ? i_sm_11 : i_sm_01        ),
  .i_bm_0        (!DIRECTION ? i_delta_010 : i_delta_100),
  .i_bm_1        (!DIRECTION ? i_delta_110 : i_delta_101),
  .o_state_metric(o_sm_10                               ),
  .o_decision    (o_dec_10                              )
);

two_way_acs #(
  .SM_WIDTH(SM_WIDTH),
  .BM_WIDTH(BM_WIDTH)
) u_two_way_acs_11 (
  .i_sm_0        (!DIRECTION ? i_sm_01 : i_sm_10        ),
  .i_sm_1        (!DIRECTION ? i_sm_11 : i_sm_11        ),
  .i_bm_0        (!DIRECTION ? i_delta_011 : i_delta_110),
  .i_bm_1        (!DIRECTION ? i_delta_111 : i_delta_111),
  .o_state_metric(o_sm_11                               ),
  .o_decision    (o_dec_11                              )
);


endmodule
