"""Utility script for Channel and SNR to fixed-point binary representations."""
from numfi import numfi
from src import Channel
from src.fixed_arithmetic.utils import to_fixed

CHANNELS = [
    [4, 2, 1],
    [2, 2, 1],
    [1, 0, 0],
    [-1, 0, 0]
]

# Convert channels to fixed-point binary representation
print("Channel quantization")
for coefficients in CHANNELS:
    channel = Channel(taps=coefficients)
    fixed = numfi(channel.coef, **to_fixed("s8/7-z/s"))
    print(f"  {coefficients}: {fixed.bin} ({channel.coef})")

# Fixed-point SIGMA binary representation for different SNR values.
print("Noise scale quantization")
for snr in range(4, 16, 1):
    channel = Channel(taps=3)
    channel.snr = snr
    fixed = numfi([channel.noise_scale], **to_fixed("u7/7-z/s"))
    print(f"{snr=:2}\t{fixed.bin[0]:07}\t{channel.noise_scale:.7f}")
