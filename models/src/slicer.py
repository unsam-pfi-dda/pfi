"""Output slicer system."""

import numpy as np


def BPSK(signal: np.float64) -> np.float64:
    """Simple BPSK detector.

    Returns
        +1.0 if signal is greater than zero
        -1.0 if signal is smaller than zero
    """
    return np.float64((signal >= 0) * 2 - 1)
