"""Traceback module testbench"""

import sys
from pathlib import Path
import pytest
import cocotb
import cocotb.regression
from cocotb.triggers import Timer
from cocotb.handle import HierarchyObject
from cocotb_test.simulator import Simulator
import numpy as np

sys.path.append(str(Path(__file__).parent.parent.parent.resolve()))

from models.src import SBVD
from models.src.sbvd import bin2int
from rtl.tests.utils import binstr2list


@pytest.mark.parametrize("direction", ["0", "1"], ids=["forward", "backward"])
def test_traceback(simulator: Simulator, direction: str):
    """Run parametrized CoCoTB test with pytest"""
    simulator.parameters["DIRECTION"] = direction
    simulator.run()


async def model_cosimulation(dut: HierarchyObject, state: tuple[int], decisions: list[int]):
    """Cosimulate RTL and model and compare results"""

    direction = True if dut.DIRECTION.value == 0 else False

    golden = SBVD.traceback(decisions, state, direction)

    dut.i_state.value = bin2int(state)
    dut.i_decisions.value = bin2int(reversed(decisions))
    await Timer(1, units="ps")  # wait a bit
    results = (dut.o_data.value, tuple(binstr2list(dut.o_state.value.binstr, bits=1)))

    assert golden == results, print(golden, results, state, decisions)


tf = cocotb.regression.TestFactory(model_cosimulation)
tf.add_option("state", [(0, 0), (0, 1), (1, 0), (1, 1)])
tf.add_option("decisions", np.random.choice([0, 1], size=(10, 4)).tolist())
tf.generate_tests()
