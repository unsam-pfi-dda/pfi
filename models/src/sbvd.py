"""Sliding block viterbi decoder component as it will be implemented in RTL code."""


import itertools
import operator
from collections.abc import Sequence
from dataclasses import dataclass, field

import numpy as np
from numfi import numfi
from numpy.typing import NDArray

from .fixed_arithmetic.utils import to_fixed
from .slicer import BPSK
from .wave import Wave


def bin2int(data: Sequence[int]) -> int:
    """Converts a binary value represented in tuples into a integer representation.

    Examples
        >>> bin2int((0, 0))
        0
        >>> bin2int((1, 0, 0, 0, 1))
        17
    """
    value = 0
    for binary in data:
        value = (value << 1) | binary
    return value


@dataclass
class SBVD:
    """Systolic sliding block viterbi decoder algorithm class."""

    depth: int
    taps: int
    symbols: tuple[np.float64, ...] = (-1.0, 1.0)
    quantize: dict[str, str] = field(default_factory=dict)

    input: Wave = field(init=False, repr=False)
    output: Wave = field(init=False, repr=False)

    i: int = 0

    def __post_init__(self):
        """Post initialization methods."""
        # Input buffer will hold two set of samples (depth) to emulate skew buffer
        # behavior at the input stage, (forward stage takes the older set, while
        # backward stage gets the newer set)
        self.input = Wave(self.depth * 4)
        self.output = Wave(-self.lag - self.depth)

        if len(self.symbols) != 2:
            raise AttributeError("sbvd only supports two-symbol system")

    def run(self, sample: np.float64, coef: NDArray[np.float64]) -> np.float64:
        """Run SBVD algoritm for a new sample.

        Calculations will be executed only when the skew buffer is filled. That
        happens every `depth` iterations.

        Output will be serialized for each clock

        Arguments:
            sample: new sample value to compute.
            coef: list of postcursors values, should not include main cursor.
        """
        # Skip if depth is zero, that's a slicer
        if self.depth == 0:
            return BPSK(sample)

        self.i = (self.i + 1) % (self.depth * 2)

        # Latch the most likelihood sequence stream once a full cycle has been done
        if self.i == 0:
            # The coefficient list received from the DFFE does not include the cursor,
            # so it is manually inserted to the coefficient list.
            # NOTE: This assumes that the equalizer main cursor value is 1 and the
            #   system's response power is normalized, or results might not be accurate.

            # Roll the new set of results to the output buffer
            self.output.set(
                np.concatenate(
                    (
                        self.output.values[self.depth * 2 :],
                        self.sbvd(self.input.values, coef),
                    ),
                ),
            )
        self.input.add(sample)

        # Serialize the output from 0 to depth. Despite the output buffer being
        # longer, it will still output the data since it's being rolled by `depth`
        # positions every calculation cycle.
        # This will emulate the SBVD synchronizing lags.
        return self.output.values[self.i]

    @property
    def clocks(self) -> int:
        """Calculates the sample-time-delay for the Sliding Block Viterbi Decoder.

        The lag value will correspond to the correlation value between the input
        and output buffers. This translates as the number of samples read at the
        output until the input sequence is output, and NOT the number of clocks
        until the input set is output.
        For example, for a L=2 system the input set takes 9 clocks until the
        first output value (Xn). However the lag value will correspond to the
        36+2 samples from t=0 up to the Xn value... (+2 since the first two
        output values Xn-1 and Xn-2 are read first).

        Each clock will output `2*depth` new samples, and then it will require
        an offset of `depth` to get the first output sample, since half of the
        output buffer is from the previous set.

        Notes
            See property clocks for calculation formula.

        Examples
            The lag for a two-tap, L=2 SBVD system is 9 for outputting the backward
            sample into the output buffer, plus 2 extra clocks due to the Xn-2
            and Xn-1 output serializing (outputs one sample for each clock)

            >>> SBVD(taps=2, depth=2).clocks
            38

            And for a two-tap and L=4 SBVD system, the lag has to be 19
            >>> SBVD(taps=2, depth=4).clocks
            124

            >>> SBVD(taps=2, depth=10).clocks
            670
        """
        if self.depth == 0:
            return 0

        return (abs(self.lag) * 2 + 1) * self.depth

    @property
    def lag(self) -> int:
        """Calculates the number of cycles required until the sample is placed
        at the output buffers. This is the clock-lag and not the sample-lag.

        The number of clocks will be calculated as
            - Fill the backward skew buffer: `2*L-1`
            - BM -> ACS lag: `1`
            - ACS -> TB lag: `3` (for the latest)
            - TB -> output: `L`

        Examples
            >>> SBVD(taps=2, depth=2).lag
            -9

            >>> SBVD(taps=2, depth=3).lag
            -12

            >>> SBVD(taps=2, depth=4).lag
            -15

            >>> SBVD(taps=2, depth=10).lag
            -33
        """
        if self.depth == 0:
            return 0

        return -((2 * self.depth - 1) + 1 + 3 + self.depth)

    @staticmethod
    def conv(symbols: Sequence[float], channel: Sequence[float]):
        """Convolution component.

        Returns a list for all the calculated state distances from the available
        symbols and the channel elements.
        The first element will correspond for the 0-state, i.e. all samples are the
        first symbol element: for (-1, 1) the 0-state would be when two -1 are
        sampled.

        Examples
            Calculate the symbol distance values for a three-tap chanel-less system
            with -1, and 1 possible symbols.

            >>> SBVD.conv((-1, 1), (1, 0, 0))
            [-1, 1, -1, 1, -1, 1, -1, 1]

            This result is ordered as follows:
                - 00 => 0
                - 00 => 1
                - 01 => 0
                - 01 => 1
                - ...
                - 11 => 1

            Or with a [4, 2, 1] channel, where 4 is the main tap, 2 and 1 are
            postcursors.

            >>> SBVD.conv((-1, 1), (4, 2, 1))
            [-7, 1, -3, 5, -5, 3, -1, 7]

            Or for a trinary system with allowed symbols -1, 0 and 1.

            >>> SBVD.conv((-1, 0, 1), (1, 0))
            [-1, 0, 1, -1, 0, 1, -1, 0, 1]
        """
        # Need to flip the itertools.product vector elements so that the MSB
        # changes for every element, instead of the LSB.
        # Because this is how is aligned in the RTL.
        #    Yes           No

        return np.dot(
            [state[::-1] for state in itertools.product(symbols, repeat=len(channel))],
            channel,
        ).tolist()

    @staticmethod
    def branch_metrics(sample: float, distances: Sequence[float]):
        """Branch metric component.

        Takes a sample value and a distance vector (from conv() method) and
        calculates the branch metric value:
            branch_metric = abs(sample - distance)

        Returns a list with all the branch metric distances for the given sample
        to all possible states.

        Example:
            Calculate the branch distance for all 8 possible states for a two-symbol
            system (-1, 1) with a three tap channel (unit response) [1, 0, 0]

            >>> SBVD.branch_metrics(-1, SBVD.conv((-1, 1), (1, 0, 0)))
            [0, 2, 0, 2, 0, 2, 0, 2]

            When a channel is applied, the sample value will also take into carry
            some of the previous symbol from the sequence. For example, for a
            [4, 2, 1] channel, three consecutive -1 symbols will result in a -6
            value (state 0), then if a 1 is sent the sample will be +4-2-1 = 1, which
            will result in 00 -> 01 state transition.

            >>> SBVD.branch_metrics(1, SBVD.conv((-1, 1), (4, 2, 1)))
            [8, 0, 4, 4, 6, 2, 2, 6]

            But for state 01 -> 10 the sample should be -4+2-1 = -3

            >>> SBVD.branch_metrics(-3, SBVD.conv((-1, 1), (4, 2, 1)))
            [4, 4, 0, 8, 2, 6, 2, 10]

        """
        return [abs(sample - distance) for distance in distances]

    @staticmethod
    def four_state_acs(
        state_metric: Sequence[float],
        branch_metric: Sequence[float],
        forward: bool = True,
    ):
        """Four state Radix-2 add compare and select component.

        Returns a list of tuple results for each possible state transition. The
        format is [(d0, d1, d2, d3), (m0, m1, m2, m3, m4, m5, m6, m7)]
        Where `dx` are the decisions and `mx` are the state metrics for a state `x`.
        E.g. d2 is the decision for state (1, 0), while m3 is the state metric for (1, 1).

        For the backward Trellis, `forward` argument must be set to False. It will
        re-arrange both `state_metric` and `branch_metric` vectors in order to
        operate in a backward trellis (new sample will be appended to the left state
        instead of the right) i.e. for a forward vs backward trellis:
            Sample      Forward         Backward
            1           00 -> 01        00 -> 10
            0           01 -> 10        10 -> 01

        Example:
            Compute decisions and metrics for a channel-less system

            >>> branch = SBVD.branch_metrics(1, SBVD.conv((-1, 1), (1, 0, 0)))
            >>> SBVD.four_state_acs([0, 0, 0, 0], branch, forward=True)
            [(0, 0, 0, 0), (2, 0, 2, 0)]

            And for a (4, 2, 1) channel (previous state 11 -> 1 sequence).

            >>> branch = SBVD.branch_metrics(7, SBVD.conv((-1, 1), (4, 2, 1)))
            >>> SBVD.four_state_acs([12, 4, 8, 0], branch, forward=True)
            [(1, 1, 1, 1), (20, 12, 8, 0)]

            And for backward

            >>> branch = SBVD.branch_metrics(-7, SBVD.conv((-1, 1), (4, 2, 1)))
            >>> SBVD.four_state_acs([0, 4, 2, 2], branch, forward=False)
            [(0, 0, 0, 0), (0, 6, 2, 8)]
        """
        sequence = list(itertools.product(range(2), repeat=3))
        accumulate: list[tuple[float, ...]] = []
        for state, _ in enumerate(state_metric):
            # Forward ACS Treillis will transition to new state adding the sample to
            # the rightmost position
            if forward:
                paths = [branch for branch in sequence if bin2int(branch[1:]) == state]
                paths_state = [state_metric[bin2int(branch[:-1])] for branch in paths]
            # While backward trellis will prepend the new sample to the leftmost position
            else:
                paths = [branch for branch in sequence if bin2int(branch[:-1]) == state]
                paths_state = [state_metric[bin2int(branch[1:])] for branch in paths]

            paths_branch = [branch_metric[bin2int(branch)] for branch in paths]
            accumulate.append(tuple(map(operator.__add__, paths_state, paths_branch)))

        return list(zip(*[(np.argmin(value), np.min(value)) for value in accumulate]))

    @staticmethod
    def four_state_ac(forward: Sequence[float], backward: Sequence[float]):
        """Four state Radix-2 add and compare component for estimating a state
        from forward and backward stages.

        Returns an estimated state in tuple format, e.g. (0, 0) for state 0, or (1, 0)
        for state 2.

        Example:
            >>> SBVD.four_state_ac((0, 12, 6, 6), (0, 12, 6, 6))
            (0, 0)

            >>> SBVD.four_state_ac((2, 12, 6, 0), (5, 12, 6, 1))
            (1, 1)
        """
        metrics = list(map(sum, zip(forward, backward)))
        states = [(0, 0), (0, 1), (1, 0), (1, 1)]
        return states[np.argmin(metrics)]

    @staticmethod
    def traceback(decisions: Sequence[int], state: Sequence[int], forward: bool):
        """Traceback component.

        Examples
            Traceback forward stage, starting from the estimated state (1, 1), the
            output decision is 1 and the new state will be determined by the decision vector
            and the trellis states:
            [[(0, 0), (1, 0)], [(0, 0), (1, 0)], [(0, 1), (1, 1)], [(0, 1), (1, 1)]]

            So for 11 state, the decision index is 0 so the origin state is 01. The
            output value is 1.
            >>> SBVD.traceback([1, 1, 1, 0], (1, 1), forward=True)
            (1, (0, 1))

            The next traceback will take the 01 state, which decision index is 0 and the
            origin state is 00 (from trellis states). The output value is 1.
            >>> SBVD.traceback([1, 0, 1, 1], (0, 1), forward=True)
            (1, (0, 0))

            The next traceback will take the 00 state, which decision index is 1, so the
            origin state is 10. The output value is 0
            >>> SBVD.traceback([1, 0, 0, 0], (0, 0), forward=True)
            (0, (1, 0))

            And so on...
            >>> SBVD.traceback([0, 1, 0, 1], (1, 0), forward=True)
            (0, (0, 1))

            For the backward traceback stage (forward=False), the output state is not the
            origin but the next state. The trellis states are now:
            [[(0, 0), (0, 1)], [(1, 0), (1, 1)], [(0, 0), (0, 1)], [(1, 0), (1, 1)]]

            Starting from 11 state, the decision index is 0 and the next state is 10. The
            output value is 0.
            >>> SBVD.traceback([1, 1, 1, 0], (1, 1), forward=False)
            (0, (1, 0))

            Now, the state is 01 and decision index is 0 so the next state is 00. The output
            value is 0.
            >>> SBVD.traceback([1, 1, 0, 1], (1, 0), forward=False)
            (0, (0, 0))

            State is 00 and decision index is 1, the next state is 01. The output value is 1.
            >>> SBVD.traceback([1, 0, 0, 0], (0, 0), forward=False)
            (1, (0, 1))
        """
        trellis = (
            [[(0, 0), (1, 0)], [(0, 0), (1, 0)], [(0, 1), (1, 1)], [(0, 1), (1, 1)]]
            if forward
            else [
                [(0, 0), (0, 1)],
                [(1, 0), (1, 1)],
                [(0, 0), (0, 1)],
                [(1, 0), (1, 1)],
            ]
        )

        # convert binary tuple (1, 1) to int (3)
        state_index = int("".join(str(ele) for ele in state), 2)
        decision_index = decisions[state_index]
        new_state = trellis[state_index][decision_index]
        decision = state[-1] if forward else decision_index

        state = (decision, state[0]) if forward else (state[1], decision)
        return (decision, new_state)

    @staticmethod
    def skew_buffer(samples: NDArray[np.float64]):
        """Skew buffer emulation for a combinational logic, the output is a pair of
        aligned sample vectors for the forward and backward decoders.

        Takes a sequence with even elements and splits in half. The second half is reversed.

        Note:
            The viterbi depth (L) parameter is calculated automatically as len(samples)//4.

        Examples:
            For a L=2 system sampling

            >>> SBVD.skew_buffer([0, 1, 2, 3, 4, 5, 6, 7])
            [[0, 1, 2, 3], [7, 6, 5, 4]]

            >>> SBVD.skew_buffer([4, 5, 6, 7, 8 ,9, 10, 11])
            [[4, 5, 6, 7], [11, 10, 9, 8]]

            And for a L=4 system, then 16 samples are required.

            >>> SBVD.skew_buffer(range(16))
            [[0, 1, 2, 3, 4, 5, 6, 7], [15, 14, 13, 12, 11, 10, 9, 8]]

            Samples buffer must have even elements

            >>> SBVD.skew_buffer(range(7))
            Traceback (most recent call last):
            ...
            ValueError: skew_buffer samples vector must be even
        """
        if len(samples) % 2:
            raise ValueError("skew_buffer samples vector must be even")
        length = len(samples) // 2
        return [list(samples[:length]), list(reversed(samples[-length:]))]

    def sbvd(
        self,
        samples: NDArray[np.float64],
        channel: NDArray[np.float64],
    ) -> NDArray[np.float64]:
        """Sliding block viterbi decoder component in combinational logic.

        To emulate input vector skew buffer, the input samples sequence must be
        twice as large (4L) where the first half are for the oldest set and the
        second for the newest set.
        E.g. for a L=2 decoder 4 new samples will be acquired for each clock tick
        for t=0 "a1, ... a3" samples, and the next clock t=1 "b1, ... b3" samples.
        The resulting input sample sequence should look like this:
        [a1, a2, a3, a4, b1, b2, b3, b4]

        The output value is a list of the estimated decoded sequence corresponding to the
        2L center elements from the samples buffer.
        E.g. for a L=2 decoder the output vector will have two samples for each set:
        [a3, a4, b1, b2]

        Examples
            Create a Sliding Block Viterbi Decoder object for a two-symbol system
            with a depth of two (L=2)

            >>> sbvd = SBVD(depth=2, taps=2, symbols=(-1, 1))
            >>> channel = (1, 0, 0)
            >>> sequence = [-1, -1, -1, 1, -1, 1, -1, 1, -1, 1]
            >>> samples = np.convolve(sequence, channel, mode="valid").tolist()
            >>> samples
            [-1, 1, -1, 1, -1, 1, -1, 1]

            Now run the sliding block viterbi decoder

            >>> sbvd.sbvd(samples=samples, channel=channel)
            array([-1,  1, -1,  1])

            Or for a L=4 Viterbi decoder it will take 4L samples and return 2L decisions.

            >>> sbvd = SBVD(depth=4, taps=2, symbols=(-1, 1))
            >>> samples = [-1, -1, -1, 1, -1, 1, -1, -1, -1, 1, -1, -1, -1, -1, 1, -1]
            >>> sbvd.sbvd(samples=samples, channel=channel)
            array([-1,  1, -1, -1, -1,  1, -1, -1])

            Test a L=4 system with a non uniform channel

            >>> channel = (4, 2, 1)
            >>> sequence = [0, 0, -1, -1, -1, 1, -1, 1, -1, -1, -1, 1, -1, -1, -1, -1, 1, -1]
            >>> samples = np.convolve(sequence, channel, mode="valid").tolist()
            >>> sbvd.sbvd(samples=samples, channel=channel)
            array([-1,  1, -1, -1, -1,  1, -1, -1])

            Same with noise

            >>> samples += np.random.normal(loc=0, size=len(samples)) * 0.1
            >>> sbvd.sbvd(samples=samples, channel=channel)
            array([-1,  1, -1, -1, -1,  1, -1, -1])

            Test a L=10 system with non-uniform channel, noise and a random sequence

            >>> L = 10
            >>> sbvd = SBVD(depth=L, taps=2, symbols=(-1, 1))
            >>> channel = (4, 2, 1)
            >>> for _ in range(10):
            ...     sequence = [0, 0] + np.random.choice(sbvd.symbols, size=4*L).tolist()
            ...     samples = np.convolve(sequence, channel, mode="valid").tolist()
            ...     samples += np.random.normal(loc=0, size=len(samples)) * 0.2
            ...     assert sequence[2+L:-L] == sbvd.sbvd(samples, channel).tolist()
        """
        distances = self.conv(symbols=self.symbols, channel=channel)
        forward: Sequence[Sequence[int]] = []
        backward: Sequence[Sequence[int]] = []
        accumulated: Sequence[np.float64] = []
        results: Sequence[int] = []

        # The number of samples is 2*depth for a depth=2*L system. This means that if the
        # L=2 then 8 samples are processed simultaneously: 4 for this new tick (backward) and
        # 4 from the previous tick (forward).
        # This emulates the extra delay present in the forward stage.
        # The samples buffer should roll 4 positions (left) for each clock, so that the
        # 4 samples from the backward will be rolled to the forward in the next tick.
        # The samples are realigned using `skew_buffer` component:
        #           samples                --->           skew_buffer
        # [a1, a2, a3, a4, b1, b2, b3, b4] ---> [a1, a2, a3, a4, b4, b3, b2, b1].

        for decoder, data, direction in zip(
            (forward, backward),
            self.skew_buffer(samples),
            (True, False),
        ):
            metrics = np.array([0.0, 0.0, 0.0, 0.0])  # initial metrics
            for sample in data:
                decision, metrics = self.four_state_acs(
                    state_metric=metrics,
                    branch_metric=self.branch_metrics(
                        sample=sample,
                        distances=distances,
                    ),
                    forward=direction,
                )
                decoder.append(decision)
            # Quantize accumulated metrics
            if self.quantize.get("sbvd", {}):
                metrics = numfi(metrics, to_fixed(self.quantize["sbvd"]))

            accumulated.append(metrics)


        # Quantize accumulated

        # Take out the las element, which contains the accumulated metrics for both
        # forward and backward stages.
        estimated_state = self.four_state_ac(*accumulated)

        # Only the second half of the slices will do the decoding part.
        decoding = (
            (forward[: -len(forward) // 2 - 1 : -1], True),
            (backward[: -len(backward) // 2 - 1 : -1], False),
        )
        for decoder, direction in decoding:
            state = estimated_state
            for decisions in decoder:
                result, state = self.traceback(decisions, state, direction)
                results.append(result)

        # Rearrange results
        # [f3, f2, f1, f0, b0, b1, b2, b3] --> [f0, f1, f2, f3, b0, b1, b2, b3]
        results = results[len(results) // 2 - 1 :: -1] + results[-len(results) // 2 :]

        return np.array([self.symbols[value] for value in results])  # type: ignore
