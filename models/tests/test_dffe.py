"""Unit testing for DFFE component."""

import hypothesis
import hypothesis.strategies
import numpy as np
import pytest
from scipy import signal
from src import DFE, DFFE, Channel, Quantizer, System, BPSK
from tests.utils import non_duobinary_channel

SYMBOLS = (-1.0, 1.0)
CHANNEL = np.array([0, 0, 1, 0.75, 0.0125], dtype=np.float64)
TAPS = len(CHANNEL)
PRECURSORS = CHANNEL[: TAPS // 2]
CURSOR = CHANNEL[TAPS // 2]
POSTCURSORS = CHANNEL[-TAPS // 2 + 1 :]

DFFE_TAPS = 5


@pytest.fixture(name="dffe", params=[0, 10])
def fixture_dffe(request: pytest.FixtureRequest) -> DFFE:
    """Creates a DFFE object."""
    dffe = DFFE(
        taps=5,
        iterations=6,
        mlse_depth=request.param,
        mlse_taps=2,
        buffer_length=128,
    )
    dffe.coef[: len(POSTCURSORS)] = POSTCURSORS
    return dffe


@pytest.fixture(name="system")
def fixture_system(dffe: DFFE) -> System:
    """Creates a System object for testing."""
    return System(
        symbols=SYMBOLS,
        components=[
            Channel(taps=CHANNEL),
            dffe,
            Quantizer(),
        ],
    )


class TestDFFEObject:
    """Test if DFFE object is created correctly."""

    def test_dffe_object(self, system: System):
        """Test that the DFFE object is created succesfully."""
        dffe = system.get_component(DFFE)

        np.testing.assert_array_equal(dffe.coef, [0.75, 0.0125, 0, 0, 0])

    def test_dffe_as_slicer(self):
        """Test that a iterations=1 DFFE with zero-depth VA is equal to a slicer."""
        system = System(
            components=[
                Channel(taps=[1, 0.75, 0.5]),
                DFFE(taps=3, iterations=1, mlse_iterations=1, mlse_depth=0),
                Quantizer(),
            ],
        )

        coef = system.channel.coef[abs(system.channel.lag) + 1 :].copy()
        coef.resize(system.feedback.coef.shape, refcheck=False)

        assert system.feedback.lag == 0

        for _ in range(10_000):
            result = system.run()
            assert BPSK(system.feedback.input.last) == result

    @hypothesis.given(
        dffe_iter=hypothesis.strategies.integers(min_value=10, max_value=20),
        mlse_depth=hypothesis.strategies.integers(min_value=0, max_value=8).filter(
            lambda x: x != 1,  # taps=1 will cause to output missed samples.
        ),
        mlse_taps=hypothesis.strategies.integers(min_value=2, max_value=2),
        channel=hypothesis.strategies.lists(
            hypothesis.strategies.floats(min_value=0, max_value=10),
            min_size=3,
            max_size=10,
        ).filter(
            lambda x: x[0] > x[1] > x[2],
        ),  # exponential channel
    )
    @hypothesis.settings(
        deadline=None,
        max_examples=50,
        suppress_health_check=[hypothesis.HealthCheck.filter_too_much],
    )
    def test_dffe_lag(
        self,
        dffe_iter: int,
        mlse_depth: int,
        mlse_taps: int,
        channel: list[float],
    ):
        """Test DFFE object lag."""
        dffe = DFFE(
            taps=len(channel),
            iterations=dffe_iter,
            mlse_depth=mlse_depth,
            mlse_taps=mlse_taps,
        )
        dffe.coef = np.array(channel)

        size = max(2 * abs(dffe.lag), 500)
        sequence = np.random.choice(SYMBOLS, size=size)
        samples = np.convolve(sequence, channel, mode="valid")
        samples += np.random.normal(size=len(samples))

        results = [dffe.run(sample) for sample in samples]

        correlation = signal.correlate(samples, results)
        correlation_lags = signal.correlation_lags(len(samples), len(results))

        assert dffe.lag == correlation_lags[np.argmax(correlation)]

    @pytest.mark.parametrize(
        "channel",
        [
            [0, 1, 0],
            [0, 0, 0, 1, 0.75, 0.5, 0.25],
            [0, 0, 0, 0, 1, 0.5, 0.25, 0.125, 0.0625],
        ],
        ids=("Impulse", "Linear", "Exponential"),
    )
    @pytest.mark.parametrize("dffe_iter", [10, 20], ids=("R:10", "R:20"))
    @pytest.mark.parametrize(
        ("mlse_depth", "mlse_iterations"),
        [(0, 0), (10, 1), (10, 3)],
        ids=("DFFE", "DFFE-VA Rv:1", "DFFE-VA Rv:3"),
    )
    def test_dffe_bitstream(
        self,
        dffe_iter: int,
        mlse_depth: int,
        channel: list[float],
        mlse_iterations: int,
    ):
        """Test a random bitstream with zero-forced random noiseless channel."""
        system = System(
            symbols=SYMBOLS,
            components=[
                Channel(taps=channel),
                DFFE(
                    taps=len(channel) + 2,
                    iterations=dffe_iter,
                    mlse_iterations=mlse_iterations,
                    mlse_depth=mlse_depth,
                    mlse_taps=2,
                    adaptive=False,
                ),
                Quantizer(),
            ],
        )
        # Copy channel coefficients
        coef = system.channel.coef[abs(system.channel.lag) + 1 :].copy()
        coef.resize(system.feedback.coef.shape, refcheck=False)
        system.feedback.coef = coef
        system.feedback.mlse_main_tap = system.channel.coef[0]

        size = max(2 * abs(system.lag), 200)
        golden = np.random.choice(SYMBOLS, size=size)
        results = [system.run(sample) for sample in golden]

        if system.lag:
            golden = golden[: system.lag]
            results = results[-system.lag :]

        np.testing.assert_array_equal(golden, results)

    def test_adapt(self):
        """Test DFFE adaptation algorithm."""
