`include "defines.v"

module traceback #(
  parameter DIRECTION = 0 // 0=Forward; 1=Backward
) (

  input [1:0] i_state,      // Current state estimation.
  input [3:0] i_decisions,  // Decisions.

  output [1:0] o_state,     // Next state estimation.
  output       o_data       // Output value.
);

assign o_data  = !DIRECTION ? i_state[0] : i_decisions[i_state];
assign o_state = !DIRECTION ? {i_decisions[i_state], i_state[1]} : {i_state[0], i_decisions[i_state]};

endmodule
