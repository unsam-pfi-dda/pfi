"""Unit testing for Viterbi component.

This is testing a well-known static system configuration.
"""
import numpy as np
import pytest
from numfi import numfi
from scipy import signal

from src import Wave, to_fixed
from src.mlse import Cell, State, Viterbi

SNR = np.inf
SYMBOLS = (-1.0, 1.0)
DEPTH = 5
CHANNEL = np.array([1, 0.5, 0.125], dtype=np.float64)  # type: ignore
SEQUENCE = [-1, 1, 1, -1, 1, 1, 1, 1, 1, 1, 1, -1, -1, 1, -1, -1, -1, -1, -1, 1, -1, 1]

COEFFICIENTS = CHANNEL[1:]
TAPS = 2
SAMPLES = np.convolve(SEQUENCE, CHANNEL, mode="valid")  # type: ignore
RESULTS = list(SEQUENCE[len(CHANNEL) - 1 : -DEPTH])

QUANTIZATION = {
    "metrics": "s9/4-z/s",
    "cost": "s9/4-z/s",
}

GOLDEN_CELLS = [
    Cell(
        symbols=SYMBOLS,
        sequence=(
            ([-1, -1, -1, -1, -1], [-1, -1, -1, -1, 1]),
            ([-1, -1, 1, -1, -1], [-1, -1, 1, -1, 1]),
        ),
    ),
    Cell(
        symbols=SYMBOLS,
        sequence=(
            ([-1, -1, -1, 1, -1], [-1, -1, -1, 1, 1]),
            ([-1, -1, 1, 1, -1], [-1, -1, 1, 1, 1]),
        ),
    ),
]
GOLDEN_CELLS[0].metrics = [[-1.625, -1.375], [0.375, 0.625]]  # type: ignore
GOLDEN_CELLS[1].metrics = [[-0.625, -0.375], [1.375, 1.625]]  # type: ignore

GOLDEN_STATES = [
    State(
        id=0,
        combination=(-1, -1),
        stream=Wave(DEPTH),
        transitions=(0, 1),
        sequence=(
            [-1, -1, -1, -1, -1],
            [-1, -1, -1, -1, 1],
        ),
        cell=GOLDEN_CELLS[0],
    ),
    State(
        id=1,
        combination=(-1, 1),
        stream=Wave(DEPTH),
        transitions=(2, 3),
        sequence=(
            [-1, -1, -1, 1, -1],
            [-1, -1, -1, 1, 1],
        ),
        cell=GOLDEN_CELLS[1],
    ),
    State(
        id=2,
        combination=(1, -1),
        stream=Wave(DEPTH),
        transitions=(0, 1),
        sequence=(
            [-1, -1, 1, -1, -1],
            [-1, -1, 1, -1, 1],
        ),
        cell=GOLDEN_CELLS[0],
    ),
    State(
        id=3,
        combination=(1, 1),
        stream=Wave(DEPTH),
        transitions=(2, 3),
        sequence=(
            [-1, -1, 1, 1, -1],
            [-1, -1, 1, 1, 1],
        ),
        cell=GOLDEN_CELLS[1],
    ),
]


@pytest.fixture(name="viterbi")
def fixture_viterbi():
    return Viterbi(depth=DEPTH, taps=TAPS, symbols=SYMBOLS)


@pytest.fixture(name="viterbi_fixed")
def fixture_viterbi_fixed():
    return Viterbi(depth=DEPTH, taps=TAPS, symbols=SYMBOLS, quantize=QUANTIZATION)


class TestMLSEObject:
    """Test if Viterbi object is created correctly."""

    def test_mlse_quantized(self, viterbi_fixed: Viterbi):
        """Test the quantization."""
        cost_quantization = to_fixed(QUANTIZATION["cost"])
        for state in viterbi_fixed.states:
            assert isinstance(state.cost, numfi)
            assert state.cost.s == cost_quantization["s"]
            assert state.cost.w == cost_quantization["w"]
            assert state.cost.f == cost_quantization["f"]
            assert state.cost.rounding == cost_quantization["rounding"]
            assert state.cost.overflow == cost_quantization["overflow"]

        metric_quantization = to_fixed(QUANTIZATION["metrics"])
        for cell in viterbi_fixed.cells:
            for metric in cell.metrics:
                assert isinstance(metric, numfi)
                assert metric.s == metric_quantization["s"]
                assert metric.w == metric_quantization["w"]
                assert metric.f == metric_quantization["f"]
                assert metric.rounding == metric_quantization["rounding"]
                assert metric.overflow == metric_quantization["overflow"]

    def test_mlse_states(self, viterbi: Viterbi):
        """Test that all Viterbi states are created as expected."""
        for golden, test in zip(GOLDEN_STATES, viterbi.states):
            assert golden.id == test.id
            assert golden.combination == test.combination
            assert golden.transitions == test.transitions
            np.testing.assert_array_equal(golden.sequence, test.sequence)  # type: ignore
            assert golden.cell.symbols == test.cell.symbols
            np.testing.assert_array_equal(golden.cell.sequence, test.cell.sequence)  # type: ignore

    def test_cells(self, viterbi: Viterbi):
        """Test cell creation."""
        for golden, test in zip(GOLDEN_CELLS, viterbi.cells):
            test.update_metrics(CHANNEL)

            assert golden.symbols == test.symbols
            np.testing.assert_array_equal(golden.sequence, test.sequence)  # type: ignore
            np.testing.assert_array_equal(golden.metrics, test.metrics)  # type: ignore

    def test_cells_quantized(self, viterbi_fixed: Viterbi):
        """Test cell creation when quantized."""
        self.test_cells(viterbi_fixed)

    def test_mlse_lag(self, viterbi: Viterbi):
        """Test Viterbi lag."""
        samples = np.random.choice(SYMBOLS, size=100)
        results = [viterbi.run(sample, np.array([0, 0])) for sample in samples]

        correlation = signal.correlate(samples, results)
        correlation_lags = signal.correlation_lags(len(samples), len(results))

        assert viterbi.lag == correlation_lags[np.argmax(correlation)]


def test_run_single(viterbi: Viterbi):
    """Compare all Viterbi internal states for a single run."""
    golden_data = [
        {
            "id": 0,
            "cost": 5.0,
            "stream": RESULTS[: DEPTH - 2] + [-1.0, -1.0],
        },
        {
            "id": 1,
            "cost": 3.0,
            "stream": RESULTS[: DEPTH - 2] + [-1.0, 1.0],
        },
        {
            "id": 2,
            "cost": 2.0,
            "stream": RESULTS[: DEPTH - 2] + [1.0, -1.0],
        },
        {
            "id": 3,
            "cost": 0.0,
            "stream": RESULTS[: DEPTH - 2] + [1.0, 1.0],
        },
    ]

    # Prior running any sample
    for state in viterbi.states:
        assert state.cost == 0.0
        assert not state.stream.values.any()  # Expect all zeros

    for sample in SAMPLES[:DEPTH]:
        viterbi.run(sample, COEFFICIENTS)

    np.testing.assert_array_equal(SAMPLES[:DEPTH], viterbi.input.values)
    viterbi.run(0, COEFFICIENTS)  # push data to output buffer
    np.testing.assert_array_equal(RESULTS[:DEPTH], viterbi.output.values)

    for golden, test in zip(golden_data, viterbi.states):
        assert golden["id"] == test.id
        assert golden["stream"] == list(test.stream.values)
        assert golden["cost"] == test.cost


def test_run_single_quantized(viterbi_fixed: Viterbi):
    """Compare all Viterbi internal states for a single run when quantized."""
    test_run_single(viterbi_fixed)


def test_run_stream(viterbi: Viterbi):
    """Compare Viterbi output values against golden."""
    results = [viterbi.run(sample, COEFFICIENTS) for sample in SAMPLES]
    results = list(map(int, results))
    # First DEPTH iterations are garbage.
    np.testing.assert_array_equal(RESULTS, results[-viterbi.lag :])


def test_run_stream_quantized(viterbi_fixed: Viterbi):
    """Compare Viterbi output values against golden when quantized."""
    test_run_stream(viterbi_fixed)
