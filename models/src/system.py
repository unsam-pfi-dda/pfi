"""System component manager."""

import functools
from collections.abc import Callable
from dataclasses import dataclass, field
from pathlib import Path
from typing import Any, TypeVar

import numpy as np
import yaml

from .agc import AGC
from .channel import Channel
from .component import ComponentBase
from .dfe import DFE
from .dffe import DFFE
from .ffe import FFE
from .generator import Generator
from .quantizer import Quantizer

T = TypeVar("T")


@dataclass
class System:
    """System class for managing multiple cascaded Components."""

    symbols: tuple[float, ...] = (-1, 1)
    generator = Generator.uniform
    components: list[ComponentBase] = field(default_factory=list)

    def __post_init__(self):
        self._update_lag()
        self.reset()

    @classmethod
    def from_yaml(
        cls,
        path: str | Path = Path("default.yaml"),
        modifier: Callable[[T], T] = lambda _d: _d,
    ):
        """Creates a System object from a YAML file."""
        if not isinstance(path, Path):
            path = Path(path)
        if not path.is_absolute():
            path = (Path(__file__).parent / "../configs/" / path).resolve()

        with open(path, encoding="utf-8") as file:
            data = modifier(yaml.safe_load(file))

        return cls.from_dict(data)

    @classmethod
    def from_dict(cls, data: dict[str, Any]):
        """Creates a System object from a dictionary."""
        if "Generator" not in data:
            raise ValueError("dictionary must contain 'Generator' key")
        if "symbols" not in data["Generator"]:
            raise ValueError("Generator must contain 'symbols' key - list symbols")

        system = cls(symbols=data.get("Generator").get("symbols"))  # type: ignore
        for _type, _args in data["Components"].items():
            system.add_component(globals()[_type](**_args))

        return system

    def add_component(self, component: ComponentBase):
        """Adds a new component to the system (by default adds to the last position)."""
        self.components.append(component)
        self._update_lag()

    def replace_component(
        self,
        component_type: type[ComponentBase],
        component: ComponentBase,
    ):
        """Replace a Component object from the System."""
        index = self.components.index(self.get_component(component_type))
        self.components[index] = component
        self._update_lag()

    def get_component(self, component_type: type[T]) -> T:
        """Get the Component object from the System."""
        for component in self.components:
            if isinstance(component, component_type):
                return component

        raise ValueError("Invalid component")

    def run(self, sample: np.float64 | None = None) -> np.float64:
        """Runs the system once cycle."""
        if not sample:
            sample = self.generator(symbols=self.symbols)

        result = sample
        for component in self.components:
            result = component.run(result)

        for component in self.components:
            component.adapt(self.error)

        return self.output

    def reset(self):
        """Resets all components."""
        for component in self.components:
            component.reset(self.symbols[0])  # type: ignore

    def _update_lag(self):
        """Calculate and save the entire system lag and each component lag."""
        offset = 0
        for component in reversed(self.components):
            component.adapt_offset = offset
            # TODO: Need to make lag/offset values consistent, either all positive or negative.
            offset -= component.lag

    @functools.cached_property
    def lag(self) -> int:
        """Returns the entire system lag (negative number)."""
        return sum([component.lag for component in self.components])

    @property
    def input(self) -> np.float64:
        """Returns the System input value."""
        return self.components[0].input.last

    @property
    def output(self) -> np.float64:
        """Returns the System output value."""
        return self.components[-1].output.last

    @property
    def error(self) -> np.float64:
        """Calculate the system output error signal."""
        try:
            return self.quantizer.error
        except ValueError:
            return 0.0

    @property
    def training(self) -> np.float64:
        """Calculates the system error against the input signal (not output signal)."""
        _output = self.components[-1].output.last
        _input = self.components[0].input.value(self.lag)
        return _output - _input

    @property
    def is_error(self) -> bool:
        """Calculate the system output error signal."""
        return self.training != 0

    @functools.cached_property
    def channel(self) -> Channel:
        """Returns the channel component object."""
        return self.get_component(Channel)

    @functools.cached_property
    def agc(self) -> AGC:
        """Returns the agc component object."""
        return self.get_component(AGC)

    @functools.cached_property
    def forward(self) -> FFE:
        """Returns the forward component object."""
        return self.get_component(FFE)

    @functools.cached_property
    def feedback(self) -> DFFE | DFE:
        """Returns the feedback component object."""
        try:
            return self.get_component(DFFE)
        except ValueError:
            return self.get_component(DFE)

    @functools.cached_property
    def quantizer(self) -> Quantizer:
        """Returns the quantizer component object."""
        return self.get_component(Quantizer)
