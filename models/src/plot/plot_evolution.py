from typing import TypeAlias

import plotly.graph_objects as go
import plotly.subplots

PlotEvolutionType: TypeAlias = list[dict[str, str | int | float]]


def plot_evolution(data: PlotEvolutionType) -> go.Figure:
    """Component evolution plot.

    Example of input data:
        data = [
            {
                "name": "FFE[0]",
                "x": [1, 2, 3, 5, 10, 100, 200, 300, 500, 1000],
                "y": [0, 0, 0, 0.01, 0.02, 0.1, 0.12, 0.14, 0.22],
            },
            {
                "name": "FFE[1]",
                "x": [1, 2, 3, 5, 10, 100, 200, 300, 500, 1000],
                "y": [0, 0, 0, 0.02, 0.025, 0.12, 0.15, 0.17, 0.21],
            },
        ]
    """
    fig = plotly.subplots.make_subplots(
        rows=len(data),
        cols=1,
        shared_xaxes=True,
        shared_yaxes=True,
        vertical_spacing=0.01,
    )

    # Trace each data elements into it's own subplot.
    for row, trace in enumerate(data, start=1):
        fig.add_trace(
            go.Scatter(
                x=trace["x"],
                y=trace["y"],
                mode="lines+markers",
                name=trace["name"],
                yaxis="y1",
                marker_color="black",
                line={"width": 0.5, "shape": "spline"},
            ),
            row=row,
            col=1,
        )
        fig.update_yaxes(title={"text": rf"$\text{{{trace['name']}}}$"}, col=1, row=row)

    fig.update_xaxes(
        title={
            "text": r"$\text{Sample}$",
        },
        gridcolor="#CCC",
        griddash="dot",
        linecolor="#000",
        tickcolor="#000",
        ticklen=6,
        ticks="inside",
        mirror="ticks",
        showline=True,
    )
    fig.update_yaxes(
        zerolinecolor="#CCC",
        zerolinewidth=1,
        gridcolor="#CCC",
        griddash="dot",
        linecolor="#000",
        tickcolor="#000",
        ticklen=6,
        ticks="inside",
        mirror="ticks",
        showline=True,
    )

    fig.update_layout(
        height=1000,
        width=1000,
        showlegend=False,
        plot_bgcolor="#ffffff",
        paper_bgcolor="#ffffff",
    )

    fig.show()
    return fig


if __name__ == "__main__":
    pass
