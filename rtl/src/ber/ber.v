`include "defines.v"

module ber
  #(
    parameter NB_BER_ERROR          = `NB_BER_ERROR,
    parameter NB_BER_CORR_DEPTH     = `NB_BER_CORR_DEPTH,
    parameter NB_BER_STATE          = `NB_BER_STATE
  ) (
    input                           clk,
    input                           rst_n,
    input                           enable,

    input                           i_transmitted,
    input                           i_received,
    input                           i_read,                        

    output reg                      o_ready,
    output reg [NB_BER_ERROR-1:0]   o_errors,
    output reg [NB_BER_ERROR-1:0]   o_counter  
  );

  localparam BER_CORR_DEPTH   =     1 << NB_BER_CORR_DEPTH;


  reg [BER_CORR_DEPTH    - 1 : 0]   tx_buff;
  reg                               rx_buff;
  wire                              is_error;
  
  reg [NB_BER_CORR_DEPTH - 1 : 0]   tx_addr_buff;

  reg [NB_BER_ERROR      - 1 : 0]   error_counter;
  reg [NB_BER_ERROR      - 1 : 0]   checked_counter;

  reg [NB_BER_CORR_DEPTH - 1 : 0]   corr_accum;
  reg [NB_BER_CORR_DEPTH - 1 : 0]   corr_accum_reg;
  reg [NB_BER_CORR_DEPTH - 1 : 0]   position_reg;
  
  reg [NB_BER_STATE      - 1 : 0]   state;
  reg [NB_BER_CORR_DEPTH - 1 : 0]   counter_corr;


  // Buffer de transmision y recepcion
  always @(posedge clk or negedge rst_n) begin:buffers
    if(!rst_n) begin
      tx_buff <= 0;
      rx_buff <= 0;
    end
    else if(enable) begin
      // Store BER_CORR_DEPTH transmitted samples (shift register style)
      tx_buff <= {tx_buff[BER_CORR_DEPTH - 2 -: BER_CORR_DEPTH - 1],i_transmitted};
      rx_buff <= i_received;
    end
  end

  assign is_error = rx_buff ^ tx_buff[tx_addr_buff];


  // Latch counters to output ports
  always @(posedge clk or negedge rst_n) begin
    if(!rst_n) begin
      checked_counter   <= 0;
      error_counter     <= 0;
	  end
    else if(enable && state == `BER_READY) begin
      checked_counter   <= checked_counter + 1;
      error_counter     <= checked_counter < -1 ? error_counter + is_error : is_error;
    end
  end

  // Latch output counters when read signal
  always @(posedge clk or negedge rst_n) begin
    if(!rst_n) begin
      o_counter   <= 0;
      o_errors    <= 0;
      o_ready     <= 0;
    end
    else if(enable) begin
      if(i_read && !o_ready) begin
        o_counter <= checked_counter;
        o_errors  <= error_counter;
        o_ready   <= 1;
      end
      else begin
        o_ready   <= 0;
      end
    end
  end

  // State machine
  always @(posedge clk or negedge rst_n) begin
    if(!rst_n) begin
      state = `BER_CHECK_CORR;
    end
    else if(enable) begin
      case(state)
      
        `BER_CHECK_CORR: begin
          if(tx_addr_buff == (BER_CORR_DEPTH-1) && counter_corr == (BER_CORR_DEPTH-1))
            state = `BER_READY;
        end

        `BER_READY: begin
          state = `BER_READY;
        end
        
        default: 
          state = `BER_CHECK_CORR;
      endcase
    end
  end

  // Correlation checker and tx buffer selection
  always @(posedge clk or negedge rst_n)  begin
    if (!rst_n) begin
      tx_addr_buff   <= 0;
      counter_corr   <= 0;
      corr_accum     <= 0;
      corr_accum_reg <= ~0;
      position_reg   <= 0;
    end
    else begin
      if (enable) begin
        case(state)
          // Cycle through all tx_addr_buffers and select the one with least errors.
          `BER_CHECK_CORR: begin
            if (counter_corr == (BER_CORR_DEPTH-1)) begin
              if(corr_accum < corr_accum_reg) begin
                corr_accum_reg <= corr_accum;
                position_reg   <= tx_addr_buff;
              end
              tx_addr_buff      <= tx_addr_buff + 1;
              counter_corr      <= 0;
              corr_accum        <= 0;
            end
            else begin
                counter_corr      <= counter_corr + 1;
                corr_accum        <= corr_accum   + is_error;
            end
          end

          `BER_READY: begin
            counter_corr   <= 0;
            corr_accum     <= 0;
            tx_addr_buff   <= position_reg;
          end
        endcase
      end
    end
  end
endmodule
