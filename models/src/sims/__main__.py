"""Plot module main file."""

import argparse

from src import Plot

from .ber_calibration import ber_calibration
from .ber_iterations import ber_iterations
from .ber_quantization import ber_quantization
from .ber_sbvd import ber_sbvd
from .histogram_basic import histogram_basic
from .histogram_equalizer import histogram_equalizer
from .sqnr_dffe import sqnr_dffe
from .sqnr_ffe import sqnr_ffe
from .sqnr_lms import sqnr_lms
from .sqnr_sbvd import sqnr_sbvd

TESTS = {
    "ber": {
        "calibration": ber_calibration,
        "quantization": ber_quantization,
        "sbvd": ber_sbvd,
        "iterations": ber_iterations,
    },
    "histogram": {
        "basic": histogram_basic,
        "equalizer": histogram_equalizer,
    },
    "sqnr": {
        "ffe": sqnr_ffe,
        "dffe": sqnr_dffe,
        "sbvd": sqnr_sbvd,
        "lms": sqnr_lms,
    },
}


def main():
    """Plot module main routine."""
    parser = argparse.ArgumentParser(description="Test ")
    subparsers = parser.add_subparsers(help="Test type", dest="type")

    ber = subparsers.add_parser("ber", help="Bit error rate tests")
    ber.add_argument("test", choices=TESTS["ber"].keys(), help="Type of test to run")

    hist = subparsers.add_parser("histogram", help="Histogram simulations")
    hist.add_argument("test", choices=TESTS["histogram"].keys(), help="Test measure")

    fixed = subparsers.add_parser("sqnr", help="Fixed point quantization tests")
    fixed.add_argument("test", choices=TESTS["sqnr"].keys(), help="Component to test")

    args = parser.parse_args()

    test = TESTS[args.type][args.test]()

    Plot.save_plot(args.type, test)


if __name__ == "__main__":
    main()
