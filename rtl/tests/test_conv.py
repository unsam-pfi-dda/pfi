"""Conv module testbench"""

import sys
from pathlib import Path
import cocotb
import cocotb.regression
from cocotb.triggers import Timer
from cocotb.handle import HierarchyObject
from cocotb_test.simulator import Simulator
import numpy as np

sys.path.append(str(Path(__file__).parent.parent.parent.resolve()))

from models.src import SBVD


def test_conv(simulator: Simulator):
    """Run parametrized CoCoTB test with pytest"""
    simulator.run()


async def model_cosimulation(dut: HierarchyObject, channel: list[int]):
    """Cosimulate RTL and model and compare results"""
    drive_inputs(dut, channel)
    await Timer(1, units="ps")  # wait a bit

    results = get_output(dut)
    golden = SBVD.conv((-1, 1), channel)
    assert golden == results, print(golden, results, channel)


tf = cocotb.regression.TestFactory(model_cosimulation)
tf.add_option("channel", np.random.choice(range(10), size=(100, 3)).tolist())
tf.generate_tests()


def drive_inputs(dut: HierarchyObject, channel):
    dut.i_cursor.value = channel[0]
    dut.i_postc1.value = channel[1]
    dut.i_postc2.value = channel[2]


def get_output(dut: HierarchyObject):
    return [
        dut.o_adq_000.value.signed_integer,
        dut.o_adq_001.value.signed_integer,
        dut.o_adq_010.value.signed_integer,
        dut.o_adq_011.value.signed_integer,
        dut.o_adq_100.value.signed_integer,
        dut.o_adq_101.value.signed_integer,
        dut.o_adq_110.value.signed_integer,
        dut.o_adq_111.value.signed_integer,
    ]
