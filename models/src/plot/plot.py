from __future__ import annotations

from collections.abc import Callable
from datetime import datetime
from pathlib import Path
from typing import Any

import plotly.graph_objects as go
import ruamel.yaml
import yaml

from .plot_ber import plot_ber
from .plot_bitstream import plot_bitstream
from .plot_coefficients import plot_coefficients
from .plot_correlation import plot_correlation
from .plot_evolution import plot_evolution
from .plot_histogram import plot_histogram
from .plot_sqnr import plot_sqnr


class Plot:
    """Plot base class."""

    _plots: dict[str, Callable[..., go.Figure]] = {
        "ber": plot_ber,
        "bitstream": plot_bitstream,
        "coefficients": plot_coefficients,
        "correlation": plot_correlation,
        "evolution": plot_evolution,
        "histogram": plot_histogram,
        "sqnr": plot_sqnr,
    }

    @classmethod
    def plot(cls, plot_data: dict[str, Any]) -> go.Figure:
        """Plot from a dict."""
        return cls._plots[plot_data["type"]](plot_data["data"])

    @classmethod
    def to_png(cls, fig: go.Figure, path: Path | str) -> None:
        """Save plot as PNG file."""
        path = Path(path) if not isinstance(path, Path) else path
        fig.update_layout(margin={"l": 10, "r": 10, "t": 20, "b": 10})
        fig.write_image(
            path.with_suffix(".png"),
            width=fig.layout["width"] or 800,
            height=fig.layout["height"] or 800,
            scale=3,
            format="png",
        )

    @classmethod
    def from_file(cls, path: Path | str, *, png: bool = False) -> None:
        """Plot from a file."""
        path = Path(path) if not isinstance(path, Path) else path
        with path.open(encoding="utf-8") as file_handler:
            results: dict[str, Any] = yaml.load(file_handler, Loader=yaml.Loader)  # type: ignore
        fig = cls.plot(results)
        if png:
            cls.to_png(fig, path)

    @classmethod
    def to_file(cls, plot_type: str, plot_data: list[dict[str, Any]]) -> Path:
        """Dump results into a file."""
        if plot_type not in cls._plots:
            msg = f"Invalid plot type - got {plot_type}"
            raise ValueError(msg)

        date = f"{plot_type}_{datetime.now().strftime('%Y%m%d_%H%M_%f')}"
        _fname = (
            (Path(__file__).parent / "../../resources" / date)
            .with_suffix(".yaml")
            .resolve()
        )

        # Create resources folder if does not exists
        _fname.parent.mkdir(exist_ok=True)

        dump_data = {
            "type": plot_type,
            "data": plot_data,
            "file": _fname.name,
        }
        with _fname.open("w", encoding="utf-8") as file_handler:
            ruamel.yaml.dump(dump_data, file_handler)  # type: ignore
            print(f"{cls.__name__}.to_file() - plot saved {dump_data['file']}")

        return _fname

    @classmethod
    def save_plot(cls, plot_type: str, plot_data: list[dict[str, Any]]) -> None:
        """Dump the results into a file and then plots from it."""
        cls.from_file(cls.to_file(plot_type, plot_data), png=True)
