"""Plot histogram for a sample adquisition."""

import itertools
from collections.abc import Sequence

import numpy as np
import plotly.figure_factory as ff
import plotly.graph_objects as go
import plotly.subplots

PlotHistogramType = list[dict[str, str | list[list[float]]]]
DistPlotType = list[
    tuple[go.Histogram | None, go.Scatter | None, go.Scatter | None], ...
]


def get_from_distplot(distplot: go.Figure) -> DistPlotType:
    """Return a list of tuples for the displot elements (histogram, curve and rug)."""
    data = distplot.data
    histograms = [x for x in data if isinstance(x, go.Histogram)]
    curves = [x for x in data if isinstance(x, go.Scatter) and x.yaxis == "y"]
    rugs = [x for x in data if isinstance(x, go.Scatter) and x.yaxis == "y2"]

    return list(itertools.zip_longest(*map(reversed, [histograms, curves, rugs])))


def normalize(data: Sequence[float]) -> list[float]:
    """Normalize a vector."""
    return list(map(float, data / np.linalg.norm(data)))


def plot_histogram(data: PlotHistogramType) -> go.Figure:
    """Histogram plots.

    Example of input data:
        data = [
            {
                "name": "Reception",
                "values": [
                    [-0.9, -1.1, -1, -0.3],  # One symbol
                    [0.87, 1.01, 0.6, 1.2],  # Another symbol
                ]
            },
            {
                "name": "Estimation",
                "values": [
                    [-1.01, -1.02, -0.99],  # One symbol
                    [0.99, 1.01, 1.02],  # Anther symbol
                ]
            },
        ]
    """
    fig = plotly.subplots.make_subplots(
        rows=len(data) * 2,
        cols=1,
        shared_xaxes=True,
        vertical_spacing=0.02,
        specs=[[{"secondary_y": True}]] * len(data) * 2,
        row_heights=normalize([0.85, 0.1] * len(data)),  # make rug plot smaller
    )

    for idx, trace in enumerate(data, start=0):
        row = 2 * idx + 1
        # Compared to regular go.Histogram plots, displot also displays the # estimated
        # normal curve.= and a rug plot.
        # However there is a major drawback, that ff.displot figures can not be directly
        # added as a go.Figure subplot trace. The workaround is to manually copy the
        # traces back to the suplot.
        values = list(trace.get("values", []))
        dist = ff.create_distplot(
            hist_data=values,
            group_labels=list(range(len(values))),
            show_rug=True,
            show_curve=True,
            show_hist=True,
        )

        for histogram, curve, rug in get_from_distplot(dist):
            if histogram:
                histogram.update({"marker": {"color": "black"}, "opacity": 0.5})
                histogram.update({"nbinsx": 250, "xbins": None})
                fig.add_trace(histogram, secondary_y=False, row=row, col=1)
            if curve:
                show = len(set(histogram.x)) > 100 if histogram else True
                curve.update({"opacity": 0.75 if show else 0})
                fig.add_trace(curve, secondary_y=True, row=row, col=1)
            if rug:
                # Limit rug samples to display
                rug.update({"marker": {"maxdisplayed": 1_000}})
                rug.update({"marker": {"color": "black", "size": 2}, "opacity": 0.5})
                fig.add_trace(rug, row=row + 1, col=1)

        title = {"text": rf"$\text{{{trace['name']}}}$"}
        fig.update_yaxes(title=title, secondary_y=False, col=1, row=row)
        fig.update_yaxes(showticklabels=False, secondary_y=True, col=1, row=row)
        fig.update_yaxes(showticklabels=False, col=1, row=row + 1)

        fig.update_xaxes(
            gridcolor="#CCC",
            griddash="dot",
            linecolor="#000",
            tickcolor="#000",
            ticklen=6,
            ticks="inside",
            mirror="ticks",
            showline=True,
            row=row,
        )
        fig.update_yaxes(
            title={
                "standoff": 0.1,
                "font": {"size": 16},
            },
            gridcolor="#CCC",
            griddash="dot",
            linecolor="#000",
            tickcolor="#000",
            ticklen=6,
            ticks="inside",
            mirror="ticks",
            showline=True,
            secondary_y=False,
            row=row,
        )

    fig.update_xaxes(
        title={"text": r"$\text{Sample value}$", "font": {"size": 16}},
        row=len(data) * 2,
    )
    fig.update_layout(
        barmode="stack",
        height=200 * len(data),
        width=800,
        showlegend=False,
        plot_bgcolor="#ffffff",
        paper_bgcolor="#ffffff",
    )
    fig.show()
    return fig
