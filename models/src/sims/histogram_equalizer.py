"""Histogram simulation for an equalizer system.

This test runs multiple iterations for a System with a noisy channel that is
compensated by an adaptive equalizer (FFE+DFFE).
The output is an histogram comparing received samples against the estimated
samples.

How to run:
    python -m src.sims histogram equalizer

The results will be sent to the Plot module (./src/Plot) for storing and
plotting with plotly.
"""
from collections import defaultdict
from collections.abc import Sequence

import numpy as np
from scipy import signal
from tqdm import tqdm

from src import System


def histogram_equalizer():
    samples = 100_000
    system = System.from_yaml("simple.yaml")

    results: dict[str, dict[float, list[float]]] = defaultdict(
        lambda: defaultdict(list[float])
    )

    # Initial noiseless coefficient adaptation
    snr = system.channel.locals["snr"]
    system.channel.snr = np.inf

    for _ in tqdm(range(10_000), desc="Adapt"):
        system.run()

    # Find sample offsets (lag) for signals
    def correlate(a: Sequence[float], b: Sequence[float]) -> int:
        corr = signal.correlate(a, b)
        lags = signal.correlation_lags(len(a), len(b))
        return int(lags[max(np.argwhere(corr == np.amax(corr)))])

    lag = correlate(system.quantizer.output.values, system.channel.input.values)
    receive_lag = correlate(
        system.quantizer.output.values,
        system.channel.output.values,
    )

    # Update noise value and start measurements
    system.channel.snr = snr
    for _ in tqdm(range(10_000), desc=f"Adapt SNR:{snr}"):
        system.run()

    for _ in tqdm(range(samples), desc="Measure"):
        system.run()
        sample = system.channel.input.values[-lag - 1]
        results["Received"][sample].append(
            float(system.channel.output.values[-receive_lag - 1])
        )
        results["Estimated"][sample].append(float(system.quantizer.input.last))

    # Rearrange data dictionary into compatible plot format
    return [
        {"name": name, "values": list(result.values())}
        for name, result in results.items()
    ]
