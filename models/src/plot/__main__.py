"""Plot module main file."""

import argparse
import pathlib

from .plot import Plot


def main():
    """Plot module main routine."""
    parser = argparse.ArgumentParser(description="Plot results from a YAML file")
    parser.add_argument(
        "path",
        type=pathlib.Path,
        help="Path to a YAML file with plot data",
    )
    parser.add_argument(
        "--png",
        action="store_true",
        default=True,
        help="Export plot to .png as well",
    )
    args = vars(parser.parse_args())

    Plot.from_file(**args)


if __name__ == "__main__":
    main()
