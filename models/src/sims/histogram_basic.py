"""Histogram simulation for AWGN, ISI and AWGN+ISI examples.

This test run a very primitive System, containing only a Channel, for
many iterations while saving the input, response, noise and output buffers.
The output will be an histogram plot for all four measurements and will be
used for explaining the effects for ISI and AWGN in a transmission.

How to run:
    python -m src.sims histogram basic

The results will be sent to the Plot module (./src/Plot) for storing and
plotting with plotly.
"""
from collections import defaultdict

from tqdm import tqdm

from src import Channel, Quantizer, System


def histogram_basic():
    """Histogram simulation for basic examples."""
    samples = 100_000

    system = System(
        components=[
            Channel(taps=[0.25, 0.5, 1, 0.5, 0.125], snr=9, normalize=False),
            Quantizer(),
        ],
    )

    results: dict[str, dict[float, list[float]]] = defaultdict(
        lambda: defaultdict(list[float])
    )

    lag = system.channel.lag
    for _ in tqdm(range(samples), desc="Measure"):
        system.run()

        sample = float(system.channel.input.values[lag - 1])
        results["ISI"][sample].append(float(system.channel.response.last))
        results["AWGN"][sample].append(float(system.channel.noise.last + sample))
        results["ISI + AWGN"][sample].append(float(system.channel.output.last))

    # Rearrange data dictionary into compatible plot format
    return [
        {"name": name, "values": list(result.values())} for name, result in results.items()
    ]
