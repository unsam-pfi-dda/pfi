from typing import TypeAlias

import numpy as np
import plotly.graph_objects as go
from plotly.colors import DEFAULT_PLOTLY_COLORS
from scipy.special import erfc

PlotBerType: TypeAlias = list[dict[str, str | list[list[tuple[int, int, int, float]]]]]


def plot_ber(data: PlotBerType) -> go.Figure:
    """BER Plot (Bit Error Rate).

    Example of input data:
        data = [
            {
                "group": "No channel",
                # "symbol": "circle",
                "results": [
                    [
                        (0, 5976, 74607, 0.08009972254614178),
                        (1, 4277, 75007, 0.05702134467449705),
                        (2, 2850, 75679, 0.03765905997700815),
                        (3, 1738, 74984, 0.023178278032646965),
                        (4, 1000, 82102, 0.012179971255267838),
                    ], # Run 0
                ],
            },
            {
                "group": "No channel + FFE + DFFE",
                "symbol": "star",
                "color": 1,
                "results": [
                    [
                        (0, 5976, 71055, 0.08410386320455984),
                        (1, 4277, 71516, 0.059804798926114434),
                        (2, 2850, 71435, 0.039896409323160915),
                        (3, 1738, 71240, 0.024396406513194833),
                        (4, 1000, 75459, 0.013252229687644947),
                    ], # Run 0
                    [
                        (0, 5976, 72110, 0.08287338787962835),
                        (1, 4277, 70983, 0.06025386360114394),
                        (2, 2850, 70087, 0.040663746486509625),
                        (3, 1738, 71698, 0.02424056459036514),
                        (4, 1000, 72352, 0.013821318000884564),
                    ], # Run 1
                ],
            },
        ]
    """
    # Find the maximum SNR plotted in the data
    snr = [
        int(_snr)
        for test in data
        for iteration in test["results"]
        for _snr, *_ in iteration
    ]
    snr_min = min(snr)
    snr_max = max(snr)

    # Ideal BER for QPSK with no loss
    ideal_x = np.arange(snr_min, snr_max, 0.01)
    ideal_y = 0.5 * erfc(np.sqrt(10 ** (ideal_x / 10)))

    # Plot SNR test results
    fig = go.Figure()

    # Subplot 1: Error
    fig.add_trace(
        go.Scatter(
            x=ideal_x,
            y=ideal_y,
            name="BPSK ideal",
            mode="lines",
            line={"width": 1, "dash": "solid"},
            showlegend=True,
        ),
    )

    range_min = np.inf
    range_max = -np.inf
    for _id, (_test) in enumerate(data):
        for _run, (_result) in enumerate(_test["results"]):
            snr, errors, samples, ber = zip(*_result)
            ber = [err / sam for err, sam in zip(errors, samples)]

            color_id = _test.get("color", (_id + 1) % 10)
            color = DEFAULT_PLOTLY_COLORS[color_id]
            # Default symbol to 'circle' if not specified
            symbol = _test["symbol"] if "symbol" in _test else "circle"

            fig.add_trace(
                go.Scatter(
                    x=snr,
                    y=ber,
                    name=f"{_test['group']}",
                    mode="lines+markers",
                    line={"width": 0.2, "dash": "solid", "shape": "spline"},
                    marker_symbol=symbol,
                    marker_color=color,
                    marker_size=10,
                ),
            )
            range_min = np.min([min(ber), range_min])
            range_max = np.max([max(ber), range_max])

    fig.update_xaxes(
        title={
            "text": r"$\text{Signal to Noise Ratio [dB]}$",
            "font": {"size": 18},
        },
        tickmode="linear",
        gridcolor="#CCC",
        griddash="dot",
        linecolor="#000",
        tickcolor="#000",
        ticklen=6,
        ticks="inside",
        mirror="ticks",
        showline=True,
    )
    fig.update_yaxes(
        title={
            "text": r"$\text{Bit Error Rate}$",
            "standoff": 0.05,
            "font": {"size": 18},
        },
        type="log",
        exponentformat="power",
        dtick="D10",
        minor={
            "tickmode": "linear",
            "ticklen": 3,
            "ticks": "inside",
            "dtick": "D1",
            "nticks": 10,
            "showgrid": True,
            "gridcolor": "#EEE",
            "griddash": "dot",
        },
        ticklabelstep=10,
        gridcolor="#CCC",
        griddash="dot",
        linecolor="#000",
        tickcolor="#000",
        ticklen=6,
        ticks="inside",
        mirror="ticks",
        showline=True,
        range=[np.floor(np.log10(range_min)), np.ceil(np.log10(range_max))],
    )

    fig.update_layout(
        height=1000,
        width=1000,
        legend={
            "yanchor": "bottom",
            "y": 0.02,
            "xanchor": "left",
            "x": 0.02,
        },
        plot_bgcolor="#ffffff",
        paper_bgcolor="#ffffff",
    )

    fig.show()
    return fig


if __name__ == "__main__":
    pass
