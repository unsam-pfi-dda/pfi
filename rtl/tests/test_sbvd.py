"""Sliding block viterbi decoder top testbench"""

import sys
from pathlib import Path
import pytest
import cocotb
from cocotb.triggers import FallingEdge, RisingEdge, ClockCycles
from cocotb.clock import Clock
from cocotb.handle import HierarchyObject
from cocotb_test.simulator import Simulator
from typing import Sequence
from itertools import islice, product, chain
import numpy as np

sys.path.append(str(Path(__file__).parent.parent.parent.resolve()))

from models.src import SBVD
from rtl.tests.utils import binstr2list, list2binstr


@pytest.mark.parametrize("depth", ["2", "4", "10"])
def test_sbvd(simulator: Simulator, depth: str):
    """Run parametrized CoCoTB test with pytest"""
    simulator.parameters["D"] = depth
    simulator.run()


SYMBOLS = (-1, 1)


@cocotb.test()
async def initialization(dut: HierarchyObject):
    """Initialize the system and checks that everything is setup as expected"""

    await startup(dut)
    dut.i_coef.value = list2binstr([1, -2, 3], dut.COEF_W.value)
    dut.i_sample.value = list2binstr(list(range(dut.M.value)), dut.SAMP_W.value)

    await ClockCycles(dut.clk, 1)
    assert [1, -2, 3] == binstr2list(dut.i_coef.value.binstr, dut.COEF_W.value)
    assert list(range(dut.M.value)) == binstr2list(
        dut.i_sample.value.binstr, dut.SAMP_W.value
    )

    for step in range(dut.M.value):
        forward = dut.slide[step].u_forward_sliding_block
        backward = dut.slide[step].u_backward_sliding_block

        # Only stages [D, 2D-1] should decode, sync stages do not.
        assert forward.DECODE.value == 0 if step < dut.D.value else 1
        assert backward.DECODE.value == 0 if step < dut.D.value else 1

        # Symbol lag (input) should start from 1 for forward, and from 0 for backward
        assert forward.LAG_SYMBOL.value == step + 1
        assert backward.LAG_SYMBOL.value == step

    for iteration, step in enumerate(reversed(range(dut.M.value))):
        forward = dut.slide[step].u_forward_sliding_block
        backward = dut.slide[step].u_backward_sliding_block

        # Decision lag step is two per stage, and the last one (2D-1) should be 3.
        # Reversing the order is way simpler for this
        assert forward.LAG_DECISION.value == 2 * iteration + 3
        assert backward.LAG_DECISION.value == 2 * iteration + 3
        assert forward.LAG_DATA.value == dut.D.value - iteration
        assert backward.LAG_DATA.value == dut.D.value - iteration


@cocotb.test()
async def word_alignment(dut: HierarchyObject):
    """Flood the system with a single symbol, verifies that all operations
    are word aligned"""
    # Create golden object
    sbvd = SBVD(depth=dut.D.value, taps=2, symbols=SYMBOLS)

    await startup(dut)
    dut.i_coef.value = list2binstr([4, 2, 1], dut.COEF_W.value)
    dut.i_sample.value = list2binstr([7] * dut.M.value, dut.SAMP_W.value)
    await ClockCycles(dut.clk, 1)  # Fill both forward and backward with data
    await ClockCycles(dut.clk, abs(sbvd.lag))  # Wait lag cycles
    await FallingEdge(dut.clk)
    np.testing.assert_array_equal([1] * dut.M.value, get_output(dut))


async def model_cosimulation(
    dut: HierarchyObject, channel: Sequence[int], noise: float
):
    """Cosimulate RTL and model and compare results"""
    # Create golden object
    sbvd = SBVD(depth=dut.D.value, taps=2, symbols=SYMBOLS)

    await startup(dut)
    dut.i_coef.value = list2binstr(channel, dut.COEF_W.value)

    sequence = (
        [-1, -1]
        + [-1] * 2 * dut.M.value
        + [1] * dut.M.value
        + [-1] * dut.M.value
        + list(
            chain.from_iterable(shift([SYMBOLS[1]] + [SYMBOLS[0]] * (dut.M.value - 1)))
        )
        + [-1] * 2 * dut.M.value
        + list(chain.from_iterable(islice(product(SYMBOLS, repeat=dut.M.value), 1_000)))
    )
    stream = np.convolve(sequence, channel, mode="valid")
    stream = stream + (np.random.normal(loc=0, size=len(stream)) * noise)

    golden = [sbvd.run(samples, channel) for samples in stream]
    results = []

    for samples in chunk(stream, dut.M.value):
        await FallingEdge(dut.clk)
        dut.i_sample.setimmediatevalue(list2binstr(samples, dut.SAMP_W.value))
        results.extend(get_output(dut))

    # Drop the first samples
    golden = golden[abs(sbvd.lag) :]
    results = results[abs(sbvd.clocks) :]
    # Match both buffer sizes
    golden, results = zip(*zip(golden, results))

    np.testing.assert_array_equal(golden, results)


tf = cocotb.regression.TestFactory(model_cosimulation)
tf.add_option("noise", [0])
tf.add_option(
    "channel",
    [
        *list(product((0, 1), repeat=3))[1:],
        [4, 2, 1],
        [5, 1, 0],
        [0b01110000, 0b00111000, 0b00011100],
    ],
)
tf.generate_tests()


async def startup(dut: HierarchyObject):
    cocotb.start_soon(Clock(dut.clk, 1, units="ns").start())
    dut.enable.value = 1
    await RisingEdge(dut.clk)
    dut.rst_n.value = 0
    await ClockCycles(dut.clk, 1)
    dut.rst_n.value = 1


def get_output(dut: HierarchyObject):
    return [SYMBOLS[value] for value in binstr2list(dut.o_data.value.binstr, bits=1)]


def chunk(arr_range, arr_size):
    arr_range = iter(arr_range)
    return iter(lambda: tuple(islice(arr_range, arr_size)), ())


def shift(data: list[int]):
    for _ in data:
        yield data.copy()
        data.insert(0, data.pop())
