"""DFFE iterations BER test.

This test will measure the Bit Error Rate for DFFE-VA with different iteration
values.

Note:
    All tests will be executed in a new process call and it, probably, will use 100% CPU.

How to run:
    python -m src.sims ber iterations

The results will be sent to the Plot module (./src/Plot) for storing and plotting with plotly.
"""

import functools
from typing import TypeVar

from src import DFFE, System

from .core import BerTestDirector

T = TypeVar("T")


def ber_iterations():
    tests = BerTestDirector()
    sweep = "iterations"
    sweep_values = range(1, 16)
    reference = DFFE
    adapt_samples = 40_000

    for mlse_depth in [0, 5, 10]:

        def modifier_fixed(depth: int, cfg: T) -> T:
            """Modifier for System configuration file for testing the system (DFFE-VA) with
            different Viterbi depths.
            """
            cfg["Components"]["DFFE"]["mlse_depth"] = depth
            return cfg

        name = f"DFFE-VA({mlse_depth})"
        system_fixed = System.from_yaml(
            "default.yaml",
            functools.partial(modifier_fixed, mlse_depth),
        )
        tests.add_test(
            name,
            system_fixed,
            sweep,
            sweep_values,
            reference,
            adapt_samples,
            "circle",
        )

    # Test: Floating test
    def modifier_floating(cfg: T) -> T:
        """Modifier for System configuration file for testing the floating system (DFFE-VA)."""
        for _component in cfg["Components"].values():
            if "quantize" in _component:
                del _component["quantize"]
        cfg["Components"]["DFFE"]["mlse_depth"] = 10
        return cfg

    name = "DFFE-VA(10)(float)"
    system_floating = System.from_yaml("default.yaml", modifier_floating)
    tests.add_test(
        name,
        system_floating,
        sweep,
        sweep_values,
        reference,
        adapt_samples,
        "circle",
    )

    return tests.run()
