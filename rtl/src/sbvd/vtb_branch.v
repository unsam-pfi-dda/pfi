`include "defines.v"

module vtb_branch #(
  parameter COEF_W   = `COEF_W,
  parameter ADQ_W    = `ADQ_W,
  parameter SM_WIDTH = `SM_WIDTH,
  parameter SAMP_W   = `SAMP_W,
  parameter BM_WIDTH = `BM_WIDTH,

  parameter DIRECTION = 0 // 0=Forward; 1=Backward   
) (

  input             clk   ,
  input             rst_n ,
  input             enable,
  input  [SM_WIDTH-1:0] i_sm_00  ,  // I State metric 00.
  input  [SM_WIDTH-1:0] i_sm_01  ,  // I State metric 01.
  input  [SM_WIDTH-1:0] i_sm_10  ,  // I State metric 10.
  input  [SM_WIDTH-1:0] i_sm_11  ,  // I State metric 11.
  input  signed   [ADQ_W-1   :0] i_adq_000,  // I Ideal adquisition of state 00 -> 00.
  input  signed   [ADQ_W-1   :0] i_adq_001,  // I Ideal adquisition of state 00 -> 01.
  input  signed   [ADQ_W-1   :0] i_adq_010,  // I Ideal adquisition of state 01 -> 10.
  input  signed   [ADQ_W-1   :0] i_adq_011,  // I Ideal adquisition of state 01 -> 11.
  input  signed   [ADQ_W-1   :0] i_adq_100,  // I Ideal adquisition of state 10 -> 00.
  input  signed   [ADQ_W-1   :0] i_adq_101,  // I Ideal adquisition of state 10 -> 01.
  input  signed   [ADQ_W-1   :0] i_adq_110,  // I Ideal adquisition of state 11 -> 10.
  input  signed   [ADQ_W-1   :0] i_adq_111,  // I Ideal adquisition of state 11 -> 11.
  input  signed   [SAMP_W-1  :0] i_sample ,  // I Real adquisition (sample).

  output reg      [SM_WIDTH-1:0] o_sm_00  ,  // I State metric 00.
  output reg      [SM_WIDTH-1:0] o_sm_01  ,  // I State metric 01.
  output reg      [SM_WIDTH-1:0] o_sm_10  ,  // I State metric 10.
  output reg      [SM_WIDTH-1:0] o_sm_11  ,  // I State metric 11.
  output                         o_dec_00 ,  // O Decision bits.
  output                         o_dec_01 ,  // O Decision bits.
  output                         o_dec_10 ,  // O Decision bits.
  output                         o_dec_11    // O Decision bits.
);

  wire [BM_WIDTH-1 :0] delta_000    ;
  wire [BM_WIDTH-1 :0] delta_001    ;
  wire [BM_WIDTH-1 :0] delta_010    ;
  wire [BM_WIDTH-1 :0] delta_011    ;
  wire [BM_WIDTH-1 :0] delta_100    ;
  wire [BM_WIDTH-1 :0] delta_101    ;
  wire [BM_WIDTH-1 :0] delta_110    ;
  wire [BM_WIDTH-1 :0] delta_111    ;
  reg  [BM_WIDTH-1 :0] delta_000_reg;
  reg  [BM_WIDTH-1 :0] delta_001_reg;
  reg  [BM_WIDTH-1 :0] delta_010_reg;
  reg  [BM_WIDTH-1 :0] delta_011_reg;
  reg  [BM_WIDTH-1 :0] delta_100_reg;
  reg  [BM_WIDTH-1 :0] delta_101_reg;
  reg  [BM_WIDTH-1 :0] delta_110_reg;
  reg  [BM_WIDTH-1 :0] delta_111_reg;
  wire [SM_WIDTH-1:0]  sm_00        ;
  wire [SM_WIDTH-1:0]  sm_01        ;
  wire [SM_WIDTH-1:0]  sm_10        ;
  wire [SM_WIDTH-1:0]  sm_11        ;
  wire                 dec_00       ;
  wire                 dec_01       ;
  wire                 dec_10       ;
  wire                 dec_11       ;


  branch_metrics #(
    .SAMP_W  (SAMP_W  ),
    .ADQ_W   (ADQ_W   ),
    .BM_WIDTH(BM_WIDTH)
  ) u_branch_metrics (
    .i_adq_000(i_adq_000),
    .i_adq_001(i_adq_001),
    .i_adq_010(i_adq_010),
    .i_adq_011(i_adq_011),
    .i_adq_100(i_adq_100),
    .i_adq_101(i_adq_101),
    .i_adq_110(i_adq_110),
    .i_adq_111(i_adq_111),
    .i_sample (i_sample ),
    .o_delta_000(delta_000),
    .o_delta_001(delta_001),
    .o_delta_010(delta_010),
    .o_delta_011(delta_011),
    .o_delta_100(delta_100),
    .o_delta_101(delta_101),
    .o_delta_110(delta_110),
    .o_delta_111(delta_111) 
  );

  always @(posedge clk or negedge rst_n) begin
    if (!rst_n) begin
      delta_000_reg <= {BM_WIDTH{1'b0}};
      delta_001_reg <= {BM_WIDTH{1'b0}};
      delta_010_reg <= {BM_WIDTH{1'b0}};
      delta_011_reg <= {BM_WIDTH{1'b0}};
      delta_100_reg <= {BM_WIDTH{1'b0}};
      delta_101_reg <= {BM_WIDTH{1'b0}};
      delta_110_reg <= {BM_WIDTH{1'b0}};
      delta_111_reg <= {BM_WIDTH{1'b0}};
    end
    else if (enable) begin
      delta_000_reg <= delta_000;
      delta_001_reg <= delta_001;
      delta_010_reg <= delta_010;
      delta_011_reg <= delta_011;
      delta_100_reg <= delta_100;
      delta_101_reg <= delta_101;
      delta_110_reg <= delta_110;
      delta_111_reg <= delta_111;
    end
  end

  // 4-State ACS
  four_state_acs #(
    .SM_WIDTH(SM_WIDTH),
    .BM_WIDTH(BM_WIDTH),
    .DIRECTION(DIRECTION)
  ) u_four_state_acs (
    .i_sm_00    (i_sm_00      ),
    .i_sm_01    (i_sm_01      ),
    .i_sm_10    (i_sm_10      ),
    .i_sm_11    (i_sm_11      ),
    .i_delta_000(delta_000_reg),
    .i_delta_001(delta_001_reg),
    .i_delta_010(delta_010_reg),
    .i_delta_011(delta_011_reg),
    .i_delta_100(delta_100_reg),
    .i_delta_101(delta_101_reg),
    .i_delta_110(delta_110_reg),
    .i_delta_111(delta_111_reg),
    .o_sm_00    (sm_00        ),
    .o_sm_01    (sm_01        ),
    .o_sm_10    (sm_10        ),
    .o_sm_11    (sm_11        ),
    .o_dec_00   (o_dec_00       ),
    .o_dec_01   (o_dec_01       ),
    .o_dec_10   (o_dec_10       ),
    .o_dec_11   (o_dec_11       )
  );

  always @(posedge clk or negedge rst_n) begin
    if (!rst_n) begin
      o_sm_00  <= {SM_WIDTH{1'b0}};
      o_sm_01  <= {SM_WIDTH{1'b0}};
      o_sm_10  <= {SM_WIDTH{1'b0}};
      o_sm_11  <= {SM_WIDTH{1'b0}};
    end
    else if (enable) begin
      o_sm_00  <= sm_00  ;
      o_sm_01  <= sm_01  ;
      o_sm_10  <= sm_10  ;
      o_sm_11  <= sm_11  ;
    end
  end

// *****************************************************************************  
endmodule
