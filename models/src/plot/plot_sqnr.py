import math
from typing import TypeAlias

import plotly.graph_objects as go
import plotly.subplots
from plotly.colors import DEFAULT_PLOTLY_COLORS

PlotQuantizeType: TypeAlias = list[dict[str, list[dict[str, dict[str, str | float]]]]]


def minmax(figure: go.Figure):
    """Get minimum and maximum values for all figure traces data."""
    min_x = float("inf")  # Initialize with positive infinity
    max_x = float("-inf")  # Initialize with negative infinity
    min_y = float("inf")  # Initialize with positive infinity
    max_y = float("-inf")  # Initialize with negative infinity

    for trace in figure.data:
        if hasattr(trace, "x") and trace.x:
            min_x = min(min_x, min(trace.x))
            max_x = max(max_x, max(trace.x))
        if hasattr(trace, "y") and trace.y:
            min_y = min(min_y, min(trace.y))
            max_y = max(max_y, max(trace.y))

    return (min_x, max_x, min_y, max_y)


def plot_sqnr(data: PlotQuantizeType) -> go.Figure:
    """Scatterplot for different quantization runs."""
    fig = plotly.subplots.make_subplots(
        cols=1,
        rows=len(data),
        shared_xaxes=True,
        shared_yaxes=True,
        vertical_spacing=0.075,
        subplot_titles=[x.get("group") for x in data],
    )

    for index, _test in enumerate(data, start=1):
        for _result in _test["results"]:
            x_data, y_data = zip(*_result["data"].items())

            fig.add_trace(
                go.Scatter(
                    x=x_data,
                    y=y_data,
                    name=f"{_test['group']}: {_result['integer']}",
                    mode="lines+markers",
                    line={"width": 0.2, "dash": "solid"},
                    marker_symbol=_test["symbol"],
                    marker_size=8,
                    marker_color="black",
                ),
                col=1,
                row=index,
            )
            fig.add_selection(
                x0=0,
                x1=20,
                y0=40,
                y1=100,
                opacity=0,
                col=1,
                row=index,
            )
            fig.add_annotation(
                x=x_data[-1],
                y=y_data[-1],
                text=f"$\\text{{{_result['integer']} integer bits}}$",
                font={"size": 14},
                showarrow=False,
                yshift=20,
                col=1,
                row=index,
            )

    # Add 40dB threshold area
    fig.add_hrect(y0=-5, y1=40, line_width=0, fillcolor="red", opacity=0.05)
    fig.add_hline(y=40, line={"width": 0.1, "color": "red"})

    min_x, max_x, *_ = minmax(fig)

    fig.update_xaxes(
        title={
            "text": r"$\text{Fractional bits}$",
            "font": {"size": 18},
        },
        col=1,
        row=len(data),
    )
    fig.update_xaxes(
        tickmode="linear",
        gridcolor="#CCC",
        griddash="dot",
        linecolor="#000",
        tickcolor="#000",
        ticklen=6,
        ticks="inside",
        mirror="ticks",
        showline=True,
        range=[min_x - 1, max_x + 1],
    )

    fig.update_yaxes(
        title={
            "text": r"$\text{Signal to Quantization Noise Ratio [dB]}$",
            "standoff": 0.05,
            "font": {"size": 18},
        },
        row=math.ceil(len(data) / 2),
    )
    fig.update_yaxes(
        zerolinecolor="#CCC",
        zerolinewidth=0.5,
        gridcolor="#CCC",
        griddash="dot",
        linecolor="#000",
        tickcolor="#000",
        ticklen=6,
        ticks="inside",
        mirror="ticks",
        showline=True,
        range=(-5, 55),
    )

    fig.update_layout(
        height=600,
        width=800,
        showlegend=False,
        plot_bgcolor="#ffffff",
        paper_bgcolor="#ffffff",
    )

    # fig.show()

    return fig


if __name__ == "__main__":
    pass
