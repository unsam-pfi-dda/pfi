# Model - Python simulator

Provides a high level model for a digital equalizer system simulator.

## Quick guide

Make sure to have `python 3.10` and all the dependencies installed:

```shell
python --version  # make sure it's 3.10 or higher
python -m pip install -r requirements.txt
```

There are two ways to run the simulator: the evolution and a simulation.

### System evolution

It will run the provided system for a specified number of samples and then report a system coefficient and error evolution over samples, as well as the final state of each component.

```shell
python main.py [--samples <int>] [--config <path>] 
```

By default will run `30_000` samples and use [default.yaml](./configs/default.yaml) configuration file.

### Simulation

Simulation runs will run multiple systems in parallel and return different results for each system. There are two major simulation groups: `ber` and `sqnr`.

To run a simulation: see the suggested options by running

```shell
python -m src.sims {ber|sqnr}
```

For example, the [Bit Error Rate quantization simulation](./src/sims/ber_quantization.py) will run three systems in parallel (`DFE`, `DFFE-VA` and `fixed-point DFFE-VA`) and return a BER plot.

```shell
python -m src.sims ber quantization
```

## File structure

### System

The source code is located in `./src` folder. The main object class is [System](./src/system.py), which will generate a "generic" system composed by any number of `Components` (which can be [Channel](./src/channel.py), [FFE](./src/ffe.py), [DFE](./src/dfe.py), [DFFE](./src/dffe.py) or [Quantizer](./src/quantizer.py) among others).

The system structure is provided by `YAML` files, which are located in [configs](./configs/) folder.

### Simulators

The simulator code are located in [sims](./src/sims/) folder (inside [src](./src)), which contains two [core classes](./src/sims/core/) (one for [ber](./src/sims/core/ber_core.py) tests and another for [sqnr](./src/sims/core/sqnr_core.py) tests).

There are several different simulations, but all of them falls into theese two groups (`ber` and `sqnr`). Individual tests can be found in [sims](./src/sims/) folder.

The simulation output will be stored in a `YAML` file in [resources](./resources/) folder, which can be parsed by [Plot](#plot) module.

### Plot

This module is located in [plot](./src/plot/) folder and will use `Plotly` package to generate an interactive plot with the simulation result files.

It has two submodules, [ber](./src/plot/plot_ber.py) and [sqnr](./src/plot/plot_sqnr.py), but they can be automatically detected by the higher-level [Plot](./src/plot/plot.py) class.

### Unit testing

Some components have unit testing scripts, located in [tests](./tests/) folder. It uses `pytest` and `hypothesis` packages for functional and property testing.

### Diagram

```plantuml
@startuml

!theme plain
skinparam roundcorner 0
hide circle


class System {
    symbols: List[float]
    generator: Callable[[], float]
    components: List[ComponentBase]
    from_yaml(path: str) -> System
    run(sample: float) -> Tuple[float, float]
}

class ComponentBase {
    input: Wave
    noise: Waive
    response: Waive
    output: Wave
    lag: int
    run(input: float) -> float
    adapt(error: float) -> None
}

class Channel {
    taps: int
    snr: float
}

class AGC {
    gain: float
}

class FFE {
    taps: int
    step: float
}

class DFFE {
    taps: int
    iterations: int
    step: float
    mlse: SBVD
}

class SBVD {
    input: Wave
    output: Wave
    depth: int
    taps: int
    lag: int
    run(input: float) -> float
}

class Wave {
    values: list[float]
    last: float
    set(new: list[float] -> None
    add(new: float) -> None
}

class BerTest {
    system: System
    sweep: str
    values: List[int]
    reference: ComponentBase
    tests: List[BerTestSingle]
    create_tests() -> None
    results() -> List[Tuple[System, int, int]]
}

class BerTestSingle {
    system: System
    samples: int
    errors: int
    ber: float
    results() -> Tuple[System, int, int]
    run() -> Tuple[System, int, int]
}

class BerTestDirector {
    tests: List[BerTest]
    add_test(system: System, sweep: str, values: List[int], reference: ComponentBase) -> None
    run() -> None
    results() -> List[Tuple[System, int, int]]
}

class BerCalibration {
}

class BerIterations {
}

class BerQuantization {
}

class BerSBVD {
}

class SQNRTestDirector{
  measure: int
  system: System
  reference: ComponentBase
  tests: list[SQNRTestWorker]
  add_test(name: str, sweep: str, fixed: List[int], fractional: List[int]) -> None
  run() -> None
  results() -> List[Dict[str, Any]]
}

class SQNRTestWorker{
  sweep: str
  fixed: List[int]
  fractional: List[int]
  reference: Component
  run() -> None
  results() -> dict[str, Any]
}

class SQNR_FFE{
}

class SQNR_DFFE{
}

class SQNR_MLSE {
}

class SQNR_LMS {
}

BerTestDirector "1"*-->"*" System
BerTestDirector "1"*-->"*" BerTest
BerTest "1"*->"*" BerTestSingle
BerCalibration "1"*--> BerTestDirector
BerDepth "1"*--> BerTestDirector
BerQuantization "1"*--> BerTestDirector
BerSBVD "1"*--> BerTestDirector

SQNRTestDirector "1"*-->"*" System
SQNRTestDirector "*"<--*"1" SQNRTestWorker
SQNR_FFE "1"*--> SQNRTestDirector
SQNR_DFFE "1"*--> SQNRTestDirector
SQNR_MLSE "1"*--> SQNRTestDirector
SQNR_LMS "1"*--> SQNRTestDirector

ComponentBase <|-- Channel
ComponentBase <|-- AGC
ComponentBase <|-- FFE
ComponentBase <|-- DFFE
System "*"*--> ComponentBase
Wave -|> ComponentBase
SBVD <- DFFE

@enduml
```
