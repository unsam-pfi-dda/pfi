"""SQNR test for LMS adaptation.

This test will measure SQNR for the LMS adaptation algorithm between a fixed point System against
a floating point System

How to run:
    python -m src.sims sqnr lms

The results will be sent to the Plot module (./src/Plot) for storing and plotting with plotly.
"""

import numpy as np

from src import Quantizer, System

from .core import SQNRTestDirector


def sqnr_lms():
    system = System.from_yaml("sqnr_lms.yaml")
    tests = SQNRTestDirector(
        adapt=10_000,
        measure=1000,
        system=system,
        reference=system.quantizer,
    )

    tests.add_test(
        name="Error",
        quantize={
            "error": "s0/0-z/s",
        },
        sweep="error",
        integer=[0],
        fractional=range(1, 18),
    )

    def lms_measure_function(
        dut: Quantizer,
        ref: Quantizer,
    ) -> tuple[np.float64, np.float64]:
        """Custom LMS measure function for SQNR calculation."""
        return (np.float64(dut.error), np.float64(ref.error))

    # Replace measure function
    for test in tests.tests:
        test._measure = lms_measure_function

    return tests.run()
