"""Measure system performance with profiling stats.

Usage:
    Just run the script and check the .svg diagram created in ./prof folder

    $ python3.10 models/profiling.py
    (...)/prof/models.svg


"""

import cProfile
import subprocess
from pathlib import Path

from src import System


def main():
    """Create and run the system to profile."""
    with cProfile.Profile() as profiler:
        system = System.from_yaml("profiling.yaml")
        for _ in range(abs(system.lag)):
            system.run()

    folder = Path(__file__).parent.parent / "prof"
    folder.mkdir(exist_ok=True, parents=True)
    pstats = folder / "models.pstats"
    profiler.dump_stats(pstats)

    gprof2dot = subprocess.run(
        args=[
            "gprof2dot",
            "-fpstats",
            pstats,
        ],
        check=True,
        capture_output=True,
    )
    subprocess.run(
        args=[
            "dot",
            "-Tsvg",
            f"-o{pstats.with_suffix('.svg')}",
        ],
        check=True,
        input=gprof2dot.stdout,
    )

    print(f"{pstats.with_suffix('.svg')}")


if __name__ == "__main__":
    main()
