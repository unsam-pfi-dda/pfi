"""Unit testing for Sliding Block Viterbi Decoder component."""

import hypothesis
import numpy as np
import pytest
from scipy import signal

from src.mlse import Viterbi
from src.sbvd import SBVD
from src.slicer import BPSK
from tests.utils import non_duobinary_channel

QUANTIZATION = {
    "metrics": "s9/4-z/s",
    "cost": "s9/4-z/s",
}


def test_no_depth():
    """Verify that SBVD with depth=0 behaves just like a slicer."""
    sbvd = SBVD(depth=0, taps=2)
    samples = np.random.normal(loc=0, size=abs(sbvd.clocks) + 10 * sbvd.depth)
    results = [sbvd.run(sample, [1, 0, 0]) for sample in samples]
    golden = [BPSK(sample) for sample in samples]

    np.testing.assert_array_equal(golden, results[-sbvd.clocks :])


@hypothesis.given(
    channel=hypothesis.strategies.lists(
        hypothesis.strategies.floats(min_value=1, max_value=100),
        min_size=3,
        max_size=3,
    ),
    depth=hypothesis.strategies.integers(min_value=2, max_value=20),
    # no need to test depth=0 (slicer), or depth=1 (known for missed values)
)
@hypothesis.settings(deadline=None)
@pytest.mark.parametrize("quantize", ({}, QUANTIZATION), ids=("floating", "fixed"))
def test_run_stream(channel: list[int], depth: int, quantize: dict[str, str]):
    """Compare SBVD output values against golden sequence."""
    sbvd = SBVD(depth=depth, taps=len(channel) - 1, quantize=quantize)
    sequence = np.random.choice(sbvd.symbols, size=abs(sbvd.lag) + 10 * sbvd.depth)
    samples = np.convolve(sequence, channel, mode="valid")  # type: ignore
    golden = list(sequence[len(channel) - 1 : sbvd.lag])

    results = [sbvd.run(sample, channel) for sample in samples]

    # First DEPTH iterations are garbage.
    np.testing.assert_array_equal(golden, results[-sbvd.lag :])

    # Validate lag
    correlation = signal.correlate(golden, results)
    correlation_lags = signal.correlation_lags(len(golden), len(results))
    assert sbvd.lag == correlation_lags[np.argmax(correlation)]



@hypothesis.given(
    channel=hypothesis.strategies.lists(
        hypothesis.strategies.floats(min_value=0, max_value=1, width=16),
        min_size=3,
        max_size=3,
    ).filter(lambda x: x[0] == 1 and non_duobinary_channel(x)),
    # Super tiny floats also breaks the code, as are not filtered by non_duobinaries.
    # Thus limiting the float width to 16 bits only.
    depth=hypothesis.strategies.integers(min_value=2, max_value=10),
)
@hypothesis.settings(
    deadline=None,
    suppress_health_check=[hypothesis.HealthCheck.filter_too_much],
)
def test_run_viterbi_compare(channel: list[float], depth: int):
    """Compare SBVD against Viterbi for non-duo binary channels."""
    taps = len(channel) - 1
    sbvd = SBVD(depth, taps)
    viterbi = Viterbi(depth, taps)

    sequence = np.random.choice(
        sbvd.symbols,
        size=abs(sbvd.clocks + viterbi.lag) + 2 * sbvd.depth,
    )
    samples = np.convolve(sequence, channel, mode="valid")

    golden = [viterbi.run(sample, np.array(channel[1:])) for sample in samples]
    results = [sbvd.run(sample, channel) for sample in samples]

    # Align buffers
    golden = golden[-viterbi.lag :]
    results = results[-sbvd.lag :]

    # Validate golden against sequence
    # The Viterbi golden model does not allow negative coefficients =(
    #   breaks for [1, 0.5, -0.5]
    np.testing.assert_array_equal(*zip(*zip(golden, sequence[taps:])))
    # Validate results against sequence
    # This one does work fine for [1, 0.5, -0.5], but testcase was excluded
    #   for obvious reasons.
    np.testing.assert_array_equal(*zip(*zip(results, sequence[taps:])))
    # Validate golden against results (redundant)
    np.testing.assert_array_equal(*zip(*zip(golden, results)))
