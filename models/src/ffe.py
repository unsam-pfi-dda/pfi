import numpy as np

from .component import Component


class FFE(Component):
    """Feedforward equalizer component."""

    step: float  # Adaptive filtering step

    def __init__(self, step: float = 1e-3, **kwargs) -> None:
        self._set_locals(locals())
        super().__init__(**kwargs)

        self.step = step

    def adapt(self, error: np.float64) -> None:
        """Update coefficients using normalized least mean squares (NLMS) algorithm."""
        # Skip if adaptation is not enabled
        if not self.adaptive:
            return

        # Grab the newest samples from the input buffer but with a certain offset that might
        # have against the error signal (this should be usually a delay)
        samples = (
            self.input.values[-self.taps :]
            if not self.adapt_offset
            else self.input.values[-self.taps - self.adapt_offset : -self.adapt_offset]
        )

        # Need to invert the input buffer because the samples are reversed, so that the latest
        # sample is multiplied with the first coefficient.
        # reverse: s3, s2, s1   so that  s1 * c1 ;  s2 * c2 ... etc.
        self.coef += self.step * error * samples[::-1]

    @property
    def lag(self) -> int:
        return -(self.taps // 2)
