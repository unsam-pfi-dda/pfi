"""Pseudo Random Binary Sequence module testbench"""

import sys
from pathlib import Path
import itertools
import cocotb
from cocotb.triggers import FallingEdge, RisingEdge, ClockCycles
from cocotb.clock import Clock
from cocotb.handle import HierarchyObject
from cocotb_test.simulator import Simulator
import pytest

sys.path.append(str(Path(__file__).parent.parent.parent.resolve()))
from models.src import Generator


@pytest.mark.parametrize("seed", ("1", "256", "511"))
def test_prbs9(simulator: Simulator, seed: str):
    """Run parametrized CoCoTB test with pytest"""
    simulator.parameters["PRBS_SEED"] = seed
    simulator.run()


@cocotb.test()
async def model_cosimulation(dut: HierarchyObject):
    """Cosimulate RTL and model and compare results"""
    await startup(dut)

    assert dut.register.value.integer == dut.PRBS_SEED.value

    SEQUENCE = 9
    limit = 2**SEQUENCE - 1
    lfsr = Generator.prbs(sequence=SEQUENCE, seed=dut.PRBS_SEED.value)

    results = []
    for _ in range(limit):
        await FallingEdge(dut.i_clock)
        results.append(dut.o_z.value.integer)

    golden = list(itertools.islice(lfsr, limit))
    assert golden == results, print(golden, results, sep="\n")


async def startup(dut: HierarchyObject):
    cocotb.start_soon(Clock(dut.i_clock, 1, units="ns").start())
    dut.i_enb.value = 1
    await RisingEdge(dut.i_clock)
    dut.i_rst_n.value = 0
    await ClockCycles(dut.i_clock, 1)
    dut.i_rst_n.value = 1
