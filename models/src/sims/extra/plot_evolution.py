from collections.abc import Iterator
from pathlib import Path
from statistics import linear_regression
from typing import Any

import numpy as np
import plotly.graph_objects as go
import yaml
from plotly.colors import DEFAULT_PLOTLY_COLORS
from plotly.subplots import make_subplots

if __name__ == "__main__":
    file = (
        Path(__file__).parent.parent.parent.parent
        / "resources/pfi-140_ber_quantize_dffe.yaml"
    )

    with file.open(encoding="utf-8") as file_handler:
        data = yaml.load(file_handler, Loader=yaml.Loader)  # type: ignore

    fig = make_subplots(
        rows=3,
        cols=1,
        subplot_titles=[
            "BER evolution",
            "Standard deviation",
            "Standardeviation evolution",
        ],
        shared_xaxes=True,
    )

    fig.update_layout(title_text="Bit error rate worker analysis")
    fig.update_xaxes(title_text="Error count", row=1, col=1)
    fig.update_yaxes(title_text="Bit error rate", row=1, col=1)
    fig.update_yaxes(title_text="Average value", row=2, col=1)
    fig.update_yaxes(title_text="Standard deviation", row=3, col=1)
    fig.update_layout(
        xaxis_showticklabels=True,
        xaxis2_showticklabels=True,
        xaxis3_showticklabels=True,
    )

    color = 0
    for test in data["data"]:
        snr_list = [
            int(_snr) for iteration in test["results"] for _snr, *_ in iteration
        ]
        for snr, ber_evolution in zip(snr_list, test["_ber"]):
            color += 1
            fig.add_trace(
                go.Scatter(
                    name=f"BER: {test['group']}[{snr}]",
                    x=list(range(len(ber_evolution))),
                    y=ber_evolution,
                    mode="lines+markers",
                    line={"width": 0.3, "dash": "dot"},
                    marker_color=DEFAULT_PLOTLY_COLORS[color % 10],
                    legendgroup=f"{test['group']}[{snr}]",
                    legendgrouptitle_text=f"{test['group']}[{snr}]",
                ),
                row=1,
                col=1,
            )

            def list_limiter(data: list[Any]) -> Iterator[list[Any]]:
                for iteration in range(1, len(data) + 1):
                    yield data[:iteration]

            std = [np.std(_value) for _value in list_limiter(ber_evolution)]

            value = [
                linear_regression(range(len(_value)), _value).slope
                for _value in list_limiter(std)
                if len(_value) > 2
            ]
            fig.add_trace(
                go.Scatter(
                    name=f"avg: {test['group']}[{snr}]",
                    x=list(range(len(ber_evolution))),
                    y=value,
                    mode="lines+markers",
                    line={"width": 0.3, "dash": "dot"},
                    marker_color=DEFAULT_PLOTLY_COLORS[color % 10],
                    legendgroup=f"{test['group']}[{snr}]",
                    legendgrouptitle_text=f"{test['group']}[{snr}]",
                ),
                row=2,
                col=1,
            )

            value = [
                linear_regression(range(len(_value)), _value).slope
                for _value in list_limiter(value)
                if len(_value) > 2
            ]
            fig.add_trace(
                go.Scatter(
                    name=f"std: {test['group']}[{snr}]",
                    x=list(range(len(ber_evolution))),
                    y=value,
                    mode="lines+markers",
                    line={"width": 0.3, "dash": "dot"},
                    marker_color=DEFAULT_PLOTLY_COLORS[color % 10],
                    legendgroup=f"{test['group']}[{snr}]",
                    legendgrouptitle_text=f"{test['group']}[{snr}]",
                ),
                row=3,
                col=1,
            )

    fig.update_layout(height=1000)
    fig.show()
