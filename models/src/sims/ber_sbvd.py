"""RTL emulation BER test.

This test will measure the Bit Error Rate for the RTL implementation for the SBVD.

Note:
    All tests will be executed in a new process call and it, probably, will use
    100% CPU.

How to run:
    python -m src.sims ber sbvd

The results will be sent to the Plot module (./src/Plot) for storing and plotting
with plotly.
"""
from __future__ import annotations

from functools import partial
from typing import TYPE_CHECKING, TypeVar

from src import Channel, System

from .core import BerTestDirector

if TYPE_CHECKING:
    from collections.abc import Callable

T = TypeVar("T")


def ber_sbvd():
    tests = BerTestDirector()
    sweep = "snr"
    reference = Channel
    adapt_samples = 1_000

    def slicer(cfg: T, modifier: Callable[[T], T] = lambda x: x) -> T:
        """Make an equivalent system but with just a slicer."""
        cfg = modifier(cfg)
        if "FFE" in cfg["Components"]:
            del cfg["Components"]["FFE"]
        if "DFFE" in cfg["Components"]:
            del cfg["Components"]["DFFE"]
        if "DFE" in cfg["Components"]:
            del cfg["Components"]["DFE"]
        return cfg

    def channel_100(cfg: T) -> T:
        cfg["Components"]["Channel"]["taps"] = [0, 0, 1, 0, 0]
        cfg["Components"]["DFFE"]["taps"] = [0, 0]
        cfg["Components"]["DFFE"]["mlse_main_tap"] = 1
        return cfg

    sys = System.from_yaml("rtl.yaml", channel_100)
    ref = System.from_yaml("rtl.yaml", partial(slicer, modifier=channel_100))
    tests.add_test(
        f"SBVA L={sys.feedback.mlse[0].depth}, Channel=[1, 0, 0]",
        sys,
        sweep,
        range(4, 12),
        reference,
        adapt_samples,
        "circle",
    )
    tests.add_test(
        "Slicer [1, 0, 0]",
        ref,
        sweep,
        range(4, 12),
        reference,
        adapt_samples,
        "x",
    )

    def channel_421(cfg: T) -> T:
        cfg["Components"]["Channel"]["taps"] = [0, 0, 4, 2, 1]
        cfg["Components"]["DFFE"]["taps"] = [0.43643578, 0.21821789]
        cfg["Components"]["DFFE"]["mlse_main_tap"] = 0.87287156
        return cfg

    sys = System.from_yaml("rtl.yaml", channel_421)
    ref = System.from_yaml("rtl.yaml", partial(slicer, modifier=channel_421))
    tests.add_test(
        f"SBVA L={sys.feedback.mlse[0].depth}, Channel=[4, 2, 1]",
        sys,
        sweep,
        range(4, 13),
        reference,
        adapt_samples,
        "circle",
    )
    tests.add_test(
        "Slicer [4, 2, 1]",
        ref,
        sweep,
        range(4, 13),
        reference,
        adapt_samples,
        "x",
    )

    def channel_221(cfg: T) -> T:
        cfg["Components"]["Channel"]["taps"] = [0.0, 0.0, 2.0, 2.0, 1.0]
        cfg["Components"]["DFFE"]["taps"] = [0.666, 0.333]
        return cfg

    sys = System.from_yaml("rtl.yaml", channel_221)
    ref = System.from_yaml("rtl.yaml", partial(slicer, modifier=channel_221))

    tests.add_test(
        f"SBVA L={sys.feedback.mlse[0].depth}, Channel=[2, 2, 1]",
        sys,
        sweep,
        range(4, 13),
        reference,
        adapt_samples,
        "circle",
    )
    tests.add_test(
        "Slicer [2, 2, 1]",
        ref,
        sweep,
        range(4, 13),
        reference,
        adapt_samples,
        "x",
    )

    return tests.run()
