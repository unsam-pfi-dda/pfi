// PRBS-9 parallel-by-4 module
//
// Transformation matrix
//
//          0  1  2  3  4  5  6  7  8
//
//        | 0  1  0  0  0  1  0  0  0 |
//        | 0  0  1  0  0  0  1  0  0 |
//        | 0  0  0  1  0  0  0  1  0 |
//        | 0  0  0  0  1  0  0  0  1 |
// M**4 = | 1  0  0  0  0  0  0  0  0 |
//        | 0  1  0  0  0  0  0  0  0 |
//        | 0  0  1  0  0  0  0  0  0 |
//        | 0  0  0  1  0  0  0  0  0 |
//        | 0  0  0  0  1  0  0  0  0 |

`include "defines.v"

module prbs9_parall4 #(
  parameter PRBS_SEED = `PRBS_SEED
)(
  input  wire       i_clock  ,
  input  wire       i_rst_n  ,
  input  wire       i_enb    ,
  output wire [3:0] o_z
);

reg [8:0] register;


always @(posedge i_clock or negedge i_rst_n) begin
  if (!i_rst_n) begin
    register <= PRBS_SEED;
  end
  else begin
    if (i_enb) begin
      register[0] <= register[1] ^ register[5];
      register[1] <= register[2] ^ register[6];
      register[2] <= register[3] ^ register[7];
      register[3] <= register[4] ^ register[8];
      register[4] <= register[0];
      register[5] <= register[1];
      register[6] <= register[2];
      register[7] <= register[3];
      register[8] <= register[4];
    end
    else begin
      register <= register;
    end
  end
end

assign o_z = {register[5], register[6], register[7], register[8]};

endmodule
