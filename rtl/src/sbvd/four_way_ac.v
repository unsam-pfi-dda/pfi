`include "defines.v"

module four_way_ac #(
  parameter SM_WIDTH = `SM_WIDTH
) (

  input [SM_WIDTH-1:0] i_f_sm_00       ,  // I Forward state metric 00.
  input [SM_WIDTH-1:0] i_f_sm_01       ,  // I Forward state metric 01.
  input [SM_WIDTH-1:0] i_f_sm_10       ,  // I Forward state metric 10.
  input [SM_WIDTH-1:0] i_f_sm_11       ,  // I Forward state metric 11.
  input [SM_WIDTH-1:0] i_b_sm_00       ,  // I Backward state metric 00.
  input [SM_WIDTH-1:0] i_b_sm_01       ,  // I Backward state metric 01.
  input [SM_WIDTH-1:0] i_b_sm_10       ,  // I Backward state metric 10.
  input [SM_WIDTH-1:0] i_b_sm_11       ,  // I Backward state metric 11.

  output         [         1:0] o_state_estimate   // O State metric 00.
);

wire [SM_WIDTH-1:0] sm_00_01 ;
wire                         dec_00_01;
wire [SM_WIDTH-1:0] sm_10_11 ;
wire                         dec_10_11;


two_way_acs #(
  .SM_WIDTH(SM_WIDTH),
  .BM_WIDTH(SM_WIDTH)
) u_two_way_acs_forward (
  .i_sm_0        (i_f_sm_00),
  .i_sm_1        (i_f_sm_01),
  .i_bm_0        (i_b_sm_00),
  .i_bm_1        (i_b_sm_01),
  .o_state_metric(sm_00_01 ),
  .o_decision    (dec_00_01)
);

two_way_acs #(
  .SM_WIDTH(SM_WIDTH),
  .BM_WIDTH(SM_WIDTH)
) u_two_way_acs_backward (
  .i_sm_0        (i_f_sm_10),
  .i_sm_1        (i_f_sm_11),
  .i_bm_0        (i_b_sm_10),
  .i_bm_1        (i_b_sm_11),
  .o_state_metric(sm_10_11 ),
  .o_decision    (dec_10_11)
);


assign o_state_estimate = (sm_00_01 <= sm_10_11) ? (dec_00_01 ? 2'b01 : 2'b00) : (dec_10_11 ? 2'b11 : 2'b10);


endmodule
