"""Model unit testing utilities."""

def non_duobinary_channel(channel: list[float]):
    """Return true if the channel is non symmetric duo-binary.

    Examples:
        >>> non_duobinary_channel([1, 1, 0])
        False

        >>> non_duobinary_channel([1, 1, 1])
        False

        >>> non_duobinary_channel([1, 0, 1])
        False

        >>> non_duobinary_channel([1, 1, 0.5])
        False

        >>> non_duobinary_channel([1, 0, 0])
        True

        >>> non_duobinary_channel([0, 0.5, 1])
        True
    """
    if channel.count(max(channel)) > 1:
        # Blocks [1, 1, x], [x, 1, 1], [1, x, 1] and [1, 1, 1] channels.
        return False
    if channel.count(max(channel)) <= 1:
        # Excludes [1, x, x], [x, 1, x] or [x, x, 1] channels.
        return True
    return False
