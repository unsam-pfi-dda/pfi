import numpy as np
from numfi import numfi

from .component import Component
from .fixed_arithmetic.utils import to_fixed
from .slicer import BPSK
from .wave import Wave


class DFE(Component):
    """Decision feedback equalizer system."""

    def __init__(self, taps: list[float] | int, step: float = 1e-3, **kwargs) -> None:
        self._set_locals(locals())
        super().__init__(taps=taps, **kwargs)

        if isinstance(taps, int):
            self.coef *= 0
        else:
            self.coef = np.array(list(map(float, taps)))

        self.step = step  # Adaptive filtering step
        self.slicer = BPSK

        # Create the decision buffer, this has to have an extra slot due to the
        # adaptation offset
        self.decision = Wave(self.taps + 1)

    def _calculate(self) -> np.float64:
        """Calculate the response for a DFE algorithm."""
        partials = self.decision.values[-self.taps :]
        # Quantize the coefficient values from the full adaptive resolution
        coefficients = (
            self.coef
            if "coef" not in self.quantize
            else numfi(self.coef, **to_fixed(self.quantize["coef"]))
        )

        # Since the buffer is reversed, last decision is stored in the last slot. By doing
        # a simple slice the samples will be reversed [n-2, n-1, n]. But coefficient vector
        # is ordered [c0, c1, c2], then samples need to be reversed, so everything is
        # well aligned
        result = self.input.last - (partials[::-1] @ coefficients)

        # Save this decision for next sample
        self.decision.add(self.slicer(result))

        return np.float64(result)

    def adapt(self, error: np.float64) -> None:
        """Update coefficients using normalized least mean squares (NLMS) algorithm."""
        # Skip if adaptation is not enabled
        if not self.adaptive:
            return

        # Has an extra delay because decision buffer has already been rolled with the current new
        # decision, but the coefficients needs to be calculated with # the current samples
        # (previous decision than the actual).
        offset = self.adapt_offset + 1
        decisions = self.decision.values[-self.taps - offset : -offset]

        # Since the buffer is reversed, last decision is stored in the last slot. By doing
        # a simple slice the samples will be reversed [n-2, n-1, n]. But coefficient vector
        # is ordered [c0, c1, c2], then samples need to be reversed, so everything is
        # well aligned
        self.coef -= self.step * error * decisions[::-1]

    @property
    def lag(self) -> int:
        # Decision feedback equalizer does not provide any lag
        return 0
