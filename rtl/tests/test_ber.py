"""Bit Error Rate module testbench"""

import sys
from pathlib import Path
import itertools
import cocotb
from cocotb.triggers import FallingEdge, RisingEdge
from cocotb.clock import Clock
from cocotb.handle import HierarchyObject, Force, Release
from cocotb_test.simulator import Simulator

sys.path.append(str(Path(__file__).parent.parent.parent.resolve()))
from models.src import Generator


def test_ber(simulator: Simulator):
    """Run parametrized CoCoTB test with pytest"""
    simulator.run()


def start_clock(clk):
    cocotb.start_soon(Clock(clk, 1, units="ns").start())


async def startup(dut: HierarchyObject):
    """Startup sequence"""

    start_clock(dut.clk)

    await FallingEdge(dut.clk)
    dut.rst_n.value = 0
    dut.enable.value = 1
    dut.i_read.value = 0
    dut.i_transmitted.value = 0
    dut.i_received.value = 0

    dut.rst_n.value = 0
    await RisingEdge(dut.clk)
    dut.rst_n.value = 1
    await FallingEdge(dut.clk)


@cocotb.test()
async def overflow(dut: HierarchyObject):
    """Check error counter resets if checked counter overflows."""
    await startup(dut)

    # Test overflow two times, first when previous sample was not an error
    # and then when it was an error. Previous error must not be missed.
    for error in (0, 1):
        dut.state.value = 2

        dut.error_counter.value = 1
        dut.checked_counter.value = -1
        dut.is_error.value = error

        await FallingEdge(dut.clk)
        dut.is_error.value = 0
        assert dut.checked_counter.value == 0
        assert dut.error_counter.value == error

        await FallingEdge(dut.clk)
        assert dut.checked_counter.value == 1
        assert dut.error_counter.value == error


@cocotb.test()
async def no_calibration_error(dut: HierarchyObject):
    """Check for no errors while switching from calibration to ready state.

    The first clock after BER_CHECK_CORR changes to BER_READY might introduce
    an error because it could compare to a different `position_reg` value.
    """
    await startup(dut)

    dut.tx_addr_buff.value = dut.BER_CORR_DEPTH.value - 1
    dut.counter_corr.value = dut.BER_CORR_DEPTH.value - 2
    await RisingEdge(dut.clk)
    dut.is_error.value = Force(1)
    await RisingEdge(dut.clk)
    dut.is_error.value = Release()
    await FallingEdge(dut.clk)
    assert dut.state.value == 2
    assert dut.checked_counter.value == 0
    assert dut.error_counter.value == 0
    assert dut.is_error.value == 0
    await FallingEdge(dut.clk)
    assert dut.checked_counter.value == 1
    assert dut.error_counter.value == 0
    assert dut.is_error.value == 0


@cocotb.test()
async def check_correlation(dut: HierarchyObject):
    """Check correlation sequence"""
    await startup(dut)

    # State = BER_CHECK_CORR for each tx_buffer (2**NB_BER_TXADDR-1)
    prbs = Generator.prbs(sequence=9)

    # Iterate through all TX_ADDR_BUFF
    for buffer in range(dut.BER_CORR_DEPTH.value):
        assert dut.tx_addr_buff.value == buffer
        # Do a full corr cycle for this buffer
        for counter in range(dut.BER_CORR_DEPTH.value):
            assert dut.state.value == 1
            assert dut.counter_corr.value == counter
            await FallingEdge(dut.clk)
            dut.i_received.value = dut.i_transmitted.value
            sample = next(prbs)
            dut.i_transmitted.value = int(sample)

    assert dut.state.value == 2
    # Since lag was forced to 1, then position_reg must be 1.
    assert dut.position_reg.value == 1


@cocotb.test()
async def matching_bitstream(dut: HierarchyObject):
    """Test for a zero-error bitstream"""
    await startup(dut)

    # Force position_reg to zero, since this test will be done without lag
    # Clear internal registers, otherwise will flag a compare error
    dut.position_reg.value = 0
    dut.state.value = 2

    prbs = Generator.prbs(sequence=9)
    bitstream = list(itertools.islice(prbs, 10_000))

    for symbol in bitstream:
        dut.i_transmitted.value = int(symbol)
        dut.i_received.value = int(symbol)
        await FallingEdge(dut.clk)

    dut.i_read.value = 1
    await FallingEdge(dut.clk)
    assert dut.o_ready.value == 1
    dut.i_read.value = 0
    await FallingEdge(dut.clk)
    assert dut.o_ready.value == 0
    await RisingEdge(dut.clk)

    errors: int = dut.o_errors.value.integer
    samples: int = dut.o_counter.value.integer

    assert samples == len(bitstream)
    assert errors == 0


@cocotb.test()
async def non_matching_bitstream(dut: HierarchyObject):
    """Test for a zero-error bitstream"""
    await startup(dut)

    # Force position_reg to zero, since this test will be done without lag
    # Clear internal registers, otherwise will flag a compare error
    dut.state.setimmediatevalue(2)
    dut.position_reg.setimmediatevalue(0)
    dut.tx_buff.setimmediatevalue(0)
    dut.checked_counter.setimmediatevalue(0)
    dut.error_counter.setimmediatevalue(0)

    samples = 10_000
    prbs1 = list(itertools.islice(Generator.prbs(sequence=9, seed=1), samples))
    prbs2 = list(itertools.islice(Generator.prbs(sequence=9, seed=-1), samples))
    errors = [a != b for a, b in zip(prbs1, prbs2)]

    error_counter = 0
    for transmitted, received, error in zip(prbs1, prbs2, errors):
        dut.i_transmitted.value = int(transmitted)
        dut.i_received.value = int(received)
        await FallingEdge(dut.clk)
        assert dut.error_counter.value == error_counter
        assert dut.is_error.value == error
        error_counter += error

    dut.i_transmitted.value = 0
    dut.i_received.value = 0
    await FallingEdge(dut.clk)

    dut.i_read.value = 1
    await FallingEdge(dut.clk)
    assert dut.o_ready.value == 1
    dut.i_read.value = 0
    await FallingEdge(dut.clk)
    assert dut.o_ready.value == 0

    assert dut.o_errors.value.integer == sum(errors)
    # Extra clock because errors have 1 clock delay.
    assert dut.o_counter.value.integer == samples+1
