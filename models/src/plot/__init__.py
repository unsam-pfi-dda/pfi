"""Module for plotting test results, either for 'ber' or 'quantization'.

To plot a result, call the `plot` module from a command line:

    python -m src.plot <path_to_result_file>

It will automatically detect which type of graph should be made, depending on
the `type` argument in the yaml file.
"""

from .plot import Plot

__all__ = ["Plot"]
