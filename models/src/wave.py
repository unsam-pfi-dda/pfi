from typing import Any

import numpy as np
import scipy.fft
import scipy.fftpack
from numfi import numfi
from numpy.typing import NDArray


class Wave:
    """Class for storing signals and utils."""

    _values: NDArray[np.float64]

    def __init__(
        self,
        length: int = 256,
        val: None | NDArray[np.float64] = None,
        **kwargs: dict[str, Any],
    ) -> None:
        self._values = np.zeros(length, dtype=np.float64)

        if val is not None:
            self.set(val)
        else:
            if kwargs.get("quantize"):
                self._values = numfi(self._values, **kwargs.get("quantize"), fixed=True)  # type: ignore

    def add(self, value: np.float64) -> None:
        """Cycle buffer by adding a new sample.
        The newest element is added in the last position of the buffer (rolls left).
        """
        # This is a more efficient approach to np.roll()
        #   see details: https://github.com/numpy/numpy/issues/10848
        # Using `out` parameters keeps the target object type (ndarray or numfi)
        np.concatenate(
            [self._values[1:], np.array([value]).ravel()],
            out=self._values,
            casting="unsafe",
        )

    def set(self, new: NDArray[np.float64]):  # noqa: A003
        """Overwrite the entire buffer with new values.

        The entire buffer length must be fed and will be converted to a
        float64 numpy array.
        """
        if new.__len__() != self._values.__len__():
            msg = (
                "can't set new values because buffer sizes does not match"
                f"expected {self._values.size}, got {new.size}",
            )
            raise ValueError(msg)

        self._values = new

    def value(self, lag: int = 0) -> np.float64:
        """Return a specific value with lag. No lag means last value."""
        if lag > 0:
            lag = -lag
        return self._values[lag - 1]

    @property
    def values(self) -> NDArray[np.float64]:
        """Returns the buffer contents value."""
        return self._values

    @property
    def size(self) -> int:
        """Data length."""
        return self.__len__()

    @property
    def last(self) -> np.float64:
        """Returns the last value."""
        return self.value()

    @property
    def x(self) -> NDArray[np.int64]:
        """Returns an evenly distributed linspace generator for easier plotting."""
        return np.arange(self.__len__(), dtype=np.int64)

    def fft(
        self,
        n: int | None = None,
    ) -> tuple[NDArray[np.float64], NDArray[np.float64]]:
        """Calculate frecuency spectrum.

        Returns
            A tuple of arrays with the first element containing the frecuencies while
            the second contains the coefficient values.

        """
        fft = np.abs(scipy.fft.fft(self._values, n=n))
        freq = scipy.fftpack.fftfreq(self.size)
        return (freq, fft)

    @property
    def energy(self):
        """Signal energy calculated as sum of squared values."""
        return self._values @ self._values

    @property
    def power(self) -> np.float64:
        """Signal average power calculated as sum of squared values."""
        return np.float64(self.energy / self.size)

    def __add__(self, other):
        if not isinstance(other, Wave):
            raise ValueError(f"other must be {self.__class__.__name__} class")

        # TODO: Assumes that both arrays have the same size
        _sum = self._values + other._values
        _length = len(_sum)
        return Wave(length=_length, val=_sum)

    def __getitem__(self, slice):
        _values = self._values.__getitem__(slice)
        _length = _values.__len__()
        return Wave(length=_length, val=_values)

    def __len__(self) -> int:
        return self._values.size

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}(length={self.__len__()}, val={self._values})"


if __name__ == "__main__":
    # Create a new wave buffer with a size of 8
    a = Wave(length=8)
    # And add some random values
    a.add(5)
    a.add(4)
    a.add(3)
    a.add(2)
    print(a.values)  # Test if values were added and cycled correctly
    # Overwrite buffer with new data (input array must match buffer size)
    a.set([0, 7, 9, 4, 2, 2, 1, 3])
    print(a.values)  # Test if values were overwritten correctly
    # Calculate FFT for the current buffer
    print(f"{a.fft()=}")

    print(f"{a=}")  # Test __repr__ method
    print(f"{len(a)=}")  # Test __len__ method
    print(f"{a[3:5]=}")  # Test __getitem__ method
    print(f"{a+a=}")  # Test __add__ method
