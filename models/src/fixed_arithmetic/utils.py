"""Utils and function helpers to extend numfi package."""

import re


def to_string(data: dict[str, int | str]) -> str:
    """Converts numfi kwargs into fixed point string format.

    Example:
        >>> to_string({'s': 1, 'w': 9, 'f': 7, 'rounding': 'zero', 'overflow': 'saturate'})
        's9/7-z/s'
    """
    signed = "s" if data["s"] else "u"
    return f"{signed}{data['w']}/{data['f']}-{data['rounding'][0]}/{data['overflow'][0]}"  # type: ignore


def to_fixed(text: None | str) -> dict[str, int | str]:
    """Convert fixed-point numfi parameters from string into kwargs.

    Example:
        >>> to_fixed('s9/7-z/s')
        {'s': 1, 'w': 9, 'f': 7, 'rounding': 'zero', 'overflow': 'saturate'}
    """
    if not text:
        return {}
    regex = (
        r"^(?P<s>[su])(?P<w>\d+)/(?P<f>\d+)-(?P<rounding>[rfzc])/(?P<overflow>[sw])$"
    )
    if not (match := re.match(regex, text)):
        raise ValueError("invalid format for fixed string")

    _groups: dict[str, str | int] = match.groupdict()  # type:ignore
    _translation = {
        "s": {"s": 1, "u": 0},
        "rounding": {"r": "round", "f": "floor", "z": "zero", "c": "ceil"},
        "overflow": {"w": "wrap", "s": "saturate"},
    }
    for key, value in _translation.items():
        _groups[key] = value[_groups[key]]  # type: ignore

    _groups["w"] = int(_groups["w"])
    _groups["f"] = int(_groups["f"])

    return _groups
