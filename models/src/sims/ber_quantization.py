"""BER Quantization test.

This test will measure the Bit Error Rate for the system with and without
quantization (DFFE-VA) against the reference (DFE).

Note:
    All tests will be executed in a new process call and it, probably, will use
    100% CPU.

How to run:
    python -m src.sims ber quantization

The results will be sent to the Plot module (./src/Plot) for storing and plotting
with plotly.
"""

from typing import TypeVar

from src import Channel, System

from .core import BerTestDirector

T = TypeVar("T")


def ber_quantization():
    tests = BerTestDirector()
    sweep = "snr"
    sweep_range = range(4, 16)
    reference = Channel
    adapt_samples = 40_000

    def modifier_reference(cfg: T) -> T:
        """Modifier for System configuration file for testing the reference system (DFE)."""
        cfg["Components"] = {
            "DFE" if k == "DFFE" else k: v for k, v in cfg["Components"].items()
        }
        del cfg["Components"]["DFE"]["iterations"]
        del cfg["Components"]["DFE"]["mlse_iterations"]
        del cfg["Components"]["DFE"]["mlse_depth"]
        del cfg["Components"]["DFE"]["mlse_taps"]
        for _component in cfg["Components"].values():
            if "quantize" in _component:
                del _component["quantize"]
        return cfg

    system_reference = System.from_yaml("quantization.yaml", modifier_reference)
    tests.add_test(
        "Reference",
        system_reference,
        sweep,
        sweep_range,
        reference,
        adapt_samples,
        "star",
    )

    # Test: No quantization
    def test_floating(cfg: T) -> T:
        """Modifier for System configuration file for testing a floating system (DFFE)."""
        for _component in cfg["Components"].values():
            if "quantize" in _component:
                del _component["quantize"]
        return cfg

    system_floating = System.from_yaml("quantization.yaml", test_floating)
    tests.add_test(
        "Floating",
        system_floating,
        sweep,
        sweep_range,
        reference,
        adapt_samples,
        "circle",
    )

    # Test: with quantization
    system_fixed = System.from_yaml("quantization.yaml")
    tests.add_test(
        "Fixed",
        system_fixed,
        sweep,
        sweep_range,
        reference,
        adapt_samples,
        "x",
    )

    return tests.run()


if __name__ == "__main__":
    pass
