from collections.abc import Callable
from typing import Any, Protocol

import numpy as np
from numfi import numfi
from numpy.typing import NDArray

from .component import Component
from .fixed_arithmetic.utils import to_fixed
from .sbvd import SBVD
from .slicer import BPSK
from .wave import Wave


class MLSE(Protocol):
    """Protocol class for MLSE algorithm."""

    depth: int
    taps: int
    symbols: tuple[np.float64, ...]
    quantize: dict[str, str]

    def __init__(
        self,
        depth: int,
        taps: int,
        symbols: tuple[np.float64, ...] = (),
        quantize: dict[str, str] | None = None,
    ) -> None:
        ...

    def run(self, sample: np.float64, coef: NDArray[np.float64]) -> np.float64:
        ...

    @property
    def lag(self) -> int:
        ...


class DFFE(Component):
    """Decision Feed-forward Equalizer system."""

    step: float
    slicer: Callable[[np.float64], np.float64]
    mlse: list[MLSE]
    decisions: list[Wave]

    def __init__(
        self,
        taps: list[float] | int,
        iterations: int,
        step: float = 1e-3,
        mlse_depth: int = 0,
        mlse_taps: int = 2,
        mlse_iterations: int = 1,
        quantize: None | dict[str, str] = None,
        buffer_length: int = 0,
        mlse: type[MLSE] = SBVD,
        mlse_main_tap: float = 1.0,
        **kwargs: Any,
    ) -> None:
        self._set_locals(locals())

        self.step = step  # Adaptive filtering step
        self.slicer = BPSK
        self.mlse = [mlse(mlse_depth, mlse_taps, quantize=quantize or {}) for _ in range(mlse_iterations)]
        self.mlse_main_tap = mlse_main_tap

        # Need at least `lag` samples in the input buffer.
        if buffer_length < -self.lag:
            buffer_length = -self.lag + 1

        # By default decision buffers will have `taps` memory, but we need to add one for the
        # adaptation offset.
        if mlse_iterations > iterations:
            iterations = mlse_iterations
        if iterations <= 0:
            iterations = taps + 1

        super().__init__(
            taps=taps,
            quantize=quantize,
            buffer_length=buffer_length,
            **kwargs,
        )
        if isinstance(taps, int):
            self.coef *= 0
        else:
            self.coef = np.array(list(map(float, taps)))

        # Create decision buffer for the iterations and steps, this has to have an extra slot
        # due to the adaptation offset.
        # TODO: Need to consider offset as an input argument, otherwise adaptation
        #       offset will be out of bounds.
        self.iterations = iterations  # NOTE: this will create self.decisions buffer

    def _calculate(self) -> np.float64:
        """Calculate the response for a DFFE algorithm."""
        # Assumes that decisions and coefficients are aligned and are the correct
        # number of elements in it, e.g. the last should not have any coefficient, etc.
        # This iterator should be done in a reversed order so that first results are not used for
        # the following step.
        _result = []
        sample = self.input.value(self.lag)
        sample_mlse = self.input.last

        # Quantize the coefficient values from the full adaptive resolution
        coefficients = (
            self.coef
            if "coef" not in self.quantize
            else numfi(self.coef, **to_fixed(self.quantize["coef"]))
        )
        for _iteration, _decisions in sorted(enumerate(self.decisions), reverse=True):
            if _iteration < len(self.mlse):
                # When mlse_depth=0 it will behave like a slicer.
                mlse_coef = np.concatenate(
                    ([self.mlse_main_tap], self.coef[: self.mlse[_iteration].taps]),
                )
                result = self.mlse[_iteration].run(sample_mlse, mlse_coef)
            else:
                # Need to sum previous partial decisions and subtract them to the current sample
                partials = [
                    self.decisions[_iteration - (i + 1)].value(i)
                    if _iteration >= i
                    else 0.0
                    for i in range(self.taps)
                ]
                result = np.float64(sample - (coefficients @ partials))

            _result.append(result)

            # Feed the partial sample through the slicer and store it in decision buffer
            _decisions.add(self.slicer(result))

        return _result[0]

    def adapt(self, error: np.float64) -> None:
        """Update coefficients using normalized least mean squares (NLMS) algorithm."""
        # Skip if adaptation is not enabled
        if not self.adaptive:
            return

        # Has an extra delay because decision buffer has already been rolled with
        # the current new decision, but the coefficients needs to be calculated with
        # the current samples (previous decision than the actual)
        offset = self.adapt_offset + 1
        decisions = self.decisions[-1].values[-self.taps - offset : -offset]

        # Need to invert decisions slice because the buffer stores in reversed order, so
        # that the latest decision is multiplied with the first coefficient.
        # reverse: d3, d2, d1   so that  d1 * c1 ;  d2 * c2 ... etc.
        self.coef -= self.step * error * decisions[::-1]

    @property
    def lag(self) -> int:
        """Calculates the time-delay for DFFE."""
        if not self.mlse:
            return 0
        return -max(abs(mlse.lag) for mlse in self.mlse)

    @property
    def iterations(self) -> int:
        """Returns the number of DFFE iterations."""
        return len(self.decisions)

    @iterations.setter
    def iterations(self, new: int) -> None:
        """Updates the decision matrix for new iterations."""
        self.decisions = [Wave(self.taps + 1) for _ in range(new)]
