// ----------------------------------------------------------------------------
// FIR Channel defaults
// ----------------------------------------------------------------------------

// Coefficient number for channel
`define FIR_TAPS        3

// Number of integer bits for channel coefficient values
`define FIR_NB_I        1
// Number of fractional bits for channel coefficient values
`define FIR_NB_F        7

`define FIR_NB  `FIR_NB_I + `FIR_NB_F

`define NB_DOT_COEFF `FIR_NB_I + $clog2(`FIR_TAPS) + `FIR_NB_F

// ----------------------------------------------------------------------------
// System defaults
// ----------------------------------------------------------------------------

// System parallelization
`define PARALLELISM         1

// Coefficient word length
`define COEF_W              `FIR_NB

// Sample word length
`define SAMP_W              `NB_DOT_COEFF

// Adquisition word length
`define ADQ_W               `SAMP_W


// ----------------------------------------------------------------------------
// SBVD defaults
// ----------------------------------------------------------------------------

// Branch metric word length
`define BM_WIDTH            13

// State metric word length
`define SM_WIDTH            13


// ----------------------------------------------------------------------------
// PRBS defaults
// ----------------------------------------------------------------------------

// Number of registers for PRBS
`define NB_PRBS             9

// Seed, must be expressed in lenght=`NB_PRBS
`define PRBS_SEED           9'h1
// `define PRBS_SEED           `NB_PRBS'h1

// ----------------------------------------------------------------------------
// BER defaults
// ----------------------------------------------------------------------------

// Error & Counter acumulators bit length
`define NB_BER_ERROR        64

// Number of bits for correlation check buffer
`define NB_BER_CORR_DEPTH   `NB_PRBS

// Number of allowed states
`define NB_BER_STATE        2
`define BER_RST             {`NB_BER_STATE'd0}
`define BER_CHECK_CORR      {`NB_BER_STATE'd1}
`define BER_READY           {`NB_BER_STATE'd2}
