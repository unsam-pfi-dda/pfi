`include "defines.v"

module two_way_acs #(
  parameter SM_WIDTH = `SM_WIDTH,
  parameter BM_WIDTH = `BM_WIDTH
) (

  input [SM_WIDTH-1:0] i_sm_0,  // I State metric.
  input [SM_WIDTH-1:0] i_sm_1,  // I State metric.
  input [BM_WIDTH-1:0] i_bm_0,  // I Branch metric.
  input [BM_WIDTH-1:0] i_bm_1,  // I Branch metric.

  output [SM_WIDTH-1:0] o_state_metric       ,  // O Updated state metric.
  output                o_decision              // O Decision bit.
);

wire [SM_WIDTH:0] partial_add[1:0];

assign partial_add[0] = i_sm_0 + {{SM_WIDTH-BM_WIDTH{1'b0}}, i_bm_0};
assign partial_add[1] = i_sm_1 + {{SM_WIDTH-BM_WIDTH{1'b0}}, i_bm_1};

assign o_decision     = (partial_add[0] <= partial_add[1]) ? 1'b0 : 1'b1;
assign o_state_metric = o_decision ? partial_add[1] : partial_add[0];

endmodule
