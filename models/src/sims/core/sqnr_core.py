"""Class module for running SQRN analysis for an entire System object."""

from collections import defaultdict
from collections.abc import Callable, Sequence
from copy import deepcopy
from dataclasses import dataclass, field
from typing import Any

import numpy as np
from tqdm import tqdm

from src import Component, System, to_fixed, to_string


def measure_function(dut: Component, ref: Component) -> tuple[np.float64, np.float64]:
    """Default function to collect signals for SQNR in SQNRTest class."""
    return (np.float64(dut.output.last), np.float64(ref.output.last))


@dataclass(frozen=True)
class SQNRTestWorker:
    """Dataclass object for testing a Component with different integer/fractional
    point bit lengths against a reference Component.

    This class is not intended to be used, see SQNRTestTest for running quantization tests.
    """

    name: str
    quantize: dict[str, str]
    sweep: str
    integer: Sequence[int]
    fractional: Sequence[int]
    reference: Component
    symbol: str = "circle"

    _measure: Callable[[Component, Component], tuple[np.float64, np.float64]] = field(
        repr=False,
        default=measure_function,
    )
    _components: dict[int, dict[int, Component]] = field(
        init=False,
        repr=False,
        default_factory=lambda: defaultdict(dict),
    )
    _signal: dict[int, dict[int, np.float64]] = field(
        init=False,
        repr=False,
        default_factory=lambda: defaultdict(lambda: defaultdict(np.float64)),
    )
    _error: dict[int, dict[int, np.float64]] = field(
        init=False,
        repr=False,
        default_factory=lambda: defaultdict(lambda: defaultdict(np.float64)),
    )

    def __post_init__(self):
        """Create one component for each sweep parameter, both integer and fractional ranges."""

        for _integer in self.integer:
            for _fractional in self.fractional:
                _quantize = to_fixed(self.quantize[self.sweep])
                _quantize["w"] = _integer + _fractional + _quantize["s"]  # type: ignore
                _quantize["f"] = _fractional

                self.quantize[self.sweep] = to_string(_quantize)

                config = self.reference.locals | {"quantize": self.quantize}
                # Create a new class of the same type as reference, but with the custom config
                self._components[_integer][_fractional] = self.reference.__class__(
                    **deepcopy(config),
                )

    def run(self, measure: bool) -> None:
        """Run the system for each quantized component."""
        sample = self.reference.input.last
        for _integer, x in self._components.items():
            for _fractional, _component in x.items():
                _component.run(sample)

                # Quantize fractional point adaptive coefficients to only measure SQNR for
                # fixed coef and not fixed adaptation.
                _component.coef -= _component.coef  # Reset to zero.
                _component.coef += np.float64(self.reference.coef)

                if measure:
                    output, reference = self._measure(_component, self.reference)
                    self._signal[_integer][_fractional] += output**2
                    self._error[_integer][_fractional] += (reference - output) ** 2

    @property
    def sqnr(self) -> dict[int, dict[int, float]]:
        """Return the result of all SQNR values for each run."""
        sqnr: dict[int, dict[int, float]] = defaultdict(lambda: defaultdict(float))
        for _integer in self.integer:
            for _fractional in self.fractional:
                _signal = self._signal[_integer][_fractional]
                _error = self._error[_integer][_fractional]
                sqnr[_integer][_fractional] = float(10 * np.log10(_signal / _error))

        return sqnr

    def results(self) -> dict[str, Any]:
        """Generate a result dictionary ready for quantization graph."""
        return {
            "group": self.name,
            "symbol": self.symbol,
            "extra": {
                "reference": self.reference.__class__.__name__,
                "sweep": self.sweep,
                "quantize": self.quantize,
                "coef": self.reference.coef.astype(float).tolist(),
            },
            "results": [
                {"integer": integer, "data": dict(data)}
                for integer, data in self.sqnr.items()
            ],
        }


@dataclass
class SQNRTestDirector:
    """Class for running a SQNR measurement for Component quantization.

    There will be one test per quantization sweep, per integer bit length and per fractional
    bit length. Each test will copy only the reference Component from the reference system.

    A single bitstream will be generated and will be used for all tests. In fact, the test consists
    only to feed the value from reference Component's input, into each test Component.
    For example for a system as follows:
        Channel -> FFE -> DFFE
    There will be a single Component object for Channel and FFE, but several for DFFE.
    One DFFE will be used as the reference, then will be copied one for each test.
    Each test component input will copy the reference input, and only the reference component
    will adapt and copy the new coefficients to all tests. The output SQNR will be calculated
    between the test output value and the reference output value.

    This ensure that ALL tests evolves in the same way and only measures the quantization impact
    for the exact same environment.

    Attributes:
        adapt: number of iterations to let the system run (and adapt) until start measuring.
        measure: number of iterations to measure and capture the output SQNR
        system: A System object class with the entire system to test
        reference: A Component object that will be used as reference.
    """

    adapt: int
    measure: int
    system: System
    reference: Component

    tests: list[SQNRTestWorker] = field(init=False, default_factory=list)

    @staticmethod
    def clean_quantizations(
        cfg: dict[str, Any],
        exclusions: Sequence[str],
    ) -> dict[str, Any]:
        """System modifier handler to remove all quantizations except for FFE."""
        for component in set(cfg["Components"]).difference(exclusions):
            if "quantize" in cfg["Components"].get(component, {}):
                del cfg["Components"][component]["quantize"]
        return cfg

    def add_test(
        self,
        name: str,
        quantize: dict[str, str],
        sweep: str,
        integer: Sequence[int],
        fractional: Sequence[int],
    ):
        """Add new test case."""
        test = SQNRTestWorker(
            name=name,
            quantize=quantize,
            sweep=sweep,
            integer=integer,
            fractional=fractional,
            reference=self.reference,
        )
        self.tests.append(test)
        return test

    def run(self) -> list[dict[str, Any]]:
        """Runs the entire test system."""
        for _ in tqdm(range(self.adapt), desc="Initial adaptation", leave=True):
            self.system.run()

        # Fill output buffers for test elements
        for _ in range(len(self.reference.output)):
            self.system.run()
            for _test in self.tests:
                _test.run(measure=False)

        # Measure SQNR
        for _ in tqdm(range(self.measure), desc="Measure", leave=True):
            self.system.run()
            for _test in self.tests:
                _test.run(measure=True)

        return self.results()

    def results(self):
        """Compiles the result from all workers for Plot module."""
        return [test.results() for test in self.tests]
