"""Four-way AC module testbench"""

import sys
from pathlib import Path
import cocotb
import cocotb.regression
from cocotb.triggers import Timer
from cocotb.handle import HierarchyObject
from cocotb_test.simulator import Simulator
import numpy as np

sys.path.append(str(Path(__file__).parent.parent.parent.resolve()))

from models.src import SBVD
from rtl.tests.utils import binstr2list


def test_four_way_ac(simulator: Simulator):
    """Run parametrized CoCoTB test with pytest"""
    simulator.run()


async def model_cosimulation(
    dut: HierarchyObject, branch_metrics: list[float], state_metrics: list[float]
):
    """Cosimulate RTL and model and compare results"""
    drive_inputs(dut, state_metrics, branch_metrics)
    await Timer(1, units="ps")  # wait a bit

    results = get_output(dut)
    golden = SBVD.four_state_ac(state_metrics, branch_metrics)
    assert golden == results, print(golden, results, branch_metrics, state_metrics)


tf = cocotb.regression.TestFactory(model_cosimulation)
tf.add_option("branch_metrics", np.random.choice(range(10), size=(10, 4)).tolist())
tf.add_option("state_metrics", np.random.choice(range(10), size=(10, 4)).tolist())
tf.generate_tests()


def drive_inputs(dut, a, b):
    dut.i_f_sm_00.value = a[0]
    dut.i_f_sm_01.value = a[1]
    dut.i_f_sm_10.value = a[2]
    dut.i_f_sm_11.value = a[3]
    dut.i_b_sm_00.value = b[0]
    dut.i_b_sm_01.value = b[1]
    dut.i_b_sm_10.value = b[2]
    dut.i_b_sm_11.value = b[3]


def get_output(dut: HierarchyObject):
    return tuple(binstr2list(dut.o_state_estimate.value.binstr, bits=1))
