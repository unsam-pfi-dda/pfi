`include "defines.v"

module sbvd
  #(
    parameter COEF_W   = `COEF_W,
    parameter ADQ_W    = `ADQ_W,
    parameter SM_WIDTH = `SM_WIDTH,
    parameter SAMP_W   = `SAMP_W,
    parameter BM_WIDTH = `BM_WIDTH,

    parameter SYMBOLS = 2, // Number of possible symbols
    parameter STATES = 4,  // Possible states for Viterbi
    parameter COEFFICIENTS = 3,  // Number of coefficients ( cursor + postcursors )
    // TODO: Propagate this parameter into childs

    parameter D = 10,  // Viterbi processor iterations.
    parameter M = 2*D // Viterbi processor depth (forward + backward)
  ) (

    input clk,
    input rst_n,
    input enable,

    input [M*SAMP_W-1: 0] i_sample,  // {Zn, Zn+1, Zn+2, Zn+3 + ... + Zn+(M-1)}
    input [COEFFICIENTS*COEF_W-1: 0] i_coef,  // {cursor, post_1, post_2}

    output [M-1: 0] o_data  // {Xn, Xn+1, ..., Xn+(M-1)}
  );


wire [(M+1)*STATES*SM_WIDTH-1:0] forward_state_metric;
assign forward_state_metric[0+:STATES*SM_WIDTH] = 0;
wire [(M+1)*STATES*SM_WIDTH-1:0] backward_state_metric;
assign backward_state_metric[0+:STATES*SM_WIDTH] = 0;

wire [STATES*SYMBOLS*ADQ_W-1:0] branch_distance;

wire [        1:0] state_estimate                  ;
wire [        1:0] state_estimate_reg              ;
wire [(M+1)*2-1:0] forward_traceback_state_estimate;
assign forward_traceback_state_estimate[(M+1)*2-1-:2] = state_estimate_reg;
wire [(M+1)*2-1:0] backward_traceback_state_estimate;
assign backward_traceback_state_estimate[(M+1)*2-1-:2] = state_estimate_reg;

wire [M-1:0] forward_data ;
wire [M-1:0] backward_data;

conv #(
  .COEF_W(COEF_W),
  .ADQ_W (ADQ_W )
) u_conv (
  .i_cursor (i_coef[COEF_W*(COEFFICIENTS-1) +: COEF_W]), // cursor
  .i_postc1 (i_coef[COEF_W*(COEFFICIENTS-2) +: COEF_W]), // postcursor 1
  .i_postc2 (i_coef[COEF_W*(COEFFICIENTS-3) +: COEF_W]), // postcursor 2
  
  .o_adq_000(branch_distance[ADQ_W*(0) +: ADQ_W]      ), // O Ideal adquisition of state 00 -> 00.
  .o_adq_001(branch_distance[ADQ_W*(1) +: ADQ_W]      ), // O Ideal adquisition of state 00 -> 01.
  .o_adq_010(branch_distance[ADQ_W*(2) +: ADQ_W]      ), // O Ideal adquisition of state 01 -> 10.
  .o_adq_011(branch_distance[ADQ_W*(3) +: ADQ_W]      ), // O Ideal adquisition of state 01 -> 11.
  .o_adq_100(branch_distance[ADQ_W*(4) +: ADQ_W]      ), // O Ideal adquisition of state 10 -> 00.
  .o_adq_101(branch_distance[ADQ_W*(5) +: ADQ_W]      ), // O Ideal adquisition of state 10 -> 01.
  .o_adq_110(branch_distance[ADQ_W*(6) +: ADQ_W]      ), // O Ideal adquisition of state 11 -> 10.
  .o_adq_111(branch_distance[ADQ_W*(7) +: ADQ_W]      )  // O Ideal adquisition of state 11 -> 11.
);

generate
  genvar i;
  for(i = 0; i < M; i = i + 1) begin : slide
    sliding_block #(
      .LAG_SYMBOL(i+1),
      .LAG_DECISION((2*M+1)-2*i),  // x-2*i = 3   when i = M-1
      .LAG_DATA(i-(D-1)),
      .DECODE(1 ? i >= D : 0),
      .DIRECTION(0)
    ) u_forward_sliding_block (
      .clk (clk),
      .enable (enable),
      .rst_n (rst_n),
      .i_symbol (i_sample[SAMP_W*((M-1)-i) +: SAMP_W]),
      .i_branch_distance (branch_distance),

      .i_state_metric (forward_state_metric[STATES*SM_WIDTH*(i) +: STATES*SM_WIDTH]),
      .o_state_metric (forward_state_metric[STATES*SM_WIDTH*(i+1) +: STATES*SM_WIDTH]),

      .i_state_estimate (forward_traceback_state_estimate[2*(i+1) +: 2]),  // [3:2]
      .o_state_estimate (forward_traceback_state_estimate[2*(i) +: 2]),    // [1:0]
      .o_data (forward_data[(M-1)-i])
    );

    sliding_block #(
      .LAG_SYMBOL  (i             ),
      .LAG_DECISION((2*M+1)-2*i   ), // x-2*i = 3   when i = M-1
      .LAG_DATA    (i-(D-1)       ),
      .DECODE      (1 ? i >= D : 0),
      .DIRECTION   (1             )
    ) u_backward_sliding_block (
      .clk              (clk                                                            ),
      .enable           (enable                                                         ),
      .rst_n            (rst_n                                                          ),
      .i_symbol         (i_sample[SAMP_W*(i) +: SAMP_W]                                 ),
      .i_branch_distance(branch_distance                                                ),
      
      .i_state_metric   (backward_state_metric[STATES*SM_WIDTH*(i) +: STATES*SM_WIDTH]  ),
      .o_state_metric   (backward_state_metric[STATES*SM_WIDTH*(i+1) +: STATES*SM_WIDTH]),
      
      .i_state_estimate (backward_traceback_state_estimate[2*(i+1) +: 2]                ),
      .o_state_estimate (backward_traceback_state_estimate[2*(i) +: 2]                  ),
      .o_data           (backward_data[i]                                               )
    );

  end
endgenerate

four_way_ac #(
  .SM_WIDTH(SM_WIDTH)
) u_four_way_ac (
  .i_f_sm_00 (forward_state_metric[STATES*SM_WIDTH*(M) + SM_WIDTH*(0) +: SM_WIDTH]),
  .i_f_sm_01 (forward_state_metric[STATES*SM_WIDTH*(M) + SM_WIDTH*(1) +: SM_WIDTH]),
  .i_f_sm_10 (forward_state_metric[STATES*SM_WIDTH*(M) + SM_WIDTH*(2) +: SM_WIDTH]),
  .i_f_sm_11 (forward_state_metric[STATES*SM_WIDTH*(M) + SM_WIDTH*(3) +: SM_WIDTH]),
  .i_b_sm_00 (backward_state_metric[STATES*SM_WIDTH*(M) + SM_WIDTH*(0) +: SM_WIDTH]),
  .i_b_sm_01 (backward_state_metric[STATES*SM_WIDTH*(M) + SM_WIDTH*(1) +: SM_WIDTH]),
  .i_b_sm_10 (backward_state_metric[STATES*SM_WIDTH*(M) + SM_WIDTH*(2) +: SM_WIDTH]),
  .i_b_sm_11 (backward_state_metric[STATES*SM_WIDTH*(M) + SM_WIDTH*(3) +: SM_WIDTH]),
  .o_state_estimate(state_estimate)
);
fifo #(
  .WIDTH(2),
  .DELAY (2)
) u_fifo_f_zn1 (
  .clk     (clk               ),
  .enable  (enable            ),
  .rst_n   (rst_n             ),
  .data_in (state_estimate    ),
  .data_out(state_estimate_reg)
);

assign o_data[M-1-:D] = forward_data[0 +: D];
assign o_data[0+:D]   = backward_data[M-1 -: D];

endmodule
