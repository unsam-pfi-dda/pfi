"""Plot correlation for a component."""

from typing import TypedDict

import numpy as np
import plotly.graph_objects as go
import plotly.subplots
from scipy import signal


class PlotcorrelationType(TypedDict):
    """Type Alias for expected data type."""

    name: str
    coefficients: list[float]
    input: list[float]  # noqa: A003
    output: list[float]


def plot_correlation(data: list[PlotcorrelationType]) -> go.Figure:
    """Component correlation plot.

    Example of input data:
        data = [
            {
                "name": "FFE",
                "coefficients": [-0.15, -0.4, 1.5, -0.20, 0.05],
                "input": [-1, 1, -1, 1, 1, 1, -1, 1, 1],
                "output": [1, 1, -1, 1, -1, 1, 1, 1, -1],
            },
            {
                "name": "DFFE",
                "coefficients": [0.25, 0.03, -0.01, -0.001, -0.015],
                "input": [-1, 1, -1, 1, 1, 1, -1, 1, 1],
                "output": [1, 1, -1, 1, -1, 1, 1, 1, -1],
            },
        ]
    """
    fig = plotly.subplots.make_subplots(
        rows=len(data),
        cols=5,
        vertical_spacing=0.05,
        horizontal_spacing=0.03,
        shared_xaxes=True,
        specs=[[{"colspan": 2}, None, {"colspan": 2}, None, {}]] * len(data),
    )

    for row, component in enumerate(data, start=1):
        # set row title
        fig.update_yaxes(
            title={
                "text": rf"$\text{{{component['name']}}}$",
                "standoff": 0.02,
                "font": {"size": 18},
            },
            row=row,
            col=1,
            matches="y",
        )

        fig.update_yaxes(row=row, col=3, matches="y2")
        fig.update_yaxes(row=row, col=5, matches="y3")

        # calculate the correlation between input and output buffers
        correlation = signal.correlate(component["input"], component["output"])
        lags = signal.correlation_lags(
            len(component["input"]), len(component["output"])
        )
        lag = int(lags[max(np.argwhere(correlation == np.amax(correlation)))])
        index = int(np.where(lags == lag)[0][0])

        # plot the correlation in the first row and column
        fig.add_trace(
            go.Scatter(
                y=correlation,
                x=lags,
                name="Correlation",
                mode="lines+markers",
                marker_color="black",
                line={"width": 1, "dash": "dot"},
            ),
            row=row,
            col=1,
        )

        # add an arrow where the lag is
        ann_text = f"{'no' if lag == 0 else 'leading' if lag > 0 else 'lag'} delay"
        if lag:
            ann_text += f" ({lag})"

        fig.add_annotation(
            x=lags[index],
            y=correlation[index],
            text=rf"$\text{{{ann_text}}}$",
            font={"size": 16},
            showarrow=True,
            arrowhead=1,
            standoff=5,
            xref="x1",
            yref="y1",
            row=row,
            col=1,
        )

        # plot signal buffers in the same plot
        fig.add_trace(
            go.Scatter(
                y=component["input"],
                name="Input buffer",
                mode="markers",
                marker_color="black",
                line={"width": 1, "dash": "dot"},
            ),
            row=row,
            col=3,
        )
        fig.add_trace(
            go.Scatter(
                y=component["output"],
                name="Output buffer",
                mode="markers",
                marker_color="gray",
                marker_symbol="star",
                line={"width": 1, "dash": "dot"},
            ),
            row=row,
            col=3,
        )

        # plot coefficients
        fig.add_trace(
            go.Scatter(
                y=component["coefficients"],
                mode="lines+markers",
                marker_color="black",
                line={"width": 1, "dash": "dot"},
            ),
            row=row,
            col=5,
        )
    # Set column and rows headers
    xaxes_titles = ["Correlation", "Input/Output buffers", "Coefficients"]
    xaxes_labels = ["Input to output offset", "Sample", "Index"]
    for col, (title, label) in enumerate(zip(xaxes_titles, xaxes_labels)):
        fig.update_xaxes(
            title={
                "text": rf"$\text{{{title}}}$",
                "standoff": 0.05,
                "font": {"size": 18},
            },
            showticklabels=False,
            side="top",
            row=1,
            col=(2 * col + 1),
        )
        fig.update_xaxes(
            title={
                "text": rf"$\text{{{label}}}$",
                "standoff": 0.1,
                "font": {"size": 16},
            },
            row=len(data),
            col=(2 * col + 1),
        )

    fig.update_xaxes(
        gridcolor="#CCC",
        griddash="dot",
        linecolor="#000",
        tickcolor="#000",
        ticklen=6,
        ticks="inside",
        mirror="ticks",
        showline=True,
    )
    fig.update_yaxes(
        zerolinecolor="#CCC",
        zerolinewidth=1,
        gridcolor="#CCC",
        griddash="dot",
        linecolor="#000",
        tickcolor="#000",
        ticklen=6,
        ticks="inside",
        mirror="ticks",
        showline=True,
    )

    fig.update_layout(
        height=200 * len(data),
        width=1200,
        showlegend=False,
        plot_bgcolor="#ffffff",
        paper_bgcolor="#ffffff",
    )

    fig.show()
    return fig


if __name__ == "__main__":
    pass
