import functools
from typing import Any, Protocol

import numpy as np
from numfi import numfi
from numpy import inf
from numpy import linalg as LA
from numpy.typing import NDArray
from scipy import signal

from .fixed_arithmetic.utils import to_fixed
from .wave import Wave


class ComponentBase(Protocol):
    """Abstract class for defining generic component base."""

    input: Wave
    noise: Wave
    response: Wave

    adaptive: bool

    locals: dict[str, Any]

    def run(self, sample: np.float64) -> np.float64:
        ...

    def reset(self, default: np.float64):
        ...

    def adapt(self, error: np.float64) -> None:
        ...

    @property
    def adapt_offset(self) -> int:
        ...

    @adapt_offset.setter
    def adapt_offset(self, new: int):
        ...

    @property
    def lag(self) -> int:
        ...

    @property
    def output(self) -> Wave:
        ...


class Component(ComponentBase):
    """Class for generic components."""

    coef: NDArray[np.float64]
    noise_scale: np.float64
    quantize: dict[str, str]

    def __init__(
        self,
        taps: list[float] | int = 1,
        snr: float = inf,
        adaptive: bool = True,
        quantize: None | dict[str, str] = None,
        buffer_length: None | int = None,
        normalize: bool = True,
    ) -> None:
        self._set_locals(locals())
        self.quantize = quantize if isinstance(quantize, dict) else {}
        super().__init__()

        # Taps argument can be either an integer number, indicating the number of taps
        # or a list of (hardcoded) coefficients
        # NOTE: Coefficient buffer (self.coef) will not be quantized so that the adaptive algorithm
        #       has full resolution for the calculations. But it will be quantized for calculating
        #       the output value (self._calculate function)
        if isinstance(taps, int):
            # if taps and not taps % 2:

            if buffer_length and taps > buffer_length:
                buffer_length = taps

            self.coef = signal.unit_impulse(shape=taps, idx="mid")

        else:  # Assumes that is a list of floats
            coef = np.array(taps)
            if normalize:
                coef = coef / LA.norm(coef)
            self.coef = coef

        if not buffer_length:
            buffer_length = len(self.coef)

        # Create Wave buffers
        self.input = Wave(buffer_length, quantize=to_fixed(self.quantize.get("input")))
        self.response = Wave(
            buffer_length,
            quantize=to_fixed(self.quantize.get("response")),
        )
        self.noise = Wave(buffer_length, quantize=to_fixed(self.quantize.get("noise")))

        # Set target SNR
        self.snr = snr
        # Enable or disable coefficient adaptation
        self.adaptive = adaptive
        self._adapt_offset = 0

    def run(self, sample: np.float64) -> np.float64:
        """Run."""
        # Add new sample to the input buffer
        self.input.add(sample)
        # Compute the system response
        self.response.add(self._calculate())
        # Create a noise component for the new sample
        if self.noise_scale > 0:
            self.noise.add(self._noise())

        return np.float64(self.output.last)

    def reset(self, default: np.float64):
        """Reset the component to initial values."""
        self.input.values.fill(default)

    @property
    def adapt_offset(self):
        return self._adapt_offset

    @adapt_offset.setter
    def adapt_offset(self, new: int):
        if new < 0:
            raise ValueError(f"Adaptation offset can not be negative - got {new}")
        self._adapt_offset = new

        # Update buffers
        if new + self.taps > len(self.input):
            # TODO: Fill new buffers with previous data
            self.input = Wave(
                new + self.taps,
                quantize=to_fixed(self.quantize.get("input")),
            )
            self.response = Wave(
                new + self.taps,
                quantize=to_fixed(self.quantize.get("response")),
            )
            self.noise = Wave(
                new + self.taps,
                quantize=to_fixed(self.quantize.get("noise")),
            )

    @property
    def output(self) -> Wave:
        """Return Wave object result from the system response and the external noise."""
        if self.noise_scale == 0:
            return self.response
        return self.response + self.noise

    def _calculate(self) -> np.float64:
        """Calculate system response using the input buffer and the FIR coefficients."""
        # Grab the newest samples from the buffer. This are located at the end of the vector.
        samples = self.input.values[-self.taps :]
        # Need to invert the input buffer because the samples are reversed. Last value [-1] is the
        # newest sample which should be multiplied with the first coefficient.
        samples = samples[::-1]

        coefficient = (
            self.coef
            if "coef" not in self.quantize
            else numfi(self.coef, **to_fixed(self.quantize["coef"]))
        )

        # Ensure the result is a float value (un-quantize if so)
        return np.float64(samples @ coefficient)

    def _noise(self) -> np.float64:
        """Create an AWGN component."""
        if self.noise_scale == 0:
            return 0.0

        return np.random.normal(loc=0) * self.noise_scale

    def adapt(self, error: np.float64) -> None:
        """Component adaptation."""
        return

    def _set_locals(self, locals: dict[str, Any]) -> None:  # noqa: A002
        """Store object creation argument values."""
        if not hasattr(self, "locals"):
            self.locals = {}

        for attr in ["self", "__class__", "kwargs"]:
            if attr in locals:
                del locals[attr]

        self.locals = self.locals | locals

    @functools.cached_property
    def taps(self) -> int:
        """Returns the number of taps."""
        return len(self.coef)

    @property
    def coef_power(self) -> np.float64:
        """Returns the component power applied by the filtering."""
        # Force floating point conversion before dot multiply
        return np.array(self.coef) @ np.array(self.coef)

    @property
    def snr(self) -> float:
        """Calculates the input signal to noise ratio in scalars."""
        if self.noise.power > 0:
            return self.input.power / self.noise.power
        else:
            return np.inf

    @snr.setter
    def snr(self, snr: float, input_power: float = 1) -> None:
        """Set the target SNR (in dB), this will automatically calculate the noise scale value
        which will be applied for adding noise.
        It will assume (by default) that the input signal power is normalized (1), otherwise set
        correct value (in scalars) with `input_power` argument.
        """
        if snr == np.inf:
            self.noise_scale = 0
            return

        # If input buffer is empty use a normalized power (1)
        if self.input.power > 0:
            input_power = self.input.power

        # Adjust Channel component noise_scale parameter for this current SNR.
        snr_scalar = 10 ** (snr / 10)
        noise_power = input_power / snr_scalar
        self.noise_scale = np.sqrt(noise_power / 2)

    @property
    def lag(self) -> int:
        """Calculates the time-delay for this system.
        Feeds the system with an impulse signal and measure the response delay.
        """
        impulse = signal.unit_impulse(self.taps * 2, self.taps // 4)
        response = signal.lfilter(b=self.coef, a=1.0, x=impulse)

        correlation = signal.correlate(impulse, response)
        correlation_lags = signal.correlation_lags(len(impulse), len(response))
        _lag_index = max(np.argwhere(correlation == np.amax(correlation)))
        _lag = int(correlation_lags[_lag_index])

        if _lag > 0:
            msg = f"output signal is leading instead of lagging, this is wrong: {self.coef}"
            raise ValueError(msg)

        return _lag
