"""Branch Metrics module testbench"""

import sys
from pathlib import Path
import cocotb
import cocotb.regression
from cocotb.handle import HierarchyObject
from cocotb.triggers import Timer
from cocotb_test.simulator import Simulator
import numpy as np

sys.path.append(str(Path(__file__).parent.parent.parent.resolve()))

from models.src import SBVD


def test_branch_metrics(simulator: Simulator):
    """Run parametrized CoCoTB test with pytest"""
    simulator.run()


async def model_cosimulation(dut: HierarchyObject, sample: int, distances: list[int]):
    """Cosimulate RTL and model and compare results"""
    drive_inputs(dut, sample, distances)
    await Timer(1, units="ps")  # wait a bit

    results = get_output(dut)
    golden = SBVD.branch_metrics(sample, distances)
    assert golden == results, print(golden, results, sample, distances)


tf = cocotb.regression.TestFactory(model_cosimulation)
tf.add_option("sample", np.random.choice(range(10), size=(10)).tolist())
tf.add_option("distances", np.random.choice(range(10), size=(20, 8)).tolist())
tf.generate_tests()


def drive_inputs(dut: HierarchyObject, sample, distances):
    dut.i_sample.value = sample
    dut.i_adq_000.value = distances[0]
    dut.i_adq_001.value = distances[1]
    dut.i_adq_010.value = distances[2]
    dut.i_adq_011.value = distances[3]
    dut.i_adq_100.value = distances[4]
    dut.i_adq_101.value = distances[5]
    dut.i_adq_110.value = distances[6]
    dut.i_adq_111.value = distances[7]


def get_output(dut: HierarchyObject):
    return [
        dut.o_delta_000.value.signed_integer,
        dut.o_delta_001.value.signed_integer,
        dut.o_delta_010.value.signed_integer,
        dut.o_delta_011.value.signed_integer,
        dut.o_delta_100.value.signed_integer,
        dut.o_delta_101.value.signed_integer,
        dut.o_delta_110.value.signed_integer,
        dut.o_delta_111.value.signed_integer,
    ]
