"""Unit testing for src.utils module."""

import numpy as np
import pytest

from src.utils import align


class TestAlign:
    """Tests for utils.align method."""

    def test_align_valid(self):
        """Test utils.align function with valid data."""
        offset = -4
        data = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        data_shift = [0, 0, 0, 1, 2, 3, 4, 5, 6]

        a_slice, b_slice, lag = align(data, data_shift, offset)

        assert len(data[a_slice]) == len(data) + offset
        np.testing.assert_array_equal(data[a_slice], data_shift[b_slice])

    def test_align_invalid(self):
        """Test utils.align function with valid data."""
        offset = -4
        data = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        data_shift = [0, 0, 0, 1, 2, 3, 4, 5, 6, 7]

        a_slice, b_slice, lag = align(data, data_shift, offset)

        assert len(data[a_slice]) == len(data) + offset
        with pytest.raises(AssertionError):
            np.testing.assert_array_equal(data[a_slice], data_shift[b_slice])
