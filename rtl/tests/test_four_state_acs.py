"""Four-state ACS module testbench"""

import sys
from pathlib import Path
import pytest
import cocotb
from cocotb.triggers import Timer
from cocotb.handle import HierarchyObject
from cocotb_test.simulator import Simulator
import numpy as np

sys.path.append(str(Path(__file__).parent.parent.parent.resolve()))

from models.src import SBVD


@pytest.mark.parametrize("direction", ["0", "1"], ids=["forward", "backward"])
def test_four_state_acs(simulator: Simulator, direction: str):
    """Run parametrized CoCoTB test with pytest"""
    simulator.parameters["DIRECTION"] = direction
    simulator.run()


async def model_cosimulation(
    dut: HierarchyObject, symbols: tuple[int], channel: tuple[int], state_metrics: list[int]
):
    """Cosimulate RTL and model and compare results"""
    sample = -1
    forward = True if dut.DIRECTION.value == 0 else False
    distances = SBVD.conv(symbols, channel)
    branch_metrics = SBVD.branch_metrics(sample, distances)
    drive_inputs(dut, state_metrics, branch_metrics)
    await Timer(1, units="ps")  # wait a bit

    results = get_output(dut)
    golden = SBVD.four_state_acs(state_metrics, branch_metrics, forward=forward)
    assert golden == results, print(golden, results, channel, state_metrics)


tf = cocotb.regression.TestFactory(model_cosimulation)
tf.add_option("symbols", [(-1, 1)])
tf.add_option("channel", [(1, 0, 0), (4, 2, 1)])
tf.add_option("state_metrics", np.random.choice(range(10), size=(10, 4)).tolist())
tf.generate_tests()


def drive_inputs(dut, sm, delta):
    dut.i_sm_00.value = sm[0]
    dut.i_sm_01.value = sm[1]
    dut.i_sm_10.value = sm[2]
    dut.i_sm_11.value = sm[3]
    dut.i_delta_000.value = delta[0]
    dut.i_delta_001.value = delta[1]
    dut.i_delta_010.value = delta[2]
    dut.i_delta_011.value = delta[3]
    dut.i_delta_100.value = delta[4]
    dut.i_delta_101.value = delta[5]
    dut.i_delta_110.value = delta[6]
    dut.i_delta_111.value = delta[7]


def get_output(dut: HierarchyObject):
    return [
        (
            dut.o_dec_00.value.integer,
            dut.o_dec_01.value.integer,
            dut.o_dec_10.value.integer,
            dut.o_dec_11.value.integer,
        ),
        (
            dut.o_sm_00.value.integer,
            dut.o_sm_01.value.integer,
            dut.o_sm_10.value.integer,
            dut.o_sm_11.value.integer,
        ),
    ]
