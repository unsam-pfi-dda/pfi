import argparse
from pathlib import Path
from typing import Any

import numpy as np
import plotly.graph_objects as go
from plotly.subplots import make_subplots
from src import ComponentBase, Plot, System, align
from tqdm import tqdm

lms_feedback: list[list[int | float]] = []
lms_forward: list[list[int | float]] = []
errors: list[list[int | float]] = []


def monitor(system: System, sample: int) -> None:
    """Store the system evolution so it can be analyzed afterwards."""
    lms_feedback.append([sample, *system.feedback.coef.copy().astype(float)])
    lms_forward.append([sample, *system.forward.coef.copy().astype(float)])
    # TODO: fix error monitors, should be output - input with lag correction
    errors.append([sample, float(system.error**2)])


def evolution() -> go.Figure:
    """Plot the system evolution measured by monitor."""
    fig = make_subplots(
        rows=2,
        cols=1,
        shared_xaxes=True,
        subplot_titles=[
            "Coefficient evolution",
            "System error",
        ],
        vertical_spacing=0.1,
        horizontal_spacing=0.1,
    )

    # Subplot 1: Coefficient evolution
    samples, *coefs = zip(*lms_feedback, strict=True)
    for cursor, coef in enumerate(coefs):
        fig.add_trace(
            go.Scatter(
                x=samples,
                y=np.array(coef),
                name=f"DFFE #{cursor}",
                mode="lines+markers",
                line={"width": 1, "dash": "dot"},
            ),
            row=1,
            col=1,
        )
    samples, *coefs = zip(*lms_forward, strict=True)
    for cursor, coef in enumerate(coefs):
        fig.add_trace(
            go.Scatter(
                x=samples,
                y=np.array(coef),
                name=f"RX #{cursor}",
                marker_symbol="star",
                mode="lines+markers",
                line={"width": 1, "dash": "dot"},
            ),
            row=1,
            col=1,
        )
    fig.update_xaxes(title_text="Sample", row=1, col=1)
    fig.update_yaxes(title_text="Value", row=1, col=1)

    # Subplot 2: Error evolution
    samples, error = zip(*errors)
    fig.add_trace(
        go.Scatter(
            x=samples,
            y=error,
            name="Estimation error",
            mode="lines+markers",
            line={"width": 1, "dash": "dot"},
        ),
        row=2,
        col=1,
    )
    fig.update_yaxes(title_text="Value", type="log", row=2, col=1)
    return fig


def coefficients(system: System) -> list[dict[str, Any]]:
    """Get system coefficient status."""
    channel = system.channel.coef.copy()
    forward = system.forward.coef.copy()
    feedback = system.feedback.coef.copy()

    # TODO: What if channel, forward and feedback have different sizes?
    convolve = np.convolve(channel, forward, "same")
    forward_offset = np.ceil((len(convolve) - len(forward)) / 2)
    # Fisrt feedback coefficient should be aligned with the first forward&channel post-cursor.
    feedback_offset = len(convolve) // 2 + 1
    channel_offset = (len(convolve) - len(channel)) // 2

    # Adjust convolve and feedback vector sizes (cant sum arrays with different lengths)
    shape = len(convolve) - (len(feedback) + feedback_offset)
    if shape < 0:
        response = np.pad(convolve, (0, abs(shape))) - np.pad(
            feedback,
            (feedback_offset, 0),
        )
    elif shape > 0:
        response = convolve - np.pad(feedback, (feedback_offset, abs(shape)))
    else:
        response = convolve - np.pad(feedback, (feedback_offset, 0))

    return [
        {
            "name": "Channel",
            "offset": int(channel_offset),
            "values": channel.astype(float).tolist(),
        },
        {
            "name": "Forward",
            "offset": int(forward_offset),
            "values": forward.astype(float).tolist(),
        },
        {
            "name": "Equivalent channel",
            "offset": 0,
            "values": convolve.astype(float).tolist(),
        },
        {
            "name": "Feedback",
            "offset": int(feedback_offset),
            "values": feedback.astype(float).tolist(),
        },
        {
            "name": "Equalizer response",
            "offset": 0,
            "values": response.astype(float).tolist(),
        },
    ]


def status(system: System, sample: int) -> go.Figure:
    input = system.channel.input
    output = system.quantizer.output
    estimate = system.quantizer.input

    # Calculate the estimation error
    error_estimate = (estimate.values - output.values) ** 2
    input_slice, estimate_slice, plot_lag = align(input, estimate, system.lag)
    error_system = (estimate.values[estimate_slice] - input.values[input_slice]) ** 2

    fig = make_subplots(
        rows=3,
        cols=1,
        shared_xaxes=False,
        subplot_titles=["Frequency response", "System error", "Signals"],
        vertical_spacing=0.1,
        horizontal_spacing=0.1,
    )
    fig.update_layout(title_text=f"Overall status for sample {sample}")

    # Subplot 1: Frequency response
    for component in coefficients(system):
        trace = go.Scatter(
            x0=component.get("offset", 0),
            y=component["values"],
            name=component["name"],
            mode="lines+markers",
            line={"width": 1, "dash": "dot"},
        )
        fig.add_trace(trace, col=1, row=1)

    fig.update_xaxes(title_text="Coefficient index", row=1, col=1)
    fig.update_yaxes(title_text="Value", row=1, col=1)
    fig.update_xaxes(tickmode="linear", row=1, col=1)

    # Subplot 2: Error
    fig.add_trace(
        go.Scatter(
            y=error_estimate,
            name="Estimation error signal",
            mode="lines+markers",
            line={"width": 1, "dash": "dot"},
        ),
        row=2,
        col=1,
    )
    fig.add_trace(
        go.Scatter(
            x0=-plot_lag,
            y=error_system,
            name="Training error signal",
            mode="lines+markers",
            line={"width": 1, "dash": "dot"},
        ),
        row=2,
        col=1,
    )
    fig.update_xaxes(title_text="Buffer", row=2, col=1)
    fig.update_yaxes(title_text="Value", type="log", row=2, col=1)

    # Subplot 3: Signals
    fig.add_annotation(
        x=len(output) - 1,
        y=output.last,
        text="Last output value",
        showarrow=True,
        arrowhead=1,
        standoff=5,
        xref="x3",
        yref="y3",
    )
    fig.add_annotation(
        x=len(output),
        y=input.values[len(output) + plot_lag],
        text="Sample not yet processed",
        showarrow=True,
        arrowhead=1,
        standoff=5,
        xref="x3",
        yref="y3",
    )
    fig.add_annotation(
        x=len(input) - 1 - plot_lag,
        y=input.last,
        text="Current input value",
        showarrow=True,
        arrowhead=1,
        standoff=5,
        xref="x3",
        yref="y3",
    )
    fig.add_trace(
        go.Scatter(
            x0=-plot_lag,
            y=input.values,
            name=f"Input buffer (power={input.power:.3f})",
            mode="markers",
        ),
        row=3,
        col=1,
    )
    fig.add_trace(
        go.Scatter(
            y=output.values,
            name=f"Output buffer (power={output.power:.3f})",
            mode="markers",
        ),
        row=3,
        col=1,
    )
    fig.update_xaxes(title_text="Buffer index", row=3, col=1)
    fig.update_yaxes(title_text="Signal value", row=3, col=1)

    fig.update_layout(xaxis2_showticklabels=True, xaxis3_showticklabels=True)
    return fig


def plot_component_lag(system: System) -> None:
    """Lag plot for components, save correlation plot into a file."""

    def _filter_component(x: ComponentBase) -> bool:
        """Filter component by attributes for plotting."""
        # Skip components that do not have the required attributes
        if not all([hasattr(x, "coef"), hasattr(x, "input"), hasattr(x, "output")]):
            return False
        # Do not plot simple components
        if not len(x.coef) > 1:
            return False
        return True

    data = [
        {
            "name": component.__class__.__name__,
            "coefficients": list(map(float, component.coef)),
            "input": list(map(float, component.input.values)),
            "output": list(map(float, component.output.values)),
        }
        for component in system.components
        if _filter_component(component)
    ]

    Plot.save_plot(plot_type="correlation", plot_data=data)


def main(config: str | Path, samples: int = 30_000) -> None:
    """Run the system."""
    system = System.from_yaml(config)

    monitor_samples = np.unique(
        np.geomspace(start=1, stop=samples - 1, num=200, dtype=int),
    )

    # Iterate until some event interrupts
    for sample in tqdm(range(samples)):
        # Compute all components for this tick
        system.run()

        # System monitor
        if sample in monitor_samples:
            monitor(system, sample)

    # Plot status at the end
    plot_component_lag(system)
    status(system, samples - 1).show()
    evolution().show()

    # Dump coefficient evolution values over samples into a YAML data file for plotting
    filters = (
        system.forward.__class__.__name__,
        lms_forward,
    ), (
        system.feedback.__class__.__name__,
        lms_feedback,
    )
    for component, data in filters:
        _samples, *coefs = zip(*data, strict=True)
        coef_evolution = [
            {
                "name": f"{component}[{cursor}]",
                "x": _samples,
                "y": coef,
            }
            for cursor, coef in enumerate(coefs)
        ]
        Plot.save_plot("evolution", coef_evolution)

    # Dump samples bitstream into a YAML data file for plotting
    data = [
        {
            "name": component.__class__.__name__,
            "lag": abs(component.lag),
            "input": component.input.values.tolist(),
            "output": component.output.values.tolist(),
        }
        for component in system.components
    ]
    Plot.save_plot("bitstream", data)

    # Dump final coefficient values for components and final system response
    # into a YAML data file for plotting.
    Plot.save_plot("coefficients", coefficients(system))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Evolution")
    parser.add_argument(
        "-c",
        "--config",
        default="default.yaml",
        help="Configuration file to use",
    )
    parser.add_argument(
        "-s",
        "--samples",
        default=30_000,
        type=int,
        help="Number of samples to run",
    )

    args = parser.parse_args()
    main(config=args.config, samples=args.samples)
