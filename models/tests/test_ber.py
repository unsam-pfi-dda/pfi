"""Unit testing for src.sim.core.ber_core module."""

import itertools

import pytest

from src import DFFE, FFE, Channel, Quantizer, System
from src.sims.core import BerTestDirector

SYMBOLS = (-1, 1)


@pytest.fixture(name="system")
def fixture_system():
    """Fixture for creating a System object fixture."""
    return System(
        symbols=SYMBOLS,
        components=[
            Channel(),
            FFE(),
            DFFE(taps=5, iterations=0),
            Quantizer(),
        ],
    )


@pytest.fixture(name="director")
def fixture_director():
    """Fixture for creating a BerTestDirector object fixture."""
    return BerTestDirector()


def test_component_sweep_attribute(director: BerTestDirector):
    """Test that test components sweep are being set as expected."""
    system = System.from_yaml("iterations.yaml")
    director.add_test(
        name="Test",
        system=system,
        sweep="iterations",
        values=range(5, 10),
        reference=DFFE,
        adapt=10_000,
        symbol="circle",
    )
    director.add_test(
        name="Test 2",
        system=system,
        sweep="taps",
        values=range(5, 10),
        reference=DFFE,
        adapt=10_000,
        symbol="star",
    )

    for test in director.tests:
        test.create_tests()
        for worker, value in zip(test.tests, test.values):
            dut = worker.system.get_component(test.reference)
            assert dut.__getattribute__(test.sweep) == value

    # Make sure that tests are not sharing any System object.
    for test_a, test_b in itertools.combinations(director._workers, 2):
        assert test_a is not test_b
        assert test_a.system is not test_b.system
