"""Unit testing for src.system module."""

from pathlib import Path

import numpy as np
import pytest
from scipy import signal

from src import DFFE, FFE, SBVD, Channel, Quantizer, System

SYMBOLS = [-1, 1]
CFG = {
    "Generator": {
        "symbols": SYMBOLS,
    },
    "Components": {
        "Channel": {
            "taps": [0, 0, 0.25, 1, 0.25, 0, 0],
        },
        "AGC": {},
        "FFE": {"taps": 11, "step": 0.001},
        "DFFE": {
            "step": 0.001,
            "taps": 3,
            "iterations": 5,
            "mlse_depth": 10,
            "mlse_taps": 2,
        },
        "Quantizer": {},
    },
}


@pytest.fixture(name="system")
def fixture_system():
    """Default System object for testing."""
    return System.from_dict(CFG)


class TestSystem:
    """Tests for System object."""

    def test_create_from_yaml_default(self):
        """Test System.from_yaml function with default arguments."""
        assert isinstance(System.from_yaml(), System)

    def test_create_from_yaml_relative(self):
        """Test System.from_yaml function with a relative path."""
        path = Path("default.yaml")
        assert isinstance(System.from_yaml(path), System)

    def test_create_from_yaml_absolute(self):
        """Test System.from_yaml function with an absolute path."""


class TestSystemRun:
    """Tests for System.run method."""

    def test_system_default(self, system: System):
        for _ in range(abs(system.lag)):
            system.run()


class TestSystemLag:
    """Test for System.lag property."""

    def test_lag(self):
        """Test System.error property for a system with lag, checks if values are aligned."""
        system = System(
            components=[
                Channel(taps=[0, 0, 1, 0, 0], buffer_length=50),
                FFE(taps=[0, 1, 0], adaptive=False),
                Quantizer(buffer_length=50),
            ],
        )

        for _ in range(50):
            system.run()

        samples = system.channel.input.values
        results = system.quantizer.output.values

        lag = signal.correlation_lags(len(samples), len(results))[
            np.argmax(signal.correlate(samples, results))
        ]
        assert system.lag == lag
        np.testing.assert_array_equal(samples[: system.lag], results[-system.lag :])

    def test_lag_duo_binary(self):
        """Ensure that lag is calculated correctly even for nasty channels."""
        samples = 1_000
        system = System(
            components=[
                Channel(taps=[0, 0, 2, 2, 1], buffer_length=samples),
                FFE(taps=1, adaptive=False),
                DFFE(
                    taps=[0.666, 0.333],
                    mlse_main_tap=0.666,
                    iterations=10,
                    mlse_iterations=3,
                    mlse_depth=10,
                    mlse_taps=2,
                    adaptive=False,
                ),
                Quantizer(buffer_length=samples),
            ],
        )

        for _ in range(samples):
            system.run()

        LAG = (
            -2 # system.channel.lag
            + SBVD(depth=system.feedback.mlse[0].depth, taps=system.feedback.mlse[0].taps).lag
        )

        assert system.lag == LAG
        np.testing.assert_array_equal(
            system.channel.input.values[: system.lag],
            system.quantizer.output.values[-system.lag :],
        )

    def test_lag_adaptive_offset(self):
        """Test System components adaptive offset attribute."""
        system = System(
            symbols=SYMBOLS,
            components=[
                Channel(taps=[0, 0, 1, 0, 0]),
                FFE(taps=[0, 1, 0], adaptive=False),
                DFFE(taps=3, iterations=4, mlse_depth=5),
                Quantizer(buffer_length=50),
            ],
        )

        assert system.quantizer.adapt_offset == 0
        assert system.feedback.adapt_offset == 0
        assert system.forward.adapt_offset == -system.feedback.lag
        assert system.channel.adapt_offset == -(
            system.feedback.lag + system.forward.lag
        )

    def test_is_error_true(self):
        """Test System.is_error property for a system that has zero errors."""
        system = System(
            symbols=SYMBOLS,
            components=[
                Channel(taps=[0, 0.25, 1, 0, 0]),
                FFE(taps=[0.25, 1, 0], adaptive=False),
                Quantizer(),
            ],
        )

        for _ in range(1000):
            system.run()
            assert not system.is_error
