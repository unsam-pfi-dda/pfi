module clock_enable #(
  parameter DIVIDER = 20
)(
  input            clk    ,
  input            rst_n  ,
  output reg [5:0] count_rg,
  output           clk_en_rg   // Use this signal as an enable @25MHz
);

assign clk_en_rg = (count_rg == (DIVIDER-1)) ? 1'b1 : 1'b0;

/* Synchronous logic to generate Clock-Enable pulse @ 25 MHz from 100 MHz clk */
always @ (posedge clk, negedge rst_n) begin
  if (!rst_n) begin
    count_rg  <= 1'b0;
  end
  else begin
    count_rg <= (count_rg < (DIVIDER-1)) ? (count_rg + 1) : 1'b0;
  end
end

endmodule
