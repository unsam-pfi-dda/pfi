"""SQNR test for MLSE component.

This test will measure SQNR for a fixed point MLSE component against a reference
(floating) MLSE.

How to run:
    python -m src.sims sqnr mlse

The results will be sent to the Plot module (./src/Plot) for storing and plotting
with plotly.
"""

import numpy as np

from src import DFFE, System

from .core import SQNRTestDirector


def sqnr_sbvd():
    system = System.from_yaml("quantization.yaml")
    tests = SQNRTestDirector(
        adapt=10_000,
        measure=1000,
        system=system,
        reference=system.feedback,
    )

    tests.add_test(
        name="Viterbi cost",
        quantize={
            "coef": "s9/8-z/s",
            "input": "s10/8-z/s",
            "response": "s10/8-z/s",
        },
        sweep="cost",
        integer=range(2, 6),
        fractional=range(1, 10),
    )

    tests.add_test(
        name="Viterbi metrics",
        quantize={
            "coef": "s9/8-z/s",
            "input": "s10/8-z/s",
            "response": "s10/8-z/s",
        },
        sweep="metrics",
        integer=range(0, 4),
        fractional=range(1, 10),
    )

    def sbvd_measure_function(dut: DFFE, ref: DFFE) -> tuple[np.float64, np.float64]:
        """Custom sbvd measure function for SQNR calculation."""
        if dut.sbvd[0].i != 0:  # only count when cost values are latched
            return (np.float64(0), np.float64(0))
        return (np.float64(dut.sbvd[0].cost), np.float64(ref.sbvd[0].cost))

    # Replace measure function
    for test in tests.tests:
        test._measure = sbvd_measure_function

    return tests.run()
